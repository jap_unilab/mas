<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct() {
        parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('encryption');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('user_model');
		$this->load->model('system_info_model');
	}

	public function index(){
	}

	public function register() {
		$arr['db'] = $this->system_info_model->fetchData();

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[5]|max_length[25]|alpha_numeric|is_unique[fx_users.username]', array('is_unique' => 'That <b>%s</b> is taken. Try another.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[fx_users.email]', array('is_unique' => 'That <b>%s</b> is taken. Try another.'));
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|max_length[25]|matches[password]');

		if ($this->form_validation->run() == false) {
			$this->load->view('user/register_view',$arr);
		}else{
			$arr['db']->success = TRUE;
			$user_id = $this->user_model->register_user($_POST);
			$this->load->view('user/register_view',$arr);
		}
	}

	public function login() {
		if(!$this->session->userdata('logged_in')){
		$arr['db'] = $this->system_info_model->fetchData();
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->form_validation->set_rules('username', 'Username', 'required|callback_resolve_user_login');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_resolve_password_login');
		$this->form_validation->set_rules('accept_terms', '...', 'callback_accept_terms');
			if ($this->form_validation->run() == false) {
				$this->load->view('user/login_view',$arr);
			}else{
					redirect('accounts', 'refresh');	
			}
		}else{
			redirect('accounts', 'refresh');
		}
	}
	
	public function reset(){
		$arr['db'] = $this->system_info_model->fetchData();
		$email = $this->input->post('email');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

		if ($this->form_validation->run() == false) {
			$this->load->view('user/reset_view',$arr);
		}else{
			 if($this->user_model->check_email($email)){
				$this->user_model->check_date_reset_email($email);
				if($this->user_model->no_reset_email($email)){
				    $plain_text = mt_rand('5000', '200000');
			        $en = $this->encryption->encrypt($plain_text);
					if($this->send_email($email,$en)){
						$this->user_model->add_no_reset_email($email,$en);
					}
					$arr['db']->success = "We sent a recovery link to your email address.";
				}else{
					$arr['db']->error = "You reach the maximum count of password recovery, try again tommorow.";
				}
			 }else{
				$arr['db']->error = "The <b>Email</b> is not registered.";
			 }
			$this->load->view('user/reset_view',$arr);
		}
	}
	
	function send_email($email,$en){
		$en2 =  http_build_query(array('key' => $en));
		$verifylink = base_url('change-password?').$en2;
		
		$to = $email;
        $subject = 'MASS Password Reset';
		$message = '<H3>Welcome to MASS (Medical Assistance Something)</H3><br>
								As requested, here is the link to reset your password. This link will expire in 24 Hours.<br><br>
							 '.$verifylink.'
							 <br><br><br>
							 
							 Thank you.
							 
							 <br><br><br>
							 
							 If you are not trying to reset your login credentials, please ignore this email. It is possible that another user entered their login information incorrectly.';
		
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        // More headers
        $headers .= 'From: MASS (Medical Assistance Something) <japmgx@gmail.com>' . "\r\n";
        
        $result = mail($to,$subject,$message,$headers);
        return $result;
	}
	
	public function change_password(){
        $code = xss_clean($this->input->get('key',true));
        $code = htmlspecialchars($code);
        $code = $this->encryption->decrypt($code);
        $status = null;
        $arr['db'] = $this->system_info_model->fetchData();
        $arr_get = $this->user_model->check_code($code);
       
        if(empty($arr_get['status'])){
            $status = $this->input->post('status');
            $arr['db']->status =  $this->input->post('status');
             $arr['db']->email_pass = $this->input->post('email');
        }else{
            $status = $arr_get['status'];
            $arr['db']->status = $arr_get['status'];
            $arr['db']->email_pass = $arr_get['email'];
        }
       
    	if($status == 1){
    	    $email = $this->input->post('email');
            $password = $this->input->post('password');
    	    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        	$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|max_length[25]|matches[password]');
            if ($this->form_validation->run() == false) {
    			$this->load->view('user/change_password_view',$arr);
    		}else{
    		    if($this->user_model->change_password_and_reset_no_attemp($email,$password)){
    		         $arr['db']->success = true;
    		         $this->load->view('user/change_password_view',$arr);
    		    }else{
    		        $arr['db']->error = true;
    		        $arr['db']->error_mes = 'Sorry, something wrong happend.';
    	            $this->load->view('user/change_password_view',$arr);
    		    }
    		   
    		}
    
    	}else{
             $arr['db']->error = true;
             $arr['db']->error_mes = 'Sorry, your password reset link expired.';
    	    $this->load->view('user/change_password_view',$arr);
    	}
	}
	
	function signout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}
	
	public function resolve_user_login(){
		$username = $this->input->post('username');
		if ($this->user_model->resolve_user_login($username)) {
			return true;
		}else{
			$this->form_validation->set_message('resolve_user_login', 'Invalid User.');
			return false;
		}
	}
	
	public function resolve_password_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if (!$this->user_model->resolve_user_password_login($username, $password)) {
			$this->form_validation->set_message('resolve_password_login', 'Invalid Password.');
			return false;
		}else{
			$sess_array = array();
			$user_id = $this->user_model->get_user_id_from_username($username);
			$user    = $this->user_model->get_user($user_id);
            
            $img = $this->avatar($user->image,$user->first_name);
            
			// set session user datas
			$sess_array = array(
				'user_id' 		=> 	$this->encryption->encrypt((int)$user->id),
				'email' 		=> 	(string)$user->email,
				'username' 		=> 	(string)$user->username,
				'first_name'	=>	(string)$user->first_name,
				'last_name' 	=>	(string)$user->last_name,
				'avatar'	    =>	$img,
				'user_type' 	=>	(string)$user->user_type,
				'user_status' 	=>	(string)$user->user_status,
				);
			$this->session->set_userdata('logged_in', $sess_array);

			return true;
		}
	}

    function avatar($image,$name){
        $usr_prf_path_img = '<img src="'.HTTP_ASSETS_PATH.'custom/img/person.jpg" width="30px" class="img-circle" alt="'.$name.' profile image">';
		if (@getimagesize('./assets/custom/img/user_prof/'.$image)) {
		    $usr_prf_path_img = '<img src="'.HTTP_ASSETS_PATH.'custom/img/user_prof/'.$image.'" width="30px" class="img-circle" alt="'.$name.' profile image">';
        }
        return $usr_prf_path_img;
    }

	
	public function accept_terms(){
		if (isset($_POST['accept_terms'])) return true;
		$this->form_validation->set_message('accept_terms', '&nbsp; <i class="fa fa-exclamation-triangle"></i> &nbsp; Please read and accept our Data Privacy Policy.');
		return false;
	}
	
	function show_404(){
      $ci = get_instance();
      $ci->output->set_status_header(404);
      $arr['message_header'] = "400";
      $arr['message_title'] = "Oops! Link Invalid.";
      $arr['message_body'] = "The page cannot be open because the address is invalid";
      $ci->load->view('errors/err404',$arr);
      echo $ci->output->get_output();
      exit(4);
    }

}
