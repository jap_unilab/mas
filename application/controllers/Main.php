<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('system_info_model');
	}

	public function index(){
	
	}

	function pages(){
		$arr['db'] = $this->system_info_model->fetchData();
		$this->load->view('main_View',$arr);
	}
}
