<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('system_info_model');
		$this->load->model('account_model');
		$this->load->model('member_model');
		$this->load->model('member_model2');
		$this->load->library('encryption');
	}

	public function index(){}
	
	public function company_list(){
		if($session = $this->session->userdata('logged_in')){
		    $arr['db'] = new \stdClass();
		    $arr['db']->page_header = array("title"=>"Company List - Accounts | MAS");
			$arr['db']->main_header = array("avatar"       =>  $this->system_info_model->fetchAvatar($session['user_id']),
			                                "name_title"   =>  $this->system_info_model->fetchNameTitle($session['user_id']),
			                                "user_info"    =>  $this->system_info_model->fetchDataUserInfo($session['user_id']),
			                                "sys_info"     =>  $this->system_info_model->fetchData()
			                          );
			
			$arr['db']->main_sub_header = array("accounts"=>TRUE,  // for tabs
			                                    "responsibility" => $this->system_info_model->fetchDataResponsibility($session['user_id']));
			                                    
			$arr['db']->page_body = array("generate_code"=>strtoupper($this->generateRandomString(10)));                                    
			
			$this->load->view('account/company_list_view',$arr);
		}else{
				redirect('login', 'refresh');
		}
	}

	public function benefit_list(){
		if($session = $this->session->userdata('logged_in')){
		    $arr['db'] = new \stdClass();
		    $arr['db']->page_header = array("title"=>"Benefit List - Accounts | MAS");
			$arr['db']->main_header = array("avatar"       =>  $this->system_info_model->fetchAvatar($session['user_id']),
			                                "name_title"   =>  $this->system_info_model->fetchNameTitle($session['user_id']),
			                                "user_info"    =>  $this->system_info_model->fetchDataUserInfo($session['user_id']),
			                                "sys_info"     =>  $this->system_info_model->fetchData()
			                          );
			
			$arr['db']->main_sub_header = array("accounts"=>TRUE,  // for tabs
			                                    "responsibility" => $this->system_info_model->fetchDataResponsibility($session['user_id']));
			                                    
			$arr['db']->page_body = array("generate_code"=>strtoupper($this->generateRandomString(10)));                                    
			
			$this->load->view('account/benefit_list_view',$arr);
		}else{
				redirect('login', 'refresh');
		}
	}
	
	public function ajax_list(){
	    $list = $this->account_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $account_model) {
	    	
			$id_enc = $this->encryption->encrypt($account_model->company_setup_id);
            
			$no++;
            $row = array();
			
			$row[] = ""; // checkbox
			$row[] = $id_enc;
			$row[] = strtoupper($account_model->comp_set_code);
			$row[] = ucwords($account_model->comp_set_company_name);
			$row[] = '<center>'.date("m/d/Y", strtotime($account_model->comp_contr_effectivity)).' - '.date("m/d/Y", strtotime($account_model->comp_contr_expiration)).'</center>';
			$row[] = '<center><div class="btn-group" id="cmpyDrpDwn" >
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <i class="fa fa-cog" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" style="background-color:#bbdefb">
                          <li class=""><a href="javascript:void(0)" tabindex="-1" onclick=contractAction("view","'.$id_enc.'")> Contract</a></li>
                          <li class=""><a href="javascript:void(0)" tabindex="-1" onclick=benefitsAction("view","'.$id_enc.'")> Benefits</a></li>
						  <li class=""><a href="javascript:void(0)" tabindex="-1" onclick=comp_setup("view","update","'.$id_enc.'")> Update</a></li>
						  <li class="dropdown-submenu pull-left">
								<a href="javascript:void(0)" tabindex="-1"  data-toggle="dropdown"><i class="fa fa-caret-left" aria-hidden="true" ></i>&nbsp; Members</a>
								<ul class="dropdown-menu ">
									<li><a href="javascript:void(0)" tabindex="-1"  onclick=contentMod("member_list","'.$id_enc.'")><i class="fa fa-eye" aria-hidden="true"></i> View Members</a>
									</li>
									<li><a href="javascript:void(0)" tabindex="-1"  onclick=enrollMemberAction("view","add","'.$id_enc.'")><i class="fa fa-eye" aria-hidden="true"></i>  Enroll Members</a>
									</li>
								</ul>
						</li>	
                        </ul>
                      </div></center>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->account_model->count_all(),
						"recordsFiltered" => $this->account_model->count_filtered(),
						"data" => $data,
				);
		
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list2(){
	    $list = $this->member_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $member_model) {
	    	
			$id_enc = $this->encryption->encrypt($member_model->member_id);
            
			$no++;
            $row = array();
			
			$row[] = ""; // checkbox
			$row[] = $id_enc;
			$row[] = $member_model->member_id;
			$row[] = date("m/d/Y",strtotime($member_model->member_hire_date));
			$row[] = $member_model->name;
			$row[] = $member_model->member_type;
			
			$row[] = '<center><div class="btn-group" id="cmpyDrpDwn">
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <i class="fa fa-cog" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li class=""><a href="javascript:void(0)" tabindex="-1" onclick=contractAction("view","'.$id_enc.'")> Contract</a></li>
                          <li class=""><a href="javascript:void(0)" tabindex="-1" onclick=benefitsAction("view","'.$id_enc.'")> Benefits</a></li>
													<li class=""><a href="javascript:void(0)" tabindex="-1" onclick=comp_setup("view","update","'.$id_enc.'")> Update</a></li>
													<li class="dropdown-submenu pull-left">
													<a href="javascript:void(0)" tabindex="-1"  data-toggle="dropdown"><i class="fa fa-caret-left" aria-hidden="true" ></i>&nbsp; Members</a>
													<ul class="dropdown-menu ">
															<li><a href="javascript:void(0);" tabindex="-1"  onclick=contentMod("member_list","'.$id_enc.'")><i class="fa fa-eye" aria-hidden="true"></i> View Members</a></li>
							<li><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>  Enroll Members</a></li>
						</ul>
						</li>	
                        </ul>
                      </div></center>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->member_model->count_all(),
						"recordsFiltered" => $this->member_model->count_filtered(),
						"data" => $data,
				);
		
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list3(){
	    $list = $this->member_model2->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $member_model2) {
	    	
			$id_enc = $this->encryption->encrypt($member_model2->member_id);
            
			$no++;
            $row = array();
			
			$row[] = ""; // checkbox
			$row[] = $id_enc;
			$row[] = $member_model2->member_id;
			$row[] = date("m/d/Y",strtotime($member_model2->member_hire_date));
			$row[] = $member_model2->name;
			$row[] = $member_model2->member_type;
			$row[] = $member_model2->member_batch_id;
			$row[] = ($member_model2->id_status == "0" ? 'For Printing' : ($member_model2->id_status == "1" ? "For Printed" : "Released"));
			$row[] = $member_model2->member_date;
			$row[] = '<a href="#">'.($member_model2->id_status == "0" ? 'For Printing' : ($member_model2->id_status == "1" ? "For Printed" : "Released"))."</a>";
			$row[] = $member_model2->member_reference;
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->member_model2->count_all(),
						"recordsFiltered" => $this->member_model2->count_filtered(),
						"data" => $data,
				);
		
		//output to json format
		echo json_encode($output);
	}

	private function company_list_ipn(){
		$ch = curl_init("http://121.58.195.170/hrportal/company");
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_PORT, 801);
        //curl_setopt($ch, CURLOPT_POSTFIELDS,$myarray);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('x-api-key: kjdasd8843874h3jek23uhe2d3jskw2hds2'));
        $data = curl_exec($ch);
        // var_dump($data); die;//($data,true); die;
       if ($data === false) {
         echo "Failed: (" . curl_errno($ch) . ") " . curl_error($ch) . " " . curl_getinfo($ch,CURLINFO_HTTP_CODE) . "<br/>";
       }
       curl_close($ch);
       $dcode = (array)json_decode($data,true);
       return $dcode;
	}

	public function getCompanyAPI(){
		$data_api = $this->company_list_ipn();
		foreach($data_api as $elem){
			$q = $this->db->query('select * from company_setup_tb where cpny_api_id = '.$elem['id']);
			if($q->num_rows() > 0){
				$result = $this->db->query("UPDATE `company_setup_tb` SET 
				`comp_set_code`='".$elem['companycode2']."',
				`comp_set_shortname`='".$elem['initial']."',
				`comp_set_status_code`='".$elem['statuscode']."',
				`comp_set_company_name`='".$elem['companyname']."',
				`comp_set_address`='".$elem['streetaddress']."',
				`comp_set_office_number`='".$elem['telephone']."',
				`comp_set_website`='".$elem['website']."',
				`comp_bil_vat_type`='".$elem['vattype']."',
				`comp_contr_effectivity`='".date("Y-m-d",strtotime($elem['effectivity']))."',
				`comp_contr_expiration`='".date("Y-m-d",strtotime($elem['expiry']))."'
				WHERE cpny_api_id = ". $elem['id']);
				$status="update";
			}else{
				$result = $this->db->query("INSERT INTO `company_setup_tb`
				(`cpny_api_id`,
				`comp_set_code`,
				`comp_set_shortname`,
				`comp_set_status_code`,
				`comp_set_company_name`,
				`comp_set_address`,
				`comp_set_office_number`,
				`comp_set_website`, 
				`comp_bil_vat_type`,
				`comp_contr_effectivity`,
				`comp_contr_expiration`
				) VALUES (
				".$elem['id'].",
				'".$elem['companycode2']."',
				'".$elem['initial']."',
				'".$elem['statuscode']."',
				'".$elem['companyname']."',
				'".$elem['streetaddress']."',
				'".$elem['telephone']."',
				'".$elem['website']."',
				'".$elem['vattype']."',
				'".date("Y-m-d",strtotime($elem['effectivity']))."',
				'".date("Y-m-d",strtotime($elem['expiry']))."'
				)");
				$status="added";
			}
		}	
	    echo json_encode(array("action"=>$result,"status"=>$status)); 
	}

	private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public function compSetGenerateCode(){
       echo json_encode(strtoupper($this->generateRandomString(10)));
	}
	
	public function compSetGetData(){
	    $id = $this->encryption->decrypt($this->input->post('id'));
	    $page = $this->input->post('page');
	    
		$data = $this->account_model->get_by_id($id);
		$data->comp_setup_user_id = $this->encryption->encrypt($data->comp_setup_user_id);
		$data->company_setup_id = $this->encryption->encrypt($data->company_setup_id);
		echo json_encode($data);
	}

	public function comp_set_code_list(){
		$arr = array();
		$q = $this->db->query('select * from company_setup_tb ');
		if($q->num_rows() > 0){
			foreach($q->result_array() as $row){
				$arr[] = '<option value="'.$row['comp_set_company_name'].'" 
				data-value="'.$row['company_setup_id'].'" 
				data-cmpny="'.$row['comp_set_company_name'].'">'
				.$row['comp_set_company_name'].' | '.$row['comp_set_code'].' | '.$row['comp_contr_effectivity'].
				'</option>';
			}
		}
		echo json_encode($arr);
	}
	
	public function member_type_list(){
		$arr = array();
		$q = $this->db->query('select * from member_type_tb');
		$arr[] = '<option value=""></option>';
		if($q->num_rows() > 0){
			foreach($q->result_array() as $row){
				$arr[] = '<option value="'.$row['member_type'].'"
				data-mmbr="'.$row['member_type'].'" 
				data-value="'.$row['member_type_id'].'">'.$row['member_type'].'</option>';
			}
		}
		echo json_encode($arr);
	}

	public function findCmpySetpInfo(){
		$q = $this->db->query("select * from company_setup_tb where company_setup_id = ".
		$this->encryption->decrypt($this->input->post('id')));
		echo json_encode($q->row());
	}

	public function findMemberInfo(){
		$q = $this->db->query("select * from member_type_tb where member_type_id = ".
		$this->encryption->decrypt($this->input->post('id')));
		echo json_encode($q->row());
	}

	public function comp_contr_expiration_func(){
	    if(date('Y-m-d',strtotime($this->input->post('comp_contr_effectivity'))) > date('Y-m-d',strtotime($this->input->post('comp_contr_expiration')))){
	        $this->form_validation->set_message('comp_contr_expiration_func', 'Expiration Date must not less than to Effectivity Date.');
			return false;
	    }
	    return true;
	}
	
	public function comp_contr_effectivity_func(){
	     if(date('Y-m-d',strtotime($this->input->post('comp_contr_effectivity'))) > date('Y-m-d',strtotime($this->input->post('comp_contr_expiration')))){
	        $this->form_validation->set_message('comp_contr_effectivity_func', 'Effectivity Date must not exceed to Expiration Date.');
			return false;
	    }
	    return true;
	}

	public function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		 return $url;
	}

	public function getMemberModInfo(){
		$q = $this->db->query("select * from company_setup_tb where company_setup_id = ".
		$this->encryption->decrypt($this->input->post('id')));
		echo json_encode($q->row());
	}

	public function CompSetupFormValidate(){
	    $page = $this->input->post('page');
        if($this->input->post('method') != ''){
            $method =  $this->input->post('method');
        }else{
            $method = null;
        }
	    
	    $this->_validateSetupCompany($page,$method);
	    
	    if($page == "save"){
	        
	        $data = array(
    			'comp_set_code' => $this->input->post('comp_set_code'),
    			'comp_set_shortname' => $this->input->post('comp_set_shortname'),
    			'comp_set_type' => $this->input->post('comp_set_type'),
    			'comp_set_company_name' => $this->input->post('comp_set_company_name'),
    			'comp_set_address' => $this->input->post('comp_set_address'),
    			'comp_set_office_number' => $this->input->post('comp_set_office_number'),
    			'comp_set_fax_number' => $this->input->post('comp_set_fax_number'),
    			'comp_set_website' => $this->input->post('comp_set_website'),
    			'comp_set_sec_number' => $this->input->post('comp_set_sec_number'),
    			'comp_set_tax_id_or_tin_no' => $this->input->post('comp_set_tax_id_or_tin_no'),
    			'comp_set_dti_no' => $this->input->post('comp_set_dti_no'),
    			'comp_set_payee_code' => $this->input->post('comp_set_payee_code'),
    			
    			'comp_bil_billing_setup' => $this->input->post('comp_bil_billing_setup'),
    			'comp_bil_send_billing_setup' => $this->input->post('comp_bil_send_billing_setup'),
    			'comp_bil_send_billing_to' => $this->input->post('comp_bil_send_billing_to'),
    			'comp_bil_vat_type' => $this->input->post('comp_bil_vat_type'),
    			'comp_bil_vtax_code' => $this->input->post('comp_bil_vtax_code'),
    			'comp_bil_vtax_code_rate' => $this->input->post('comp_bil_vtax_code_rate'),
    			'comp_bil_ip_admin_fee_rate' => $this->input->post('comp_bil_ip_admin_fee_rate'),
    			'comp_bil_ip_admin_fee_amount' => $this->input->post('comp_bil_ip_admin_fee_amount'),
    			'comp_bil_op_admin_fee_rate' => $this->input->post('comp_bil_op_admin_fee_rate'),
    			'comp_bil_op_admin_fee_amount' => $this->input->post('comp_bil_op_admin_fee_amount'),
    			'comp_bil_re_admin_fee_rate' => $this->input->post('comp_bil_re_admin_fee_rate'),
    			'comp_bil_re_admin_fee_amount' => $this->input->post('comp_bil_re_admin_fee_amount'),
    			'comp_bil_dc_admin_fee_rate' => $this->input->post('comp_bil_dc_admin_fee_rate'),
    			'comp_bil_dc_admin_fee_amount' => $this->input->post('comp_bil_dc_admin_fee_amount'),
    			'comp_bil_naf_rate' => $this->input->post('comp_bil_naf_rate'),
    			'comp_bil_naf_amount' => $this->input->post('comp_bil_naf_amount'),
    			
    			'comp_contr_effectivity' => date('Y-m-d', strtotime($this->input->post('comp_contr_effectivity'))),
    			'comp_contr_expiration' => date('Y-m-d', strtotime($this->input->post('comp_contr_expiration'))),
    			'comp_contr_corporate_limit' => $this->input->post('comp_contr_corporate_limit'),
    			'comp_contr_remarks' => $this->input->post('comp_contr_remarks'),
    			'comp_setup_user_id' => $this->encryption->decrypt($this->input->post('comp_setup_user_id'))
    		);
    		if($method == 'add'){
    		    $result = $this->db->insert('company_setup_tb', $data);
    		}else if($method == 'update'){
    		    $this->db->update('company_setup_tb', $data, array('company_setup_id' => $this->encryption->decrypt($this->input->post('edit_id'))));
    		}
	    }
	   echo json_encode(array("status" => TRUE));
	}

	private function _validateSetupCompany($type,$method){
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
		if($type == "comp_basic_info"){
		    if($method != "update"){
			$this->form_validation->set_rules('comp_set_code', 'Code', 'required|is_unique[company_setup_tb.comp_set_code]',
				array(
					'required'      => 'You have not provided %s.',
					'is_unique'     => 'That %s is taken. Try to generate a new one.'
				));
		    }else{
		        	$this->form_validation->set_rules('comp_set_code', 'Code', 'required');
		    }
			$this->form_validation->set_rules('comp_set_type', 'Type', 'required');
			$this->form_validation->set_rules('comp_set_company_name', 'Company Name', 'required');
			$this->form_validation->set_rules('comp_set_sec_number', 'SEC Number', 'required');
			$this->form_validation->set_rules('comp_set_tax_id_or_tin_no', 'TAX ID', 'required');
			$this->form_validation->set_rules('comp_set_dti_no', 'DTI No.', 'required');
			$this->form_validation->set_rules('comp_set_payee_code', 'Payee Code', 'required');
		}else if($type == "comp_bil_info"){
		    $this->form_validation->set_rules('comp_bil_billing_setup', 'Billing Setup', 'required');
			$this->form_validation->set_rules('comp_bil_send_billing_setup', 'Send Billing Setup', 'required');
			$this->form_validation->set_rules('comp_bil_vat_type', 'VAT Type', 'required');
			$this->form_validation->set_rules('comp_bil_send_billing_to', 'Send Billing To', 'required');
		}else if($type == "comp_cont_info"){
		    $this->form_validation->set_rules('comp_contr_effectivity', 'Effectivity', 'required|callback_comp_contr_effectivity_func');
		    $this->form_validation->set_rules('comp_contr_expiration', 'Expiration', 'required|callback_comp_contr_expiration_func');
		}

		if ($this->form_validation->run() == FALSE){
			foreach ($this->form_validation->error_array() as $key => $value) {
				$data['inputerror'][] = $key;
				$data['error_string'][] = $value;
				$data['status'] = FALSE;
			}
		}else{
			$data['inputerror'][] = '';
			$data['error_string'][] = '';
			$data['status'] = TRUE;
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function BenefitSetupFormValidate(){
		$session = $this->session->userdata('logged_in');
		$page = $this->input->post('page');
        if($this->input->post('method') != ''){
            $method =  $this->input->post('method');
        }else{
            $method = null;
        }
	  
		$this->_validateSetupBenefit($page,$method);

		$data_bet_limit_arr = array();
		$member_id_arr = array();
		$data = array();
		$company_id_arr = ""; 

		if($page == "save"){
			$company_id_arr = implode(",",$this->input->post('company_id_arr'));
	        $data = array(
				'company_setup_id' => $company_id_arr,
				'benefits_created_by' => $this->encryption->decrypt($session['user_id']),
			);

				if($method == 'add'){
					$result = $this->db->insert('benefits_tb', $data);
					$bet_last_insert_id = $this->db->insert_id();
				}
			
				$member_limit_arr = explode(",",$this->input->post('member_limit_arr'));
				$member_id_arr = explode(",",$this->input->post('member_limit_arr'));
				for($ie = 0;$ie < count($member_limit_arr);$ie++){
				$data_bet_limit_arr[] = $data_bet_limit = array(
					'benefits_id' => $bet_last_insert_id,
					'member_type_id' => $member_id_arr[$ie],
					'limit_abl' => $this->input->post('member_abl_'.$member_limit_arr[$ie]),
					'limit_mxbl' => $this->input->post('member_mxbl_'.$member_limit_arr[$ie]),
					'limit_lbl' => $this->input->post('member_lbl_'.$member_limit_arr[$ie]),
					'limit_pel' => $this->input->post('member_pel_'.$member_limit_arr[$ie]),
					'limit_cil' => $this->input->post('member_cil_'.$member_limit_arr[$ie]),
					'limit_mbl' => $this->input->post('member_mbl_'.$member_limit_arr[$ie]),
					'limit_opd' => $this->input->post('member_opd_'.$member_limit_arr[$ie]),
					'limit_age' => $this->input->post('member_age_limit_'.$member_limit_arr[$ie]),

					'inclu_abl' => $this->input->post('abl_inclu_'.$member_limit_arr[$ie]),
					'inclu_mxbl' => $this->input->post('mxbl_inclu_'.$member_limit_arr[$ie]),
					'inclu_lbl' => $this->input->post('lbl_inclu_'.$member_limit_arr[$ie]),
					'inclu_pel' => $this->input->post('pel_inclu_'.$member_limit_arr[$ie]),
					'inclu_cil' => $this->input->post('cil_inclu_'.$member_limit_arr[$ie]),
					'inclu_mbl' => $this->input->post('mbl_inclu_'.$member_limit_arr[$ie]),
					'inclu_opd' => $this->input->post('opd_inclu_'.$member_limit_arr[$ie]),
					'inclu_room_and_board' => $this->input->post('room_and_board_inclu_'.$member_limit_arr[$ie]),
					'inclu_dental' => $this->input->post('dental_inclu_'.$member_limit_arr[$ie]),
					'inclu_ape' => $this->input->post('ape_inclu_'.$member_limit_arr[$ie]),
					'inclu_ecu' => $this->input->post('ecu_inclu_'.$member_limit_arr[$ie]),
					'mod_modality' => $this->input->post('modality_mod_'.$member_limit_arr[$ie]),
					'mod_amount' => $this->input->post('amount_mod_'.$member_limit_arr[$ie]),
					'mod_remarks' => $this->input->post('remark_mod_'.$member_limit_arr[$ie]),

				);
				}
				if($method == 'add'){
					$this->db->insert_batch('benefits_limit_tb', $data_bet_limit_arr);
				}
				//else if($method == 'update'){
    		//     $this->db->update('company_setup_tb', $data, array('company_setup_id' => $this->encryption->decrypt($this->input->post('edit_id'))));
    		// }
		}
		echo json_encode(array("status" => TRUE,"data"=>$data_bet_limit_arr));
	}

	private function _validateSetupBenefit($type,$page){
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		// Benefits
		if($type == "phase1"){
			$this->form_validation->set_rules('comp_set_code', 'Account Name / Code ', 'required');
			$this->form_validation->set_rules('member_type', 'Member Type', 'required');
			//$this->form_validation->set_rules('age_limit', 'Age Limit', 'required|numeric');
		}else if($type == "phase2"){
			$member_limit_arr = explode(",",$this->input->post('member_limit_arr'));
		
			for($ie = 0;$ie < count($member_limit_arr);$ie++){
				//$this->form_validation->set_rules('member_abl_'.$member_limit_arr[$ie], 'ALB '.$member_limit_arr[$ie], 'required');
			}
		}else if($type == "phase3"){
			/*$this->form_validation->set_rules('abl_inclu', 'ALB Inclusion', 'required');
			$this->form_validation->set_rules('mxbl_inclu', 'MXBL Inclusion', 'required');
			$this->form_validation->set_rules('lbl_inclu', 'LBL Inclusion', 'required');
			$this->form_validation->set_rules('pel_inclu', 'PEL Inclusion', 'required');
			$this->form_validation->set_rules('cil_inclu', 'CIL Inclusion', 'required');
			$this->form_validation->set_rules('mbl_inclu', 'MBL Inclusion', 'required');
			$this->form_validation->set_rules('opd_inclu', 'OPD Inclusion', 'required');
			$this->form_validation->set_rules('room_and_board_inclu', 'Room and Board', 'required');
			$this->form_validation->set_rules('dental_inclu', 'Dental', 'required');
			$this->form_validation->set_rules('ape_inclu', 'APE', 'required');
			$this->form_validation->set_rules('ecu_inclu', 'ECU', 'required');*/
		}else if($type == "phase4"){

		}
		
		if ($this->form_validation->run() == FALSE){
			foreach ($this->form_validation->error_array() as $key => $value) {
				$data['inputerror'][] = $key;
				$data['error_string'][] = $value;
				$data['status'] = FALSE;
			}
		}else{
			$data['inputerror'][] = '';
			$data['error_string'][] = '';
			$data['status'] = TRUE;
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function memberSetupFormValidate(){
		$session = $this->session->userdata('logged_in');
		$page = $this->input->post('page');
		$mem_last_insert_id = '';
        if($this->input->post('method') != ''){
            $method =  $this->input->post('method');
        }else{
            $method = null;
        }
	  
		$this->_validateEnrollMem($page,$method);

		if($page == "save"){
			$data = array(
				'member_no_id' => $this->input->post('member_exst_id_enroll_mem'),
				'member_type_id' => $this->input->post('member_type_enroll_mem'),
				'member_type_subtype_id' => $this->input->post('sub_type_enroll_mem'),
				'member_hire_date' => date('Y-m-d',strtotime($this->input->post('hired_date_enroll_mem'))),
				'member_firsrt_name' => $this->input->post('first_name_enroll_mem'),
				'member_last_name' => $this->input->post('last_name_enroll_mem'),
				'member_middle_name' => $this->input->post('middle_name_enroll_mem'),
				'member_marital_status' => $this->input->post('martial_status_enroll_mem'),
				'member_gender' => $this->input->post('gender_enroll_mem'),
				'member_bday' => date('Y-m-d',strtotime($this->input->post('birth_date_enroll_mem'))),
				'member_age' => $this->input->post('age_enroll_mem'),
				'member_batch_id' => '',
				'id_status' => '0',
				'member_reference' => '',
			);

			if($method == 'add'){
				$result = $this->db->insert('member_tb', $data);
				$mem_last_insert_id = $this->db->insert_id();
			}
		}

		echo json_encode(array("status" => TRUE,"data"=>$mem_last_insert_id));
	}
	
	private function _validateEnrollMem($type,$page){
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($type == "phase1"){
			$this->form_validation->set_rules('first_name_enroll_mem', 'First Name', 'max_length[100]|callback_validateName');
			$this->form_validation->set_rules('last_name_enroll_mem', 'Last Name', 'max_length[100]|callback_validateName');
			$this->form_validation->set_rules('middle_name_enroll_mem', 'Middle Name', 'max_length[100]|callback_validateName');
			$this->form_validation->set_rules('sub_type_enroll_mem', 'Sub Type', 'required');
			$this->form_validation->set_rules('hired_date_enroll_mem', 'Hired Date', 'required');
			$this->form_validation->set_rules('martial_status_enroll_mem', 'Martial Status', 'required');
			$this->form_validation->set_rules('gender_enroll_mem', 'Gender', 'required');
			$this->form_validation->set_rules('birth_date_enroll_mem', 'Birth Date', 'required|callback_validateBirthdate');
			$this->form_validation->set_rules('age_enroll_mem', 'Age', 'required|numeric|greater_than[0]');
		}

		if ($this->form_validation->run() == FALSE){
			foreach ($this->form_validation->error_array() as $key => $value) {
				$data['inputerror'][] = $key;
				$data['error_string'][] = $value;
				$data['status'] = FALSE;
			}
		}else{
			$data['inputerror'][] = '';
			$data['error_string'][] = '';
			$data['status'] = TRUE;
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	function validateBirthdate($date) {
		$this->form_validation->set_message('validateBirthdate', "Please enter a valid %s");
		$d = DateTime::createFromFormat('m/d/Y', $date);
	
		if($d && $d->format('m/d/Y') === $date || $date == ''){
			if(date('Y',strtotime($date)) < '1850'){
				$this->form_validation->set_message('validateBirthdate', "%s must be higher than 1849");
				return false;
			}else if(date('Y',strtotime('-9 years')) <= date('Y',strtotime($date))){
				$this->form_validation->set_message('validateBirthdate', "%s must be lower than ".date('Y',strtotime('-9 years'))." years");
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	function validateName($str) {
		if(!empty($str)){
		if (!preg_match("/^([a-zñÑ. ])+$/i", $str)) {
			$this->form_validation->set_message('validateName', 'The %s field can only be alphabetical characters');
			return FALSE;
		}else {
			return TRUE;
		}
		}else{
			return TRUE;
		}
	}

}
