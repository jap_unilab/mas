<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Health_claims extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('system_info_model');
		$this->load->library('encryption');
	}

	public function index(){
	
	}

	function health_claims_list(){
		if($session = $this->session->userdata('logged_in')){
		    $arr['db'] = new \stdClass();
		    $arr['db']->page_header = array("title"=>"Health Claims Page - Health Claims | MASS");
			$arr['db']->main_header =  array("avatar"       =>  $this->system_info_model->fetchAvatar($session['user_id']),
			                                "name_title"   =>  $this->system_info_model->fetchNameTitle($session['user_id']),
			                                "user_info"    =>  $this->system_info_model->fetchDataUserInfo($session['user_id']),
			                                "sys_info"     =>  $this->system_info_model->fetchData()
			                          );
			
			$arr['db']->main_sub_header = array("health_claims"=>TRUE,  // for tabs
			                                    "responsibility" => $this->system_info_model->fetchDataResponsibility($session['user_id']));      
			
			
			$this->load->view('health_claims/health_claims_view',$arr);
		}else{
				redirect('login', 'refresh');
		}
	}

	function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		 return $url;
	}
}
