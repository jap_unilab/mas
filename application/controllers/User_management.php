<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_management extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('system_info_model');
		$this->load->model('user_management_model');
		$this->load->library('encryption');
	}

	public function index(){
	
	}

	function user_list(){
		if($session = $this->session->userdata('logged_in')){
		    $arr['db'] = new \stdClass();
		    $arr['db']->page_header = array("title"=>"User Management - Settings | MAS");
			$arr['db']->main_header = array("avatar"       =>  $this->system_info_model->fetchAvatar($session['user_id']),
			                                "name_title"   =>  $this->system_info_model->fetchNameTitle($session['user_id']),
			                                "user_info"    =>  $this->system_info_model->fetchDataUserInfo($session['user_id']),
			                                "sys_info"     =>  $this->system_info_model->fetchData()
			                                );
			
			$arr['db']->main_sub_header = array("settings"=>array("user_management"=>TRUE), // for tabs
			                                    "responsibility" => $this->system_info_model->fetchDataResponsibility($session['user_id'])); 
			$arr['db']->page_body = array("img"=>$this->get_gravatar('test@gmail.com')); 
			
			$this->load->view('user_management_view',$arr);
		}else{
				redirect('login', 'refresh');
		}
	}
	
	function ajax_list(){
	    $list = $this->user_management_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $user_management_model) {
			$user_status = $this->input->post('user_status');
	    	$umm_upd_info_pass_avtr = $this->input->post('umm_upd_info_pass_avtr');
	    	$umm_assgn_res = $this->input->post('umm_assgn_res');
	    	
			$id_enc = $this->encryption->encrypt($user_management_model->id);
			$usr_prf_path = $user_management_model->image;
			$usr_prf_path_img = '<img src="'.HTTP_ASSETS_PATH.'custom/img/person.jpg" width="30px" class="img-circle" alt="'.$user_management_model->first_name.' profile image">';
			if (@getimagesize('./assets/custom/img/user_prof/'.$usr_prf_path)) {
			    $usr_prf_path_img = '<img src="'.HTTP_ASSETS_PATH.'custom/img/user_prof/'.$usr_prf_path.'" width="30px" class="img-circle" alt="'.$user_management_model->first_name.' profile image">';
            }
            
			$no++;
            $row = array();
			
			$row[] = ""; // checkbox
			$row[] = $id_enc;
			$row[] = $usr_prf_path_img;
			$row[] = ($user_management_model->first_name == '') ? '<i style="color:#888">No Name</i>' :  $user_management_model->first_name.' '.$user_management_model->last_name;
			$row[] = $user_management_model->username;
			$row[] = $user_management_model->email;
			$row[] = ($user_management_model->user_type == '0' ? 'Super Administrator' : ($user_management_model->user_type == '1' ? 'Administrator' : ($user_management_model->user_type == '2' ? 'Processor' : 'Normal User')));
			//$row[] = ($user_management_model->user_email_verified == '1' ? 'Verified' : 'Not Verified');
			//$row[] = ucfirst($user_management_model->user_status);
			$row[] = date("F j, Y g:i a", strtotime($user_management_model->created_at));
			$row[] = ($user_management_model->updated_at == null ? 'No Update' : date("F j, Y g:i a", strtotime($user_management_model->updated_at)));
			$row[] = '<center><div class="btn-group" id="usrmgmntDrpDwn">
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <i class="fa fa-cog" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li class="'.($umm_upd_info_pass_avtr == "FALSE" ? "hidden" : "").'"><a href="javascript:void(0)" tabindex="-1" onclick=editUserProf("view","'.$id_enc.'")><i class="fa fa-user-circle-o"></i> Update Profile</a></li>
                          <li class="'.($umm_upd_info_pass_avtr == "FALSE" ? "hidden" : "").'"><a href="javascript:void(0)" tabindex="-1" onclick=changeUserPwd("view","'.$id_enc.'")><i class="fa fa-key" aria-hidden="true"></i>Reset Password</a></li>
						  <li class="'.($umm_upd_info_pass_avtr == "FALSE" ? "hidden" : "").'"><a href="javascript:void(0)" tabindex="-1" onclick=uploadUserPhoto("view","'.$id_enc.'","'.$usr_prf_path.'")><i class="fa fa-picture-o" aria-hidden="true"></i>Profile Image</a></li>
						  <li class="divider '.($umm_upd_info_pass_avtr == "FALSE" ? "hidden" : "").'"></li>
						  <li class="dropdown-submenu pull-left hidden">
                            <a href="javascript:void(0)"  data-toggle="dropdown"><i class="fa fa-caret-left" aria-hidden="true" ></i>&nbsp; Logs</a>
                            <ul class="dropdown-menu ">
                                <li><a href="#" tabindex="-1"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
								<li><a href="#"><i class="fa fa-hourglass-3" aria-hidden="true"></i>  Activity</a></li>
							</ul>
						  </li>	
						  <li class="dropdown-submenu pull-left">
                            <a href="javascript:void(0)"  data-toggle="dropdown"><i class="fa fa-caret-left" aria-hidden="true" ></i>&nbsp; Resposibility</a>
                            <ul class="dropdown-menu ">
                                <li class="'.($umm_assgn_res == "FALSE" ? "hidden" : "").'"><a href="javascript:void(0)" tabindex="-1" onclick=responUserProf("view","'.$id_enc.'","accounts")><i class="fa fa-circle" aria-hidden="true"></i> Accounts</a></li>
								<li class="'.($umm_assgn_res == "FALSE" ? "hidden" : "").'"><a href="javascript:void(0)" tabindex="-1" onclick=responUserProf("view","'.$id_enc.'","user_management")><i class="fa fa-circle" aria-hidden="true"></i> User Management</a></li>
								<li class="'.($umm_assgn_res == "FALSE" ? "hidden" : "").'"><a href="javascript:void(0)" tabindex="-1" onclick=responUserProf("view","'.$id_enc.'","health_claims")><i class="fa fa-circle" aria-hidden="true"></i> Health Claims</a></li>
							</ul>
						  </li>	
                        </ul>
                      </div></center>';
			$data[] = $row;
      
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->user_management_model->count_all(),
						"recordsFiltered" => $this->user_management_model->count_filtered(),
						"data" => $data,
				);
		
		//output to json format
		echo json_encode($output);
	}
	
	function addUserProf(){
	    $this->_validate("add");
	   
		$password = $this->hash_password($this->input->post('password'));
		$data = array(
		'flex_id' => 0,
		'user_type' => $this->input->post('user_type'),
		'first_name' => $this->input->post('first_name'),
		'last_name' => $this->input->post('last_name'),
		'middle_name' => $this->input->post('middle_name'),
		'username' => $this->input->post('username'),
		'email' =>  $this->input->post('email'),
		'password' => $password,
		'mobile_number' => $this->input->post('mobile'),
		'gender' => $this->input->post('gender'),
		'created_at' => date('Y-m-d H:i:s'),
		'created_by' => '0',
		'user_status' => 'active',
		'user_email_verified' => '0');
		$last_id = $this->user_management_model->save($data);
		
		echo json_encode(array("status" => TRUE));
	}
	
	function getInfoProfile(){
	    $id = $this->encryption->decrypt($this->input->post('id'));
		$q = $this->db->query('select * from fx_users where id = '.$id);
		if($q->num_rows() > 0){
			echo json_encode($q->row());
		}else{
			echo json_encode(null);
		}
	}
	
	function editUserProf(){
	    $this->_validate("edit");
		$edit_id = $this->encryption->decrypt($this->input->post('edit_id'));
		$data = array(
		'user_type' => $this->input->post('user_type'),
		'first_name' => $this->input->post('first_name'),
		'last_name' => $this->input->post('last_name'),
		'middle_name' => $this->input->post('middle_name'),
		'username' => $this->input->post('username'),
		'email' =>  $this->input->post('email'),
		'mobile_number' => $this->input->post('mobile'),
		'gender' => $this->input->post('gender'),
		//'user_verified' => '1',
		'user_type' => $this->input->post('user_type'),
		'user_status' => $this->input->post('user_status'),
		'updated_at' => date('Y-m-d H:i:s'));
		$edit_mdl = $this->user_management_model->update(array('id' => $edit_id), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	function changeUserPwd(){
	    $this->_validate("changePW");
		$edit_id = $this->encryption->decrypt($this->input->post('edit_id'));
		$password = $this->hash_password($this->input->post('password'));
		$data = array(
		'password' => $password
		);
		$chpw_mdl = $this->user_management_model->update(array('id' => $edit_id), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	private function hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}
	
	function uploadUserPhoto(){
		if($_SERVER['REQUEST_METHOD']=="POST"){
			$encoded = $_POST['image-data'];
			$id = $this->encryption->decrypt($this->input->post('id'));
			$imgname = $id."_".round(microtime(true));
			$imgname_wext = $id."_".round(microtime(true)).'.jpg';
			$exstImg = $this->input->post('exstImg');

			//explode at ',' - the last part should be the encoded image now
			$exp = explode(',', $encoded);
			//decode the image and finally save it
			$data = base64_decode($exp[1]);
			
			$file = './assets/custom/img/user_prof/'.$imgname.'.jpg';
			//make sure you are the owner and have the rights to write content
			$q = file_put_contents($file, $data);
			$q = $this->db->query('UPDATE `fx_users` SET
			`image`="'.$imgname_wext.'"
			 WHERE id = '.$id);
			 
			 $return_text = 0;
			if($exstImg != ""){ 
				if(file_exists('./assets/custom/img/user_prof/'.$exstImg)){
					// Remove file 
					unlink('./assets/custom/img/user_prof/'.$exstImg);
					 $return_text = 1;
				}else{
					 $return_text = 0;
				}
			}
			echo json_encode($imgname_wext);
		}else{
			echo "Error";
		}
	}

    public function deleteUserProf(){
		$delete_id = $this->encryption->decrypt($this->input->post('id'));
		$this->db->query('UPDATE `user_account_tb` SET `user_status`= "deleted" WHERE user_id = '.$delete_id);
		echo json_encode(array("status" => TRUE));
	}
	
	public function actionMultiple(){
		$arr_ids = $this->input->post('id');
		$title = strtolower($this->input->post('title'));
		if($title == 'delete'){
		    $title = 'deleted';
		}
		$arr = array();
		foreach($arr_ids as $key => $ids){
		   $id = $this->encryption->decrypt(implode(" ",$ids));
		   $arr[] = $title;
		   $this->db->query('UPDATE `fx_users` SET `user_status`= "'.$title.'" WHERE id = '.$id);
		}
		echo json_encode($arr);
	}
	
	public function responUserProf(){
        $arr = array();
        $id = $this->encryption->decrypt($this->input->post('id'));
        $umm = $this->input->post('umm');
        $umm_view = $this->input->post('umm_view');
        $umm_add_user = $this->input->post('umm_add_user');
        $umm_upd_info_pass_avtr = $this->input->post('umm_upd_info_pass_avtr');
        $umm_chg_usr_sta = $this->input->post('umm_chg_usr_sta');
        $umm_assgn_res = $this->input->post('umm_assgn_res');
        $status = false;
       
           // $q_umm_search = $this->db->query('select * from user_responsibility_tb where responsibility_name = "'.$umm_chg_usr_sta.'" and user_id = '.$id)->num_rows();
            $q_umm = ($umm != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "umm" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "umm" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"umm")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "umm" and user_id = '.$id.'';  
            $q_umm_view = ($umm_view != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "umm_view" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "umm_view" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"umm_view")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "umm_view" and user_id = '.$id.'';
            $umm_add_user = ($umm_add_user != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "umm_add_user" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "umm_add_user" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"umm_add_user")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "umm_add_user" and user_id = '.$id.''; 
            $q_umm_upd_info_pass_avtr = ($umm_upd_info_pass_avtr != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "umm_upd_info_pass_avtr" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "umm_upd_info_pass_avtr" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"umm_upd_info_pass_avtr")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "umm_upd_info_pass_avtr" and user_id = '.$id.''; 
            $q_umm_chg_usr_sta = ($umm_chg_usr_sta != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "umm_chg_usr_sta" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "umm_chg_usr_sta" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"umm_chg_usr_sta")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "umm_chg_usr_sta" and user_id = '.$id.''; 
            $q_umm_assgn_res = ($umm_assgn_res != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "umm_assgn_res" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "umm_assgn_res" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"umm_assgn_res")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "umm_assgn_res" and user_id = '.$id.''; 
                
            $r_umm = $this->db->query($q_umm);
            $r_umm_view = $this->db->query($q_umm_view);
            $r_umm_add_user = $this->db->query($umm_add_user);
            $r_umm_upd_info_pass_avtr = $this->db->query($q_umm_upd_info_pass_avtr);
            $r_umm_chg_usr_sta = $this->db->query($q_umm_chg_usr_sta);
            $r_umm_assgn_res = $this->db->query($q_umm_assgn_res);
            $status = true;
            $message = "good";
        
       
	   echo json_encode(array("status"=>$status,"message"=>$message)); 
	} 
	
	function responAccountsMod(){
	    $arr = array();
        $id = $this->encryption->decrypt($this->input->post('id'));
        $accts_mm = $this->input->post('accts_mm');
        $accts_crt = $this->input->post('accts_crt');
        $accts_all_sb = $this->input->post('accts_all_sb');
        $accts_mmbr_sb = $this->input->post('accts_mmbr_sb');
        $status = false;
        
            $q_accts_mm = ($accts_mm != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "accts_mm" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "accts_mm" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"accts_mm")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "accts_mm" and user_id = '.$id.'';  
            $q_accts_crt= ($accts_crt != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "accts_crt" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "accts_crt" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"accts_crt")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "accts_crt" and user_id = '.$id.'';
            $q_accts_all_sb = ($accts_all_sb != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "accts_all_sb" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "accts_all_sb" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"accts_all_sb")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "accts_all_sb" and user_id = '.$id.''; 
            $q_accts_mmbr_sb = ($accts_mmbr_sb != null) ? 
                    ($this->db->query('select * from user_responsibility_tb where responsibility_name = "accts_mmbr_sb" and user_id = '.$id)->num_rows() != 0 ? 
                    'select * from user_responsibility_tb where responsibility_name = "accts_mmbr_sb" and user_id = '.$id.'' :
                'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"accts_mmbr_sb")') : 
                'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "accts_mmbr_sb" and user_id = '.$id.''; 
          
                
            $r_accts_mm = $this->db->query($q_accts_mm);
            $r_accts_crt = $this->db->query($q_accts_crt);
            $r_accts_all_sb = $this->db->query($q_accts_all_sb);
            $r_accts_mmbr_sb = $this->db->query($q_accts_mmbr_sb);
            $status = true;
            $message = "good";
       
	   echo json_encode(array("status"=>$status,"message"=>$message)); 
	}
	
	public function responHlthClms(){
	    $arr = array();
        $id = $this->encryption->decrypt($this->input->post('id'));
        $hlth_clms_mm = $this->input->post('hlth_clms_mm');
        $status = false;
        
        $q_hlth_clms_mm = ($hlth_clms_mm != null) ? 
                ($this->db->query('select * from user_responsibility_tb where responsibility_name = "hlth_clms_mm" and user_id = '.$id)->num_rows() != 0 ? 
                'select * from user_responsibility_tb where responsibility_name = "hlth_clms_mm" and user_id = '.$id.'' :
            'INSERT INTO `user_responsibility_tb`(`user_id`, `responsibility_name`) VALUES ('.$id.',"hlth_clms_mm")') : 
            'DELETE FROM `user_responsibility_tb` WHERE responsibility_name = "hlth_clms_mm" and user_id = '.$id.'';  
      
        $r_hlth_clms_mm = $this->db->query($q_hlth_clms_mm);
        $status = true;
        $message = "good";
        
       
	   echo json_encode(array("status"=>$status,"message"=>$message)); 
	}
	
	public function getResponsibilityAjax(){
	    $id = $this->encryption->decrypt($this->input->post('id'));
	    $q = $this->db->query('select * from user_responsibility_tb where user_id = '.$id);
	    $arr = array();
	    if($q->num_rows() > 0){
	        foreach($q->result_array() as $row){
	            $arr[] = $row['responsibility_name'];
	        }
	    }
	    echo json_encode($arr); 
	}

	function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		 return $url;
	}
	
	function validateName($str) {
		if(!empty($str)){
		if (!preg_match("/^([a-zñÑ. ])+$/i", $str)) {
			$this->form_validation->set_message('validateName', 'The %s field can only be alpha numeric');
			return FALSE;
		}else {
			return TRUE;
		}
		}else{
			return TRUE;
		}
	}
	
	function valid_password($password = ''){
        $password = trim($password);
        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
        if (empty($password)){
            $this->form_validation->set_message('valid_password', 'The {field} field is required.');
            return FALSE;
        }
        return TRUE;
    }
	
	private function _validate($type){
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		
		if($type == "add"){
			$this->form_validation->set_rules('first_name', 'First Name', 'required|callback_validateName');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required|callback_validateName');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[5]|max_length[25]|alpha_numeric|is_unique[fx_users.username]',
				array(
					'required'      => 'You have not provided %s.',
					'is_unique'     => 'That %s is taken. Try another.'
				));
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[fx_users.email]', 
				array(
				'is_unique' => 'That %s is taken. Try another.'
				));
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_valid_password');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|max_length[25]|matches[password]');	
			
		}else if($type == "edit"){
		    
			$this->form_validation->set_rules('first_name', 'First Name', 'required|callback_validateName');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required|callback_validateName');
			$edit_id = $this->encryption->decrypt($this->input->post('edit_id'));
			$rw = $this->db->query('select * from fx_users where id = '.$edit_id)->row();
			if($rw->username != $this->input->post('username')){
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[5]|max_length[25]|alpha_numeric|is_unique[fx_users.username]',
				array(
					'required'      => 'You have not provided %s.',
					'is_unique'     => 'That %s is taken. Try another.'
				));
			}
			if($rw->email != $this->input->post('email')){
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|is_unique[fx_users.email]', 
				array(
				'is_unique' => 'That %s is taken. Try another.'
				));
			}
		}else if($type == "changePW"){
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_valid_password');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|max_length[25]|matches[password]');	
		}
		
		
		if ($this->form_validation->run() == FALSE){
			foreach ($this->form_validation->error_array() as $key => $value) {
				$data['inputerror'][] = $key;
				$data['error_string'][] = $value;
				$data['status'] = FALSE;
			}
		}else{
			$data['inputerror'][] = '';
			$data['error_string'][] = '';
			$data['status'] = TRUE;
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}
