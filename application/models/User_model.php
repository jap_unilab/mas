<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model{
	
	public function resolve_user_login($username){
		$this->db->where('(username = "'.$username.'")');
		$query = $this->db->get('fx_users');
        if ($query->num_rows() > 0){
            return true;
        }
        else{
        return false;
		}
	}

	public function resolve_user_password_login($username, $password){
		$this->db->select('password');
		$this->db->from('fx_users');
		$this->db->where('(username = "'.$username.'")');
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
	}

	public function get_user_id_from_username($username) {
		$this->db->select('id');
		$this->db->from('fx_users');
		$this->db->where('(username = "'.$username.'")');
		return $this->db->get()->row('id');
	}

	public function get_user($user_id) {
		$this->db->from('fx_users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
	}

	public function register_user(){
		$query = $this->db->get('fx_users');
    if ($query->num_rows() > 0){
			$user_type = '1';
		}else{
			$user_type = '0';
		}
		
	
		$data = array(
			'flex_id' => 0,
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->hash_password($this->input->post('password')),
			'user_type' => $user_type,
			'created_at' => date('Y-m-d H:i:s'),
			'created_by' => '0',
			'user_status' => 'active',
			'user_email_verified' => '0'
		);
		$result = $this->db->insert('fx_users', $data);
		$insert_id = $this->db->insert_id();
		$this->check($this->input->post('agreement'),$insert_id);
		return $result;
	}

	public function check_email($email){
		$this->db->where('(email = "'.$email.'")');
		$query = $this->db->get('fx_users');
    if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function check_date_reset_email($email){
		$this->db->select('pw_reset_date');
		$this->db->from('fx_users');
		$this->db->where('(email = "'.$email.'")');
		if($this->db->get()->row('pw_reset_date') != date('Y-m-d')){
			$this->db->query('UPDATE `fx_users` SET `pw_reset_num`="0", pw_reset_date = "'.date('Y-m-d').'" WHERE email = "'.$email.'"');
		}
	}

	public function no_reset_email($email){
		$this->db->select('pw_reset_num');
		$this->db->from('fx_users');
		$this->db->where('(email = "'.$email.'")');

		if($this->db->get()->row('pw_reset_num') < 3){
			return true;
		}else{
			return false;
		}
	}

	public function add_no_reset_email($email,$en){
	    $num = 0;
		$this->db->select('pw_reset_num');
		$this->db->from('fx_users');
		$this->db->where('(email = "'.$email.'")');
		$num = $this->db->get()->row('pw_reset_num') + 1;

	    $result = $this->db->query('UPDATE `fx_users` SET `pw_reset_num`=pw_reset_num + 1, rem_token="'.$en.'", pw_reset_date = "'.date('Y-m-d').'" WHERE email = "'.$email.'"');
		return $result;
	}
	
	public function change_password_and_reset_no_attemp($email,$password){
	    $password = $this->hash_password($password);

	    $result = $this->db->query('UPDATE `fx_users` SET 
	    `password`="'.$password.'",
	     rem_token = "",
	    `pw_reset_num`= "0", 
	     pw_reset_date = "'.date('Y-m-d').'" WHERE email = "'.$email.'"');
		return $result;
	}
	
	public function check_code($code){
	    $status = FALSE;
	    $email = null;
		$q = $this->db->query("select rem_token, email from fx_users where DATE_ADD(`pw_reset_date` , INTERVAL 1 DAY) > NOW()");
	    if($q->num_rows() > 0){
			foreach($q->result_array() as $row){
			    if($code == $this->encryption->decrypt($row['rem_token'])){
			        $status = TRUE;  
			        $email = $row['email'];
			    }
			    
			}
	   }
	   return array("status"=>$status,"email"=>$email);
	}
	
	private function check($agree,$user_id){
	    if($agree == "super"){
	        // User Management
	        $data = array(
                array('user_id' =>$user_id ,'responsibility_name' => 'umm'),
                array('user_id' =>$user_id ,'responsibility_name' => 'umm_view'),
                array('user_id' =>$user_id ,'responsibility_name' => 'umm_add_user'),
                array('user_id' =>$user_id ,'responsibility_name' => 'umm_upd_info_pass_avtr'),
                array('user_id' =>$user_id ,'responsibility_name' => 'umm_chg_usr_sta'),
                array('user_id' =>$user_id ,'responsibility_name' => 'umm_assgn_res'),
            );
            $this->db->insert_batch('user_responsibility_tb', $data); 
	    }
	}

	private function hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}

	private function verify_password_hash($password, $hash) {
		return password_verify($password, $hash);
	}
    	
}
?>