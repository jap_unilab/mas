<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model2 extends CI_Model {

    var $table = 'member_tb';
    var $tableas = 'member_tb m';
	var $column_order = array(null,null,
	    'm.member_id',
    	'm.member_hire_date',
        'm.member_firsrt_name',
        'm.member_batch_id',
        'm.id_status',
        'm.member_date',
        'm.member_reference'
	);
	
	var $column_search = array(
	    'm.member_id',
	    'DATE_FORMAT(m.member_hire_date, "%m/%d/%Y")',
        'CONCAT(m.member_last_name,", ",m.member_firsrt_name)',
        'm.member_id',
        'm.member_batch_id',
        'm.id_status',
        'm.member_date',
        'm.member_reference'
	);
	
	var $order = array('m.member_last_name' => 'asc'); // default order 

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query(){
        $this->db->select(
            'm.member_id as member_id,
            m.member_hire_date as member_hire_date,
            CONCAT(m.member_last_name,", ",m.member_firsrt_name) as name,
            mt.member_type as member_type,
            m.member_batch_id,
            m.id_status,
            m.member_date,
            m.member_reference
            ');
        $this->db->join('member_type_tb mt', 'mt.member_type_id = m.member_type_id', 'left');
		$this->db->from($this->tableas);
		
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id){
	    $this->db->from($this->table);
		$this->db->where('company_setup_id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	
	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('user_id', $id);
		$this->db->delete($this->table);
	}
	
}

