<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class System_info_model extends CI_Model{
	
	function fetchData(){
		$q = $this->db->query("select * from system_info_tb");
		return $q->row();
	}
	
	function fetchDataUserInfo($id){
	    $id = $this->encryption->decrypt($id);
		$q = $this->db->query("select id, first_name, middle_name, last_name, image, user_type  from fx_users where id = ".$id);
		$q->row()->id = $this->encryption->encrypt($q->row('id'));
		return $q->row();
	}
	
	function fetchAvatar($id){
	    $id = $this->encryption->decrypt($id);
		$q = $this->db->query("select image, first_name, last_name from fx_users where id = ".$id);
		$image = $q->row('image'); $name = $q->row('first_name').' '.$q->row('last_name');
		
		$usr_prf_path_img = '<img src="'.HTTP_ASSETS_PATH.'custom/img/person.jpg" width="30px" class="img-circle" alt="'.$name.' profile image">';
		if (@getimagesize('./assets/custom/img/user_prof/'.$image)) {
		    $usr_prf_path_img = '<img src="'.HTTP_ASSETS_PATH.'custom/img/user_prof/'.$image.'" width="30px" class="img-circle" alt="'.$name.' profile image">';
        }
        return $usr_prf_path_img;
	}
	
	function fetchNameTitle($id){
	    $id = $this->encryption->decrypt($id);
	    $q = $this->db->query("select username, first_name, last_name from fx_users where id = ".$id);
	    $name_title = "";
	    	if($q->row('first_name') == ''){
	    	    $name_title = $q->row('username');
	    	}else{
	    	    $name_title = ucwords($q->row('first_name').' '.$name_title = $q->row('last_name'));
	    	}
		return $name_title;
	}
	
	function fetchDataResponsibility($id){
	    $id = $this->encryption->decrypt($id);
	    $q = $this->db->query('select * from user_responsibility_tb where user_id = '.$id);
	    $arr = array();
	    if($q->num_rows() > 0){
	        foreach($q->result_array() as $row){
	            $arr[] = $row['responsibility_name'];
	        }
	    }
	    return $arr; 
	}
    	
}
?>