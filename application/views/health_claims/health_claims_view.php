<!DOCTYPE html>
<html>
    
<?php 
$arr['db'] = isset($db->page_header) ? $db->page_header : array();
$this->load->view('include/header',$arr); 
?>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <?php 
  $arr['db'] = isset($db->main_header) ? $db->main_header : array();
  $this->load->view('include/main_header',$arr); 
  ?> 


  <!-- Full Width Column -->
  <div class="content-wrapper">
  <?php 
  $arr['db'] = isset($db->main_sub_header) ? $db->main_sub_header : array();
  $this->load->view('include/main_sub_header',$arr); 
  ?>

  <div class="">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="" >
        <h3 style="margin:0;padding:0;font-size:20px;font-weight:600;margin-left:-25px" class="pull-left">
        <ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0;">
          <li><a href="#">&emsp; Health Claims</a></li>
        </ol>  
        </h3>

        <div class="pull-right " style="margin-top:-5px;margin-bottom:10px">
          <button class="btn btn-default btn-md" onClick="comp_set('view')">Create</button>&nbsp;
        </div>
      </section>

      <!-- Main content -->
      <section class="content" >
       

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="col-xs-6" style="margin:0;padding:0">
              <h3 class="box-title" style="margin-top:4px;margin-left:4px;margin-bottom:2px">Health Module</h3>
            </div>
            <div class="col-xs-5 pull-right" style="margin:0;padding:0">
              <div class="input-group" >
                <input type="text" id="searchEvntMtr" class="form-control" placeholder="Search by characters like names, reason, etc">
                  <span class="input-group-btn">
                  <button type="submit" id="searchEvntMtrBtn" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                  </span>
                  
              </div>
              <div class="pull-right"><a href="javascript:void(0)">Advance Search</a></div>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="margin-top:0;padding-top:0">
                On Developement
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          </div>
          </div>


      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
    
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Beta Version:</b> 1.0.0 | 12.17.2018
      </div>
      <strong>Omniquest.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Company Setup</h4>
      </div>
      <div class="modal-body">
        <p>Form</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php $this->load->view('include/modals'); ?>
<!-- page script -->
<script>
  $(document).ready(function() {
  
} );

function comp_set(action){
  if(action == "view"){
    $('#myModal').modal('show');
  }
}
</script>

</body>
</html>
