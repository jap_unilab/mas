<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($db['title']) ? $db['title'] : ''; ?></title> 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <!--<link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/square/blue.css">
   <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/select2/dist/css/select2.min.css">
  <!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.css">-->
  
  <style>
    .dataTables_filter label { display: none; }
    	.cropit-preview {
		background-color: #f8f8f8;
		background-size: cover;
		border: 1px solid #ccc;
		border-radius: 3px;
		margin-top: 7px;
		width: 250px;
		height: 250px;
	}
	.cropit-preview-background {
	  opacity: .2;
	}

	input.cropit-image-zoom-input {
	  position: relative;
	}

	#image-cropper {
	  overflow: hidden;
	}
	.cropit-preview-image-container {
		cursor: move;
	}

	.image-size-label {
		margin-top: 10px;
	}

	#myModalProfImage input {
		display: block;
	}

	#myModalProfImage button[type="submit"] {
		margin-top: 10px;
	}
	
#usrmgmntDrpDwn .dropdown-submenu {
    position:relative;
}
#usrmgmntDrpDwn .dropdown-submenu>.dropdown-menu {
    top:0;
    left:100%;
    margin-top:-6px;
    margin-left:-1px;
    -webkit-border-radius:0 6px 6px 6px;
    -moz-border-radius:0 6px 6px 6px;
    border-radius:0 6px 6px 6px;
}
#usrmgmntDrpDwn .dropdown-submenu:hover>.dropdown-menu {
    display:block;
}
#usrmgmntDrpDwn .dropdown-submenu>a:after {
    display:block;
    content:" ";
    float:left;
    width:0;
    height:0;
    border-color:transparent;
    border-style:solid;
    border-width:5px 0 5px 5px;
    border-left-color:#ffffff;
    margin-top:5px;
    margin-right:-10px;
}
#usrmgmntDrpDwn .dropdown-submenu:hover>a:after {
    border-left-color:#ffffff;
}
#usrmgmntDrpDwn .dropdown-submenu.pull-left {
    float:none;
}
#usrmgmntDrpDwn .dropdown-submenu.pull-left>.dropdown-menu {
    left:-100%;
    margin-left:0;
    -webkit-border-radius:6px 0 6px 6px;
    -moz-border-radius:6px 0 6px 6px;
    border-radius:6px 0 6px 6px;
}

.bs-wizard {margin-top: 40px;}

/* old to new color
#fbe8aa - #b3e5fc
#fbbd19 - #03a9f4
*/

/*Form Wizard*/
.bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
.bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
.bs-wizard > .bs-wizard-step + .bs-wizard-step {}
.bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
.bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
.bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #b3e5fc; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
.bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #03a9f4; border-radius: 50px; position: absolute; top: 8px; left: 8px; } 
.bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #b3e5fc;}
.bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
.bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
.bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
.bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
.bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
.bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
.bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }

/*Benefit code select 2*/
.select2-container--default .select2-selection--single .select2-selection__rendered {
  padding-left:0 !important; 
}

.select2-selection--single {
  height: 100% !important;
}
.select2-selection__rendered{
  word-wrap: break-word !important;
  text-overflow: inherit !important;
  white-space: normal !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice{
    color:white !important;
    background-color:#42a5f5 !important;
}

.has-error .select2-selection {
    border: 1px solid #a94442;
    border-radius: 4px;
}

#cmpyDrpDwn .dropdown-submenu {
    position:relative;
}
#cmpyDrpDwn .dropdown-submenu>.dropdown-menu {
    top:0;
    left:100%;
    margin-top:-6px;
    margin-left:-1px;
    -webkit-border-radius:0 6px 6px 6px;
    -moz-border-radius:0 6px 6px 6px;
    border-radius:0 6px 6px 6px;
}
#cmpyDrpDwn .dropdown-submenu:hover>.dropdown-menu {
    display:block;
}
#cmpyDrpDwn .dropdown-submenu>a:after {
    display:block;
    content:" ";
    float:left;
    width:0;
    height:0;
    border-color:transparent;
    border-style:solid;
    border-width:5px 0 5px 5px;
    border-left-color:#ffffff;
    margin-top:5px;
    margin-right:-10px;
  
}
#cmpyDrpDwn .dropdown-submenu:hover>a:after {
    border-left-color:#ffffff;
}
#cmpyDrpDwn .dropdown-submenu.pull-left {
    float:none;
}
#cmpyDrpDwn .dropdown-submenu.pull-left>.dropdown-menu {
    left:-140%;
margin-top:0px;
    margin-left:0;
    -webkit-border-radius:6px 0 6px 6px;
    -moz-border-radius:6px 0 6px 6px;
    border-radius:6px 0 6px 6px;
    background-color:#bbdefb;
}


</style>

  <!-- jQuery 3 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/js/demo.js"></script>
<script src="<?php echo HTTP_ASSETS_PATH; ?>custom/js/main.js"></script>
<script src="<?php echo HTTP_ASSETS_PATH; ?>custom/js/jquery.cropit.js"></script>

<!-- DataTables -->
<!--
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
-->
 <!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<!-- iCheck -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/icheck.min.js"></script>
<!-- InputMask -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Select2 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.js"></script>-->

</head>
