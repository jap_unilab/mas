<nav class="navbar navbar-default" style="margin-bottom:0;padding-bottom:0">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header visible-xs">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div style="margin:0 !important;padding: 0 !important" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php echo isset($db['accounts']) ? 'active' : ''; echo (array_search('accts_mm', $db['responsibility']) !== false) ? '' : ' hidden'; ?>"><a href="<?php echo base_url('accounts'); ?>">Accounts</a></li>
        <li class="<?php echo isset($db['health_claims']) ? 'active' : ''; echo (array_search('hlth_clms_mm', $db['responsibility']) !== false) ? '' : ' hidden'; ?>"><a href="<?php echo base_url('health-claims'); ?>">Health Claims</a></li>
        <li class="hidden"><a href="#">LOA</a></li>
      </ul>
      <!-- navbar-right -->
      <ul class="nav navbar-nav ">
        <li class="dropdown <?php echo isset($db['settings']) ? 'active' : ''; ?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="<?php echo isset($db['settings']['user_management']) ? 'active ' : ''; echo (array_search('umm', $db['responsibility']) !== false) ? '' : 'hidden'; ?>"><a href="<?php echo base_url('user-management'); ?>">User Management</a></li>
          
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<?php 
?>