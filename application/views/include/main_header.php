<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div >
        <div class="">
          <a href="javascript:void(0)" class="navbar-brand">
          <?php echo isset($db['sys_info']->system_title) ? $db['sys_info']->system_title : '';  ?>
          </a>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <?php echo $db['avatar']; ?>&nbsp;
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><?php echo $db['name_title']; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                    <?php echo $db['avatar']; ?>
                  <p>
                    <?php echo $db['name_title']; ?>
                    <small><?php echo ($db['user_info']->user_type == '0') ? 'Super Admin' : ($db['user_info']->user_type) == '1' ? 'Administrator' : 'Processor'; ?></small>
                  </p>
                </li>
                <!-- Menu Body -->
                
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="javascript:void(0)" class="btn btn-default btn-flat" onClick="profileModal('show')">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url('signout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>