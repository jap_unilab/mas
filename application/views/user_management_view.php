<!DOCTYPE html>
<html>
    
<?php 
$arr['db'] = isset($db->page_header) ? $db->page_header : array();
$this->load->view('include/header',$arr); 
?>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <?php 
  $arr['db'] = isset($db->main_header) ? $db->main_header : array();
  $this->load->view('include/main_header',$arr); 
  ?> 


  <!-- Full Width Column -->
  <div class="content-wrapper">
  <?php 
  $arr['db'] = isset($db->main_sub_header) ? $db->main_sub_header : array();
  $this->load->view('include/main_sub_header',$arr); 
  ?>

  <?php 
      $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
      );
  ?>
  <div class="">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="" >
        <h3 style="margin:0;padding:0;font-size:20px;font-weight:600;margin-left:-25px" class="pull-left">
        <ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0;">
          <li><a href="#">&emsp; User Management</a></li>
        </ol>  
        </h3>

        <div class="pull-right" style="margin-top:-5px;margin-bottom:10px">
         <?php 
          echo (array_search('umm_add_user', $db->main_sub_header['responsibility']) !== false) ? '<button class="btn btn-primary btn-md" onclick=addUserProf("view")><i class="fa fa-user-plus"></i> Create User</button>&nbsp;' : '<button class="btn btn-primary btn-md" style="visibility:hidden">&nbsp;</button>&nbsp;';
         ?>
        </div>
      </section>

      <!-- Main content -->
      <section class="content" >
       

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <div class="col-xs-6" style="margin:0;padding:0">
              <h3 class="box-title" style="margin-top:4px;margin-left:4px;margin-bottom:2px">User List</h3>
            </div>
            <div class="col-xs-5 pull-right" style="margin:0;padding:0">
              <div class="input-group" >
                <input type="text" id="searchEvntMtr" class="form-control" placeholder="Search and press ENTER" onkeyup="searchDefaultByEnter()">
                  <span class="input-group-btn">
                  <button type="submit" id="searchEvntMtrBtn" onclick="searchDefault()" class="btn btn-flat" ><i class="fa fa-search"></i>
                  </button>
                  </span>
              </div>
              <div class="pull-right hidden" id="searchDiv"><a href="javascript:void(0)" onClick="showAdvanceSearch()">Show Advance Search</a></div>
            </div>
            </div>
            <!-- /.box-header -->
            
            <div id="tablediv" class="box-body table-responsive" style="margin-top:0;padding-top:0">
                <!--AJAX TABLE LOADING-->
            </div><br><br><br>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->
          </div>
          </div>


      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
    
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="">
        <!--container-->
      <div class="pull-right hidden-xs">
        <b>Beta Version:</b> 1.0.0 | 12.17.2018
      </div>
      <strong>Omniquest.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>&nbsp;
			<div class="pull-right" id="btn-header"></div>&nbsp; 
			
			<h4 class="modal-title pull-left" id='mod-title'></h4>
		  </div>
		  <div class="modal-body">
			<div id='mod-content'>
			</div>
		  </div>
		  <div class="modal-footer">
			<span class="pull-left" id="notif-mess"></span>
			<div id="mod-footer"></div>
		  </div>
		</div>
	</div>
</div>
<?php $this->load->view('include/modals'); ?>
<!-- page script -->


<script type="text/javascript">
 var save_method, table;
 var cct = '<?php echo $this->security->get_csrf_hash() ?>';
 var get_csrf_token_name = '<?php echo $this->security->get_csrf_token_name() ?>';
 var http_assets_path = '<?php echo HTTP_ASSETS_PATH; ?>';

    $.ajaxSetup({
        type:'post',
        data:{ <?php echo $this->security->get_csrf_token_name() ?> :cct}
    });
 
    $(function () {
        tableAJAX();
    });
     
    function viewDeletedUser(){
    	 if($('#viewDeletedUser').data('sta') == 0){
    		 $('#viewDeletedUser').data('sta',1);
    		 $('#viewDeletedUser').removeClass('btn-danger').addClass('btn-success');
    		 $('#viewDeletedUser').html('<i class="fa fa-eye" aria-hidden="true"></i>&nbsp; View Active User');
    		  tableAJAX(3);
    	 }else{
    		 $('#viewDeletedUser').data('sta',0);
    		 $('#viewDeletedUser').removeClass('btn-success').addClass('btn-danger');
    		 $('#viewDeletedUser').html('<i class="fa fa-trash" aria-hidden="true"></i>&nbsp; View Deleted User');
    		  tableAJAX(0);
    	 }
    	
    }     
 
    function tableAJAX(action = null, status='active', make_mul_action=''){
        $("#tablediv").html(
            '<table id="ajaxTbl" class="display" width="100%" >'+
            '<thead>'+
            	'<tr>'+
                    '<th><input type="checkbox" value="" class="selectall"></th>' +
                    '<th></th>'+  // id hidden
            	    '<th></th>'+  // img
            		'<th>Full Name</th>'+  
            		'<th>Username</th>'+
            		'<th>Email Address</th>'+
            		'<th>User Type</th>'+
            		'<th>Created Date</th>'+
            		'<th>Update Date</th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</thead>'+
            '<tbody></tbody>'+
            '<tfoot hidden>'+
            	'<tr>'+
                    '<th><input type="checkbox" value="" class="selectall"></th>' +
                    '<th></th>'+
            	    '<th></th>'+ 
            		'<th>Full Name</th>'+   
            		'<th>Status</th>'+
            		'<th>Email Address</th>'+
            		'<th>User Type</th>'+
            		'<th>Created Date</th>'+
            		'<th>Update Date</th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</tfoot>'+
            '</table>'
        );
	 
    	if(action == 'showSearch'){
            $('#ajaxTbl tfoot th').each( function (i) {
                var title = $(this).text();
                if(i != 0 && i != 7){
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                }
            });
        }
    
       	table = $('#ajaxTbl').DataTable({
       	    "processing": true, 
            "serverSide": true, 
            scrollY:   '50vh',
            "fixedHeader": {
                header: true,
                footer: true
            },
            "order": [
             [ 7, "desc" ]
            ], 
            // "order": [[ 3, "desc" ]]
            "ajax": {
                "url": "<?php echo site_url('User_management/ajax_list')?>",
                "type": "POST",
                "data":{
                    user_status:status, 
                    umm_upd_info_pass_avtr: "<?php if(array_search('umm_upd_info_pass_avtr', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>",
                    umm_assgn_res: "<?php if(array_search('umm_assgn_res', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>"
                },
                 "error": function(xhr, ajaxOptions, thrownError) {
                     if (xhr.status == 403) {
                      table.settings()[0].jqXHR;
                     }
                 }
            },
           
            "language": {
                processing:
                "<img src='" +
                http_assets_path +
                "custom/img/ajax-loader.gif' width='2%'>&emsp; Please Wait... (Reload the page if doesn't work.) ",
                "infoFiltered": " "
            },
            "columnDefs": [
                {targets:1, orderable: false, visible: false },
                {targets:0, orderable: false, width: "3%", searchable: false, className: 'select-checkbox'},
                {targets:2, orderable: false, width: "3%", searchable: false},
                {targets:9, orderable: false, width: "2%", searchable: false}
            ],
            select: {
                style:    'os',
                selector: 'td:first-child'
            },
        });
        
        if(action == 'showSearch'){
            table.columns().every( function (i) {
                var that = this;
               if(i != 0 && i != 7){
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                });
               } 
            });
        }
    
        $.fn.dataTable.ext.errMode = "none";
        
        table.on('xhr', function() {
    	
    	});
        
        table.on("draw", function() {
       
        });
    
        $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	});
    	
    	$(".selectall").on("ifChanged", selectallfunc);
     
        $(<?php  if(array_search('umm_view', $db->main_sub_header['responsibility']) !== false){ ?>
            '<b>View all: </b><select id="table_status" onchange="table_status()">'+
            '<option value="active" '+(status == "active" ? "selected" : "")+'>Active Users</option>'+
            '<option value="inactive" '+(status == "inactive" ? "selected" : "")+'>Inactive Users</option>'+
            '<option value="block" '+(status == "block" ? "selected" : "")+'>Block Users</option>'+
            '<option value="deleted" '+(status == "deleted" ? "selected" : "")+'>Deleted Users</option>'+
         '</select>&emsp;'+
         <?php } ?>
         <?php  if(array_search('umm_chg_usr_sta', $db->main_sub_header['responsibility']) !== false){ ?>
         '<b>Multiple Action: </b><select id="table_action" onchange="table_action()">'+
            '<option value="" '+(make_mul_action == "" ? "selected" : "")+'>Select User Status</option>'+
            '<option value="block" '+(make_mul_action == "block" ? "selected" : "")+'>Block</option>'+
            '<option value="deleted" '+(make_mul_action == "delete" ? "selected" : "")+'>Delete</option>'+
            '<option value="active" '+(make_mul_action == "active" ? "selected" : "")+'>Make Active</option>'+
         '</select>'
         <?php } ?>
        ).insertAfter( ".dataTables_filter label" );
        
    } // end of tableajax
 
    function selectallfunc(e){
        if(e.target.checked){
            $('.selectall').iCheck('check'); 
            table.rows(  ).select();   
        }else{
            $('.selectall').iCheck('uncheck'); 
            table.rows(  ).deselect(); 
        }
    }
 
    function table_status(){
        var table_status = $('#table_status option:selected').val();
        if(table_status != undefined){
            tableAJAX(null,table_status);
        }
    }

    function table_action(){
        var table_action = $('#table_action option:selected').val();
        var title;
        switch(table_action) {
          case 'deleted':
            title = 'Delete';
            break;
          case 'block':
            title = 'Block';
            break;
          case 'active':
            title = 'Active';
            break;    
          default:
             title = 'Active';
        }
        var arrTableSelectedRowsRendered = [];
        var dataTableRows = table.rows({selected: true}).data().toArray();
        if(dataTableRows.length > 0){
            $('#myModal').modal('show');
    		$('#mod-title').html(title +" User");
    		$('#myModal .modal-dialog').removeClass('modal-lg');
    		$('#mod-content').html(
    		    '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+
    		    '<p id="tableactionForm">Are you sure to '+title+' this user/s?</p>');
    		$('#notif-mess').html(' ');
    		$('#mod-footer').html('<button id="tableactiobtnform"  class="btn btn-primary" onclick=actionMultiple("'+title+'") >Submit</button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        }else{
            $('#table_action').val('')
            $('#myModal').modal('show');
    		$('#mod-title').html("Alert Message");
    		$('#myModal .modal-dialog').removeClass('modal-lg');
    		$('#mod-content').html('<p>Select a record from the table</p>');
    		$('#mod-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        }
    }

    function actionMultiple(title){
        var arrTableSelectedRowsRendered = [];
        var dataTableRows = table.rows({selected: true}).data().toArray();
        for (var i = 0; i < dataTableRows.length; i++){
            dataTableRows[i] = dataTableRows[i].slice(1, dataTableRows[i].length);
            arrTableSelectedRowsRendered.push( dataTableRows[i].slice( 0, 1));
        }
        
        $.ajax({
    		type:'POST',
    		url:'<?php echo site_url('User_management/actionMultiple'); ?>',
    		data:{id:arrTableSelectedRowsRendered,title:title},
    		dataType:'json',
    		beforeSend:function(){
    		        $('.loadingFORM').removeClass('hidden');
                    $(".loadingFORM").fadeIn();
                    $('#tableactionForm').css('opacity','0.1');
                    $('#tableactiobtnform').attr('disabled',true);
                    $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                        'width':'190px',
                        'height':'50px',
                        'border-radius':'5px',
                        'background-color':'white',
                        'padding':'15px',
                        'color':'black',
                    }).addClass('alpha60');
    		 },
    		success:function(datadb){
    		    $('.loadingFORM').html('').addClass('hidden');
                $('#tableactionForm').css('opacity','10');
                 $('#tableactiobtnform').attr('disabled',false);
    			$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user/s has been '+title+'.</b>').css('color','green');
    			setTimeout(function(){ 
    				$('#notif-mess').html('').css('color','black');
    				$('#myModal').modal('hide'); 
    				reload_table();
    			}, 1000);
    
    		}
    	})
    }

 
    function searchDefault(){
        table.search($("#searchEvntMtr").val()).draw();
    } 
    
    function searchDefaultByEnter(){
        var keyPressed = event.keyCode || event.which;
        if(keyPressed==13){
             table.search($("#searchEvntMtr").val()).draw();
             keyPressed=null;
        }else{
            return false;
        }
    }


/* Options */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function reload_table(){
    table.ajax.reload(null,false); //reload datatable ajax 
}

function addUserProf(action){
	 if(action == 'view'){
		$('#btn-header').html(''); 
		$('#myModal').modal({backdrop: 'static', keyboard: false})
		$('#mod-title').html("Add User");
		$('#myModal .modal-dialog').addClass('modal-lg');
		$('#mod-content').html(
			 '<form id="addUserProfForm">'+
    			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
				 '<div class="col-xs-6">'+
					'<div class="form-group">'+
						  '<label class="control-label">First Name</label>'+
						  '<input type="text" name="first_name" class="form-control capitalize form-val" placeholder="Ex: John">'+
						  '<span class="help-block"></span>'+
						  '</div>'+
					'<div class="form-group">'+
						  '<label class="control-label">Last Name</label>'+
						  '<input type="text" name="last_name" class="form-control capitalize form-val" placeholder="Ex: Santos">'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						  '<label class="control-label">Middle Name</label>'+
						  '<input type="text" name="middle_name" class="form-control capitalize form-val" placeholder="Ex: Olivar">'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						'<div class="radio">'+
							'<b class="hidden-xs">Gender:</b> &nbsp;'+
							'<label style="padding-left:0" > Male &nbsp;'+
							  '<input type="radio" class="iCheck" name="gender" value="male" checked>'+
							'</label>'+
							'<label  style="padding-left:15px"> Female &nbsp;'+
							  '<input type="radio" class="iCheck" name="gender" value="female">'+
							'</label >'+
						 '</div>'+
					'</div>'+
				 '</div>'+
				 
				'<div class="col-xs-6">'+
					'<div class="form-group">'+
						  '<label>Username</label>'+
						  '<input type="text" name="username" class="form-control form-val" placeholder="john.santos" >'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						  '<label>Email Address</label>'+
						  '<input type="text" name="email" class="form-control form-val" placeholder="john.santos@gmail.com" >'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						  '<label>Mobile No</label>'+
						  '<input type="text" class="form-control" name="mobile" placeholder="+(63)912-3456-789" data-mask="phone" data-bind="textInput:phone">'+
					'</div>'+
					'<div class="form-group input-group input-group-md col-md-12 col-xs-12">'+
					  '<label>Password</label>'+
						'<div class="form-group input-group input-group-md col-md-12 col-xs-12" style="padding-bottom:0;margin-bottom:0;">'+
							'<input type="password" name="password" placeholder="Password" class="form-control showpassword">'+	
							'<span class="input-group-btn">'+
							 '<button type="button" class="btn btn-default btn-flat showpasswordbtn"><i class="fa fa-eye" aria-hidden="true"></i></button>'+
							'</span>'+
						'</div>'+
						'<span class="help-block helpforpw" style="padding-bottom:0;margin-bottom:0;"></span>'+
					'</div>'+
					'<div class="form-group" style="margin-top:-15px">'+
						  '<input type="password" name="passconf" placeholder="Confirm Password" class="form-control showpassword2">'+	
						  '<span class="help-block"></span>'+
					'</div>'+
					'<input type="checkbox"  id="showpasswordcheckbox" class="hidden" />'+
					'<div class="form-group">'+
						  '<label>User Type</label>'+
						  '<select class="form-control" name="user_type">'+
							'<option value="1">Administration</option>'+
							'<option value="2">Processor</option>'+
							'<option value="3">Normal User</option>'+
						  '</select>'+
					 '</div>'+
				 '</div>'+
			 '</form>'+
			 '<div style="clear:both"><div>');
		$('#notif-mess').html(' ');
		$('#mod-footer').html('<button id="addUserSubmitform"  class="btn btn-primary" onclick=addUserProf("save") >Submit</button>'+
		'<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>');
		formPlugins();
	 }else if(action == "save"){
		var form = $('#addUserProfForm').serialize();
		$.ajax({
			 type:'POST',
			 url:'<?php echo site_url('User_management/addUserProf'); ?>',
			 data:form,
			 success:function(datadb){
				var data = JSON.parse(datadb); 
				if(data.status){
					$('.form-group').removeClass('has-error'); // clear error class
					$('.help-block').empty(); // clear error string
					$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> A new user has been added.</b>').css('color','green');
					setTimeout(function(){ 
						$('#notif-mess').html('').css('color','black');
						$('#myModal').modal('hide'); 
						reload_table();
					}, 3000);
				}else{
					for (var i = 0; i < data.inputerror.length; i++){
						if(data.inputerror[i] != "password"){
						$('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
						$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
						}else{
							$('.helpforpw').parent().addClass('has-error');
							$('.helpforpw').text(data.error_string[i]);
						}
					}
				}
			 }
		})
	 }
 }

function editUserProf(actions,id){
    if(actions == "view"){
       $('#myModal').modal({backdrop: 'static', keyboard: false})
		$('#myModal .modal-dialog').addClass('modal-lg');
		$('#mod-title').html("Edit User Profile");
		$('#mod-content').html(
		    '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+
			 '<form id="editUserProfForm" class="editUserProfForm">'+
			    '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
				'<input type="hidden" value="'+id+'" name="edit_id">'+
				 '<div class="col-xs-6">'+
					'<div class="form-group">'+
						  '<label class="control-label">First Name</label>'+
						  '<input type="text" name="first_name" class="form-control capitalize form-val" placeholder="Ex: John">'+
						  '<span class="help-block"></span>'+
						  '</div>'+
					'<div class="form-group">'+
						  '<label class="control-label">Last Name</label>'+
						  '<input type="text" name="last_name" class="form-control capitalize form-val" placeholder="Ex: Santos">'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						  '<label class="control-label">Middle Name</label>'+
						  '<input type="text" name="middle_name" class="form-control capitalize form-val" placeholder="Ex: Olivar">'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						'<div class="radio">'+
							'<b class="hidden-xs">Gender:</b>  &emsp;&nbsp;'+
							'<label style="padding-left:0" > Male &emsp;'+
							  '<input type="radio" class="iCheck" name="gender" value="male" checked>'+
							'</label>'+
							'<label  style="padding-left:15px"> Female &emsp;'+
							  '<input type="radio" class="iCheck" name="gender" value="female">'+
							'</label >'+
						 '</div>'+
					'</div>'+
				 '</div>'+
				 
				'<div class="col-xs-6">'+
					'<div class="form-group">'+
						  '<label>Username</label>'+
						  '<input type="text" name="username" class="form-control form-val" placeholder="john.santos" >'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						  '<label>Email Address</label>'+
						  '<input type="text" name="email" class="form-control form-val" placeholder="john.santos@gmail.com" >'+
						  '<span class="help-block"></span>'+
					'</div>'+
					'<div class="form-group">'+
						  '<label>Mobile No</label>'+
						  '<input type="text" class="form-control" name="mobile" placeholder="+(63)912-3456-789" data-mask="phone" data-bind="textInput:phone">'+
					'</div>'+
					'<div class="form-group">'+
						  '<label>User Type</label>'+
						  '<select class="form-control" name="user_type">'+
							'<option value="1">Administration</option>'+
							'<option value="2">Processor</option>'+
							'<option value="3">Normal User</option>'+
						  '</select>'+
					 '</div>'+
					 '<div class="form-group">'+
						  '<label>User Status</label>'+
						  '<select class="form-control" name="user_status">'+
							'<option value="active">Active</option>'+
							'<option value="inactive">Inactive</option>'+
							'<option value="block">Blocked</option>'+
							'<option value="deleted">Delete</option>'+
						  '</select>'+
					 '</div>'+
				 '</div>'+
			 '</form>'+
			 '<div style="clear:both"><div>');
		$('#notif-mess').html(' ');
		$('#mod-footer').html('<button id="editUserProfSubmitform"  class="btn btn-primary" onclick=editUserProf("save") >Submit</button>'+
		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
		formPlugins();
		getInfoProfile(id);
    }else if(actions == "save"){
        var form = $('#editUserProfForm').serialize();
		$.ajax({
			 type:'POST',
			 url:'<?php echo site_url('User_management/editUserProf'); ?>',
			 data:form,
			 success:function(datadb){
				var data = JSON.parse(datadb); 
				if(data.status){
					$('.form-group').removeClass('has-error'); // clear error class
					$('.help-block').empty(); // clear error string
					$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user has been updated.</b>').css('color','green');
					setTimeout(function(){ 
						$('#notif-mess').html('').css('color','black');
						$('#myModal').modal('hide'); 
						reload_table();
					}, 1000);
				}else{
					for (var i = 0; i < data.inputerror.length; i++){
						if(data.inputerror[i] != "password"){
						$('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
						$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
						}else{
							$('.helpforpw').parent().addClass('has-error');
							$('.helpforpw').text(data.error_string[i]);
						}
					}
				}
			 }
		})
    }
}

function changeUserPwd(action,id){
	  if(action == 'view'){
		$('#btn-header').html('');
		$('#myModal').modal({backdrop: 'static', keyboard: false})
		$('#myModal .modal-dialog').removeClass('modal-lg');
		$('#mod-title').html("Reset Password");
		$('#mod-content').html(
			 '<form id="changeUserPwdForm" >'+
			    '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
				'<input type="hidden" value="'+id+'" name="edit_id">'+
				 '<div class="form-group input-group input-group-md col-md-12 col-xs-12">'+
					  '<label>Reset Password:</label>'+
						'<div class="form-group input-group input-group-md col-md-12 col-xs-12" style="padding-bottom:0;margin-bottom:0;">'+
							'<input type="password" name="password" placeholder="Password" class="form-control showpassword">'+	
							'<span class="input-group-btn">'+
							 '<button type="button" class="btn btn-default btn-flat showpasswordbtn"><i class="fa fa-eye" aria-hidden="true"></i></button>'+
							'</span>'+
						'</div>'+
						'<span class="help-block helpforpw" style="padding-bottom:0;margin-bottom:0;"></span>'+
					'</div>'+
					'<div class="form-group" style="margin-top:-15px">'+
						  '<input type="password" name="passconf" placeholder="Confirm Password" class="form-control showpassword2">'+	
						  '<span class="help-block"></span>'+
					'</div>'+
					'<input type="checkbox"  id="showpasswordcheckbox" class="hidden" />'+
					
				 '</div>'+
			 '</form>'+
			 '<div style="clear:both"><div>');
		$('#notif-mess').html(' ');
		$('#mod-footer').html('<button id="editUserPWSubmitform"  class="btn btn-primary" onclick=changeUserPwd("save") >Submit</button>'+
		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
		$("input[type='password']").val('');
		formPlugins();
	 }else if(action == 'save'){
		 var form = $('#changeUserPwdForm').serialize();
		$.ajax({
			 type:'POST',
			 url:'<?php echo site_url('User_management/changeUserPwd'); ?>',
			 data:form,
			 success:function(datadb){
				var data = JSON.parse(datadb); 
				if(data.status){
					$('.form-group').removeClass('has-error'); // clear error class
					$('.help-block').empty(); // clear error string
					$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user\'s password has been reset.</b>').css('color','green');
					setTimeout(function(){ 
						$('#notif-mess').html('').css('color','black');
						$('#myModal').modal('hide'); 
						reload_table();
					}, 1000);
				}else{
					for (var i = 0; i < data.inputerror.length; i++){
						if(data.inputerror[i] != "password"){
						$('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
						$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
						}else{
							$('.helpforpw').parent().addClass('has-error');
							$('.helpforpw').text(data.error_string[i]);
						}
					}
				}
			 }
		})
	 }
 }
 
 function uploadUserPhoto(action,id,uplpath,subaction=null){
	if(action == 'view'){
	   	$('#mod-content').empty();
		$('#myModal .modal-dialog').removeClass('modal-lg');
		$('#mod-title').html("Upload User Profile Photo");
		$('#btn-header').html('<a class="btn btn-xs btn-default pull-right" style="margin-right:10px;color:black;cursor: pointer;" onclick=choosePhoto()><i class="fa fa-upload" aria-hidden="true"></i>&nbsp; Choose Photo</a>');
		$('#mod-content').html(
		'<div id="loadingUpload" class="hidden" style="text-align:center"></div>'+
		'<div id="myModalProfImageBody"></div>');
		$('#notif-mess').html(' ');
		$('#mod-footer').html('<div id="replaceUploadPhotoDiv" style="float:left"></div>'+
			'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
		
		displayFormUploadPhoto(id,uplpath);
		if(uplpath == ''){
			$('.cropit-image-input').trigger('click');
			var imgprev = "";
			$('#existProfImg').val('');
			
		}else{			
			var imgprev =  http_assets_path + "custom/img/user_prof/"+uplpath;
			$('#existProfImg').val(uplpath);
		}
		
		$('.image-editor').cropit({
			imageState: {
				src: imgprev,
			},
			imageBackground: true,
			imageBackgroundBorderWidth: 25,
			type: 'image/jpeg',
			quality: .9,
			allowDragNDrop: false,
			originalSize: true,
			onFileChange:function(){		
				$('#loadingUpload').removeClass('hidden');
				$('#formAreaUploadPhoto').addClass('hidden');
				$('#loadingUpload').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'> Uploading...");
				$('#replaceUploadPhotoDiv').html('');
			
			},
			onFileReaderError:function(){
			    $('#loadingUpload').html('<i class="fa fa-picture-o" aria-hidden="true"></i> Select a photo');
				$('#loadingUpload').addClass('hidden');
				$('#formAreaUploadPhoto').removeClass('hidden');
				$('#replaceUploadPhotoDiv').html('');
			    $('#notif-mess').html('<b>Image cannot load, the image valid format are:<br> format: png, jpg | Minimum width and height is 250px by 250px </b>').css({color:'red','text-align':'left'});  
			},
			onImageError: function(){
			    $('#loadingUpload').html('<i class="fa fa-picture-o" aria-hidden="true"></i> Select a photo');
				$('#loadingUpload').addClass('hidden');
				$('#formAreaUploadPhoto').removeClass('hidden');
				$('#replaceUploadPhotoDiv').html('');
			    $('#notif-mess').html('<b>Image cannot load, the image valid format are:<br> format: png, jpg | Minimum width and height is 250px by 250px </b>').css({color:'red','text-align':'left'});  
			},
			onImageLoaded: function(cropitObject) {
				$('#loadingUpload').html('<i class="fa fa-picture-o" aria-hidden="true"></i> Select a photo');
				$('#loadingUpload').addClass('hidden');
				$('#formAreaUploadPhoto').removeClass('hidden');
				$('#replaceUploadPhotoDiv').html('<button type="button" class="btn btn-info hidden" id="replaceUploadPhoto" onclick="replaceUploadPhoto()">Upload</button>');
				//$('#replaceUploadPhoto').removeClass('hidden');
				$('#notif-mess').html('');
				if($('.cropit-image-input').val() != ''){
				    $('#replaceUploadPhoto').removeClass('hidden');  
				}else{
				   $('#replaceUploadPhotoDiv').html('');
				}
			}	 
		 
		});
		
		 if(subaction == null){ $('#myModal').modal({backdrop: 'static', keyboard: false}) }
	}else{
		
	}
 }
 
 function choosePhoto(){
	$('.cropit-image-input').trigger('click');
 }
 // if upload button is click, submit button in cropit will be triggered
 function replaceUploadPhoto(){
	$('#submitUploadPhoto').trigger('click');
 }
 
 function cropitImageInput(){
	$('#logoUpload').addClass('hidden');
	$('.cropit-image-zoom-input').removeClass('hidden');
	$('.image-size-label').trigger('change');
	$('.image-size-label').removeClass('hidden');
	$('#replaceUploadPhoto').removeClass('hidden');
	$('.cropit-preview-image-container').css('cursor','move');
	
 }
 
 
 function displayFormUploadPhoto(u_id,uplpath){ 
 
	 if(uplpath == ""){
		var uplpathdiv = '<div id="logoUpload" style="font-size:30px;color:#d0d0d0;text-align:center"><i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; Upload a Photo</div>';
	 }else{
		 var uplpathdiv = '';
	 }
		 $('#myModalProfImageBody').html(
		 uplpathdiv+
			'<input type="hidden" id="u_id" value="'+u_id+'">'+
			'<form action="#" id="formAreaUploadPhoto" class="hidden">'+
			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
			 '<div class="image-editor" style="margin:30px">'+
				'<input type="file" onchange="cropitImageInput()" class="cropit-image-input hidden">'+
				'<input type="hidden" id="existProfImg">'+
				
				'<div class="cropit-preview" style="margin:0 auto"></div><br><br>'+
				'<center><div class="image-size-label hidden">'+
				  'Resize image'+
				'</div>'+
				'<input type="range" class="cropit-image-zoom-input hidden" ></center>'+
				'<input type="hidden" name="image-data" class="hidden-image-data" />'+
				'<button type="submit" class="hidden" id="submitUploadPhoto">Submit</button>'+
			 ' </div>'+
			'</form>'
		 );
 }
 
  function displayFormUploadPhotoSubmit(u_id,obj){ 
  var imgprev = http_assets_path+"custom/img/user_prof/"+obj;
  $('.image-editor').cropit({
		imageState: {
			src: imgprev,
		},
		allowDragNDrop: false,
		imageBackground: true,
		imageBackgroundBorderWidth: 25,
		type: 'image/jpeg',
		quality: .9,
		originalSize: true,
		onFileChange:function(){		
			$('#loadingUpload').removeClass('hidden');
			$('#formAreaUploadPhoto').addClass('hidden');
			$('#loadingUpload').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'> Uploading...");
		},
		onImageLoaded: function(cropitObject) {
			$('#loadingUpload').html('<i class="fa fa-picture-o" aria-hidden="true"></i> Select a photo');
			$('#loadingUpload').addClass('hidden');
			$('#formAreaUploadPhoto').removeClass('hidden');
		}	 
	 
	});
  }
 
  $(document).on('submit','#formAreaUploadPhoto',function(e) {
	  e.preventDefault();
	  var u_id = $('#u_id').val();
	  var imageData = $('.image-editor').cropit('export');
	  $('.hidden-image-data').val(imageData);
	  var formValue = $(this).serialize();
	  var rformValue = formValue + '&id=' + encodeURIComponent(u_id) + '&exstImg='+$('#existProfImg').val();
	  
	   $.ajax({
		type:'POST',
		url:'<?php echo site_url('User_management/uploadUserPhoto'); ?>', 
		data:rformValue,
		beforeSend:function(){
			$('#btnSaveProImg').text('Saving...'); //change button text
			$('#btnSaveProImg').attr('disabled',true); //set button disable 
			$('[name="profImg"]').attr('disabled',true); //set button disable 
		},
		success:function(data){
			var obj = JSON.parse(data);
			$('#btnSaveProImg').text('Submit'); //change button text
			$('#btnSaveProImg').attr('disabled',false); //set button disable 
			$('[name="profImg"]').attr('disabled',false); //set button disable 
			reload_table();
			displayFormUploadPhoto(u_id);
			displayFormUploadPhotoSubmit(u_id,obj);
		}
	  })
  });
  
  function deleteUserProf(action,id){
	  if(action == 'view'){
		$('#btn-header').html(''); 
		$('#myModal').modal('show');
		$('#mod-title').html("Delete User");
		$('#myModal .modal-dialog').removeClass('modal-lg');
		$('#mod-content').html('<p>Are you sure to delete this request?</p>'+
		'<input type="hidden" value="'+id+'" id="delete_id">');
		$('#notif-mess').html(' ');
		$('#mod-footer').html('<button id="deleteUserSubmitform"  class="btn btn-primary" onclick=deleteUserProf("save") >Submit</button>'+
		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
	   }else if(action == 'save'){
		   var delete_id = $('#delete_id').val();
			$.ajax({
				type:'POST',
				url:'<?php echo site_url('User_management/deleteUserProf'); ?>',
				data:{id:delete_id},
				success:function(datadb){
					$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user has been deleted.</b>').css('color','green');
					setTimeout(function(){ 
						$('#notif-mess').html('').css('color','black');
						$('#myModal').modal('hide'); 
						reload_table();
					}, 1000);

				}
			})
	   }
 }
 
 function responUserProf(action,id,responsibility){
    switch(responsibility) {
      case 'user_management':
            if(action == 'view'){
        		$('#btn-header').html(''); 
        		$('#myModal').modal('show');
        		$('#mod-title').html("User Responsibility : User Management");
        		$('#myModal .modal-dialog').removeClass('modal-lg');
        		$('#mod-content').html( '<form id="addResposibilityForm">'+
            			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
            	         '<input type="hidden" value="'+id+'" id="id" name="id">'+
        				 '<div class="col-xs-7">'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm" name="umm" class="iCheck parentIcheck">&nbsp; User Management Module</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck" style="margin-left:30px">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_view" name="umm_view" class="iCheck childIcheck">&nbsp; View User List</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck" style="margin-left:30px">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_add_user" name="umm_add_user" class="iCheck childIcheck">&nbsp; Add User</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck" style="margin-left:30px">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_upd_info_pass_avtr" name="umm_upd_info_pass_avtr" class="iCheck childIcheck">&nbsp; Update user info, password, avatar</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        				'</div>'+
        				 
        				'<div class="col-xs-5">'+
        			    	'<div class="form-group">'+
        						 '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<br>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						 '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_chg_usr_sta" name="umm_chg_usr_sta" class="iCheck childIcheck">&nbsp; Change User Status</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_assgn_res" name="umm_assgn_res" class="iCheck childIcheck">&nbsp; Assign Responsibility</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					
        				 '</div>'+
        			 '</form>'+
        			 '<div style="clear:both"><div>');
        		$('#notif-mess').html(' ');
        		$('#mod-footer').html('<button id="deleteUserSubmitform"  class="btn btn-primary" onclick=responUserProf("save","'+id+'","'+responsibility+'") >Submit</button>'+
        		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        		
        		$('.iCheck').iCheck({
        		  checkboxClass: 'icheckbox_square-blue',
        		  radioClass: 'iradio_square-blue',
        		  increaseArea: '10%' /* optional */
        		});
        		
        		$(".parentIcheck").on("ifChanged", responsibilityParentfunc);
        		$(".childIcheck").attr('disabled',true);
        		getResponsibilityAjax(id);
    		
    	   }else if(action == 'save'){
        		var form = $('#addResposibilityForm').serialize();
        		$.ajax({
        			 type:'POST',
        			 url:'<?php echo site_url('User_management/responUserProf'); ?>',
        			 data:form,
        			 success:function(datadb){
        				var data = JSON.parse(datadb); 
        			    	$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user responsibility has been updated.</b>').css('color','green');
            				setTimeout(function(){ 
            					$('#notif-mess').html('').css('color','black');
            					$('#myModal').modal('hide'); 
            					reload_table();
            				}, 1000);
        			 }
        		})
    	   }
        
        break;
      case 'accounts':
           if(action == 'view'){
        		$('#btn-header').html(''); 
        		$('#myModal').modal('show');
        		$('#mod-title').html("User Responsibility : Accounts");
        		$('#myModal .modal-dialog').removeClass('modal-lg');
        		$('#mod-content').html( '<form id="addAccountsForm">'+
            			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
            	         '<input type="hidden" value="'+id+'" id="id" name="id">'+
        				 '<div class="col-xs-7">'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="accts_mm" name="accts_mm" class="iCheck parentIcheck">&nbsp; Accounts Module</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck" style="margin-left:30px">'+
                                    '<label>'+
                                      '<input type="checkbox" id="accts_crt" name="accts_crt" class="iCheck childIcheck">&nbsp; Account Creation</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck" style="margin-left:30px">'+
                                    '<label>'+
                                      '<input type="checkbox" id="accts_all_sb" name="accts_all_sb" class="iCheck childIcheck">&nbsp; All Sub</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck" style="margin-left:30px">'+
                                    '<label>'+
                                      '<input type="checkbox" id="accts_mmbr_sb" name="accts_mmbr_sb" class="iCheck childIcheck">&nbsp; Membership Sub</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        				'</div>'+
        				 
        				'<div class="col-xs-5">'+
        			    	'<div class="form-group">'+
        						 '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<br>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group hidden">'+
        						 '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_chg_usr_sta" name="umm_chg_usr_sta" disabled="disabled" class="iCheck childIcheck">&nbsp; Change User Status</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					'<div class="form-group hidden">'+
        						  '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="umm_assgn_res" name="umm_assgn_res" disabled="disabled" class="iCheck childIcheck">&nbsp; Assign Responsibility</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        					
        				 '</div>'+
        			 '</form>'+
        			 '<div style="clear:both"><div>');
        		$('#notif-mess').html(' ');
        		$('#mod-footer').html('<button id="deleteUserSubmitform"  class="btn btn-primary" onclick=responUserProf("save","'+id+'","'+responsibility+'") >Submit</button>'+
        		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        		
        		$('.iCheck').iCheck({
        		  checkboxClass: 'icheckbox_square-blue',
        		  radioClass: 'iradio_square-blue',
        		  increaseArea: '10%' /* optional */
        		});
        		
        		$(".parentIcheck").on("ifChanged", responsibilityParentfunc);
        		$(".childIcheck").attr('disabled',true);
        		getResponsibilityAjax(id);
    		
    	   }else if(action == 'save'){
        		var form = $('#addAccountsForm').serialize();
        		$.ajax({
        			 type:'POST',
        			 url:'<?php echo site_url('User_management/responAccountsMod'); ?>',
        			 data:form,
        			 success:function(datadb){
        				var data = JSON.parse(datadb); 
        			    	$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user responsibility has been updated.</b>').css('color','green');
            				setTimeout(function(){ 
            					$('#notif-mess').html('').css('color','black');
            					$('#myModal').modal('hide'); 
            					reload_table();
            				}, 1000);
        			 }
        		})
    	   }
          
        break;  
        
      case 'health_claims':
            if(action == 'view'){
        		$('#btn-header').html(''); 
        		$('#myModal').modal('show');
        		$('#mod-title').html("User Responsibility : Health Claims");
        		$('#myModal .modal-dialog').removeClass('modal-lg');
        		$('#mod-content').html( '<form id="addResposibilityForm">'+
            			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
            	         '<input type="hidden" value="'+id+'" id="id" name="id">'+
        				 '<div class="col-xs-7">'+
        					'<div class="form-group">'+
        						  '<div class="checkbox icheck ">'+
                                    '<label>'+
                                      '<input type="checkbox" id="hlth_clms_mm" name="hlth_clms_mm" class="iCheck parentIcheck">&nbsp; Health Claims</a>'+
                                    '</label>'+
                                  '</div>'+
        					'</div>'+
        				
        				'</div>'+
        				 
        				'<div class="col-xs-5">'+
        			    '</div>'+
        			 '</form>'+
        			 '<div style="clear:both"><div>');
        		$('#notif-mess').html(' ');
        		$('#mod-footer').html('<button id="deleteUserSubmitform"  class="btn btn-primary" onclick=responUserProf("save","'+id+'","'+responsibility+'") >Submit</button>'+
        		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        		
        		$('.iCheck').iCheck({
        		  checkboxClass: 'icheckbox_square-blue',
        		  radioClass: 'iradio_square-blue',
        		  increaseArea: '10%' /* optional */
        		});
        		
        		$(".parentIcheck").on("ifChanged", responsibilityParentfunc);
        		$(".childIcheck").attr('disabled',true);
        		getResponsibilityAjax(id);
    		
    	   }else if(action == 'save'){
        		var form = $('#addResposibilityForm').serialize();
        		$.ajax({
        			 type:'POST',
        			 url:'<?php echo site_url('User_management/responHlthClms'); ?>',
        			 data:form,
        			 success:function(datadb){
        				var data = JSON.parse(datadb); 
        			    	$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The user responsibility has been updated.</b>').css('color','green');
            				setTimeout(function(){ 
            					$('#notif-mess').html('').css('color','black');
            					$('#myModal').modal('hide'); 
            					reload_table();
            				}, 1000);
        			 }
        		})
    	   }
         break;  
      default:
      
    }
 }
 
 function responsibilityParentfunc(e){
      if(e.target.checked){
        $('.childIcheck').iCheck('check');  
        $(".childIcheck").attr('disabled',false);
     }else{
        $('.childIcheck').iCheck('uncheck'); 
        $(".childIcheck").attr('disabled',true);
     }
 }
 
 function getResponsibilityAjax(id){
     
     $.ajax({
		 type:'POST',
		 url:'<?php echo site_url('User_management/getResponsibilityAjax'); ?>',
		 data:{id:id},
		 dataType:'json',
		 success:function(datadb){
		     if(jQuery.inArray("umm", datadb) !== -1){  $('#umm').iCheck('check'); }else{ $('#umm').iCheck('uncheck'); }
		     if(jQuery.inArray("umm_view", datadb) !== -1){  $('#umm_view').iCheck('check');  }else{ $('#umm_view').iCheck('uncheck'); }
		     if(jQuery.inArray("umm_add_user", datadb) !== -1){  $('#umm_add_user').iCheck('check');  }else{ $('#umm_add_user').iCheck('uncheck'); }
		     if(jQuery.inArray("umm_upd_info_pass_avtr", datadb) !== -1){  $('#umm_upd_info_pass_avtr').iCheck('check'); }else{ $('#umm_upd_info_pass_avtr').iCheck('uncheck'); }
		     if(jQuery.inArray("umm_chg_usr_sta", datadb) !== -1){  $('#umm_chg_usr_sta').iCheck('check'); }else{ $('#umm_chg_usr_sta').iCheck('uncheck'); }
		     if(jQuery.inArray("umm_assgn_res", datadb) !== -1){  $('#umm_assgn_res').iCheck('check'); }else{ $('#umm_assgn_res').iCheck('uncheck'); }
		     
		 }
	})
 }


function formPlugins(){
		$('[data-mask="phone"]').inputmask({
			mask: '+(63)999-9999-999'
		});
	
		$('.showpasswordbtn').click(function(e) {	
			e.preventDefault();
			if($(this).find('i').hasClass('fa-eye')){
				$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
				$('#showpasswordcheckbox').prop('checked',true).change();
			}else{
				$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
				$('#showpasswordcheckbox').prop('checked',false).change();
			}
		});
	
		$('#showpasswordcheckbox').change(function(){
			var $input = $('.showpassword'); 	var $input2 = $('.showpassword2');
			  var change = $(this).is(":checked") ? "text" : "password";
					var rep = $(".showpassword")
						.attr("type", change)
						.attr("id", $input.attr("id"))
						.attr("name", $input.attr("name"))
						.attr('class', $input.attr('class'))
						.val($input.val())
						.insertBefore($input);
					$input = rep;
					var rep2 = $(".showpassword2")
						.attr("type", change)
						.attr("id", $input2.attr("id"))
						.attr("name", $input2.attr("name"))
						.attr('class', $input2.attr('class'))
						.val($input2.val())
						.insertBefore($input2);
					$input2 = rep2;
		})
	 
		$('.iCheck').iCheck({
		  checkboxClass: 'icheckbox_square-blue',
		  radioClass: 'iradio_square-blue',
		  increaseArea: '10%' /* optional */
		});

		$('.capitalize').on('keydown', function(event) {
			if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
			   var $t = $(this);
			   event.preventDefault();
			   var char = String.fromCharCode(event.keyCode);
			   $t.val(char + $t.val().slice(this.selectionEnd));
			   this.setSelectionRange(1,1);
			}
		});
 }
 
 
 function getInfoProfile(id){
	 $.ajax({
		 type:'POST',
		 url:'<?php echo site_url('User_management/getInfoProfile'); ?>',
		 data:{id:id},
		 dataType:'JSON',
		 beforeSend:function(){
		        $('.loadingFORM').removeClass('hidden');
                $(".loadingFORM").fadeIn();
                $('#editUserProfForm').css('opacity','0.1');
                $('#editUserProfForm :input').attr('disabled', true);
                $('#editUserProfSubmitform').attr('disabled',true);
                $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                    'width':'190px',
                    'height':'50px',
                    'border-radius':'5px',
                    'background-color':'white',
                    'padding':'15px',
                    'color':'black',
                }).addClass('alpha60');
		 },
		 success:function(data){
            $('.loadingFORM').html('').addClass('hidden');
            $('#editUserProfForm').css('opacity','10');
            $('#editUserProfForm :input').attr('disabled', false);
            $('#editUserProfForm select').attr('disabled', false);
            $('#editUserProfForm textarea').attr('disabled', false);
            $('#editUserProfSubmitform').attr('disabled',false);
		     
			 $('[name="first_name"]').val(data.first_name);
			 $('[name="last_name"]').val(data.last_name);
			 $('[name="middle_name"]').val(data.middle_name);
			 $('[name="gender"]').each(function(){
			  if($(this).val() == data.gender){
				  $(this).iCheck('check');
			  }
			})
			 $('[name="username"]').val(data.username);
			 $('[name="email"]').val(data.email);
			 $('[name="mobile"]').val(data.mobile_number);
			 
			 $('[name="user_type"] option').each(function(){
				  if($(this).val() == data.user_type){
					  $(this).prop('selected',true);
				  }
			 });
			 $('[name="user_status"] option').each(function(){
				  if($(this).val() == data.user_status){
					  $(this).prop('selected',true);
				  }
			 })
			
		 }
		 
	 })
 }
 


</script>

</body>
</html>
