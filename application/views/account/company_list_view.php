<!DOCTYPE html>
<html>
    
<?php 
$arr['db'] = isset($db->page_header) ? $db->page_header : array();
$this->load->view('include/header',$arr); 
?>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <?php 
  $arr['db'] = isset($db->main_header) ? $db->main_header : array();
  $this->load->view('include/main_header',$arr); 
  ?> 


  <!-- Full Width Column -->
  <div class="content-wrapper">
  <?php 
  $arr['db'] = isset($db->main_sub_header) ? $db->main_sub_header : array();
  $this->load->view('include/main_sub_header',$arr); 
  ?>
 <?php 
      $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
      );
  ?>
  <?php 
    $u_id_enc = $db->main_header['user_info']->id;
    $gen_code = $db->page_body['generate_code'];
  ?>
  

    <div id="mod_content_div"></div>
    
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="">
      <div class="pull-right hidden-xs">
        <b>Beta Version:</b> 1.0.0 | 12.17.2018
      </div>
      <strong>Omniquest.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>&nbsp;
			<div class="pull-right" id="btn-header"></div>&nbsp; 
			
			<h4 class="modal-title pull-left" id='mod-title'></h4>
		  </div>
		  <div class="modal-body">
			<div id='mod-content'>
			</div>
		  </div>
		  <div class="modal-footer">
			<span class="pull-left" id="notif-mess"></span>
			<div id="mod-footer"></div>
		  </div>
		</div>
	</div>
</div>
<?php $this->load->view('include/modals'); ?>
<!-- page script -->
<script>
 var save_method, table_company, table_member, company_name; pass_method = null;
 var cct = '<?php echo $this->security->get_csrf_hash() ?>';
 var get_csrf_token_name = '<?php echo $this->security->get_csrf_token_name() ?>';
 var http_assets_path = '<?php echo HTTP_ASSETS_PATH; ?>';

    $.ajaxSetup({
        type:'post',
        data:{ <?php echo $this->security->get_csrf_token_name() ?> :cct}
    });
    
    $(function () {
        contentMod('company_list');
     });

     function contentMod(mod,id){
         var responsibility = '<?php echo (array_search("accts_crt", $db->main_sub_header["responsibility"] ) !== false); ?>';
        
        if(mod == "company_list"){
            $('#mod_content_div').html(
                '<section class="content-header">'+
                    '<h3 style="margin:0;padding:0;font-size:20px;font-weight:600;margin-left:-25px" class="pull-left">'+
                        '<ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0">'+
                            '<li><a href="#">&emsp; Home</a></li>'+
                        '</ol>'+
                    '</h3>'+
                    '<div class="pull-right" style="margin-top:-5px;margin-bottom:10px">'+
                    '<a class="btn btn-primary btn-md" >Provider Setup</a>&nbsp;'+
                    (responsibility == 1 ? 
                        '<button class="btn btn-primary btn-md" onClick=comp_setup("view","add")>Company Setup</button>&nbsp;' : '')+
                        '<a class="btn btn-primary btn-md" onClick=benefit_setup("view","add")>Benefit Setup</a>'+
                        '<div class="pull-left hidden" style="margin-top:-2px"><b>Create by bulk (xlsx, xls)</b> &emsp;<br> Download Template here:</b></div>'+
                        '<a class="btn btn-default btn-md hidden" href="<?php echo base_url("accounts/benefit"); ?>">Benefit Setup Page</a>'+
                        '<a class="btn btn-default btn-md hidden" href="javascript:void(0)" onclick="getCompanyAPI()">Get Info API</a>'+
                    '</div>'+
                '</section>'+
                '<section class="content" >'+
                    '<div class="row">'+
                        '<div class="col-xs-12">'+
                            '<div class="box">'+
                        (responsibility == 1 ? 
                                '<div class="box-header">'+
                                    '<div class="col-xs-6" style="margin:0;padding:0">'+
                                    '<h3 class="box-title" style="margin-top:4px;margin-left:4px;margin-bottom:2px">Company List</h3>'+
                                '</div>'+
                                '<div class="col-xs-5 pull-right" style="margin:0;padding:0">'+
                                    '<div class="input-group" >'+
                                    '<input type="text" id="searchEvntMtr" class="form-control" placeholder="Enter any keyword to search." onkeyup="searchDefaultByEnter()">'+
                                    '<span class="input-group-btn">'+
                                        '<button type="submit" id="searchEvntMtrBtn" onclick="searchDefault()" class="btn btn-flat" ><i class="fa fa-search"></i></button>'+
                                    '</span>'+
                                '</div>'+
                                '<div class="pull-right" id="searchDiv"><a href="javascript:void(0)" >Show Advance Search</a></div>'+
                            '</div>'+ // box
                        '</div>'+ // col-xs-12
                        '<div id="tablediv" class="box-body table-responsive" style="margin-top:0;padding-top:0"></div>'+
                        '<br><br><br>'
                        :
                        '<div style="paddding:10px;margin:20px">No don\'t have an access to this module, kindly contact the system admin. Thank you.<br><br><br><br></div>'
                        ) +
                    '</div>'+ // row
                 '</section>'
            ); // $('#company_list_content_div')
            tableAJAX(null,'active','',mod);
        }else if(mod == "member_list"){ 
            $('#mod_content_div').html(
                '<section class="content-header">'+
                    '<h3 style="margin:0;padding:0;font-size:20px;font-weight:600;margin-left:-25px" class="pull-left" id="breadcrumb_member">'+
                        '<ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0">'+
                            '<li><a href="javascript:void(0)" style="margin-left:5px;font-size:16px;opacity:0.6">&emsp; Home</a></li>'+
                            '<li><a href="javascript:void(0)" style="margin-left:-5px;font-size:16px;opacity:0.6">Member</a></li>'+
                            '<li><a href="javascript:void(0)" style="margin-left:-5px;font-size:16px">View Member List</a></li>'+
                        '</ol>'+
                    '</h3>'+
                    '<div class="pull-right" style="margin-top:-5px;margin-bottom:10px">'+
                    (responsibility == 1 ? 
                        '<button class="btn btn-primary btn-md" onClick=comp_setup("view","add")>Company Setup</button>&nbsp;' : '')+
                        '<a class="btn btn-primary btn-md" onClick=benefit_setup("view","add")>Benefit Setup</a>'+
                        '<div class="pull-left hidden" style="margin-top:-2px"><b>Create by bulk (xlsx, xls)</b> &emsp;<br> Download Template here:</b></div>'+
                        '<a class="btn btn-default btn-md hidden" href="<?php echo base_url("accounts/benefit"); ?>">Benefit Setup Page</a>'+
                        '<a class="btn btn-default btn-md hidden" href="javascript:void(0)" onclick="getCompanyAPI()">Get Info API</a>'+
                    '</div>'+
                '</section>'+
                '<section class="content" >'+
                    '<div class="row">'+
                        '<div class="col-xs-12">'+
                            '<div class="box">'+
                        (responsibility == 1 ? 
                                '<div class="box-header">'+
                                    '<div class="col-xs-6" style="margin:0;padding:0">'+
                                    '<h3 class="box-title" style="margin-top:4px;margin-left:4px;margin-bottom:2px">Company <span id="memberModGetCompany"></span></h3>'+
                                '</div>'+
                                '<div class="col-xs-5 pull-right" style="margin:0;padding:0">'+
                                    '<div class="input-group" >'+
                                    '<input type="text" id="searchEvntMtr2" class="form-control" placeholder="Enter any keyword to search." onkeyup="searchDefaultByEnter2()">'+
                                    '<span class="input-group-btn">'+
                                        '<button type="submit" id="searchEvntMtrBtn" onclick="searchDefault()" class="btn btn-flat" ><i class="fa fa-search"></i></button>'+
                                    '</span>'+
                                '</div>'+
                                '<div class="pull-right" id="searchDiv"><a href="javascript:void(0)" >Show Advance Search</a></div>'+
                            '</div>'+ // box
                        '</div>'+ // col-xs-12
                        '<div class="row" style="margin-left:2px;margin-right:2px">'+
                            '<div class="col-xs-6">'+
                                '<div class="input-group">'+
                                    '<div class="input-group-addon">'+
                                        '<i class="fa fa-calendar"></i>'+
                                    '</div>'+
                                    '<input type="text" class="form-control pull-right" id="member_date_range">'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-xs-6" id="account_buttons">'+
                                '<a class="btn btn-primary btn-md pull-right" onClick=contentMod("card_transmittal","'+id+'")>Card Transmittal</a>'+
                            '</div>'+
                        '</div>'+
                        '<div id="tablediv2" class="box-body table-responsive" style="margin-top:10px;padding-top:0"></div>'+
                        '<br><br><br>'
                        :
                        '<div style="paddding:10px;margin:20px">No don\'t have an access to this module, kindly contact the system admin. Thank you.<br><br><br><br></div>'
                        ) +
                    '</div>'+ // row
                 '</section>'
            ); 
             getMemberModInfo(id);
             $('#member_date_range').daterangepicker();
             tableAJAX(null,'active','',mod);
             reload_table_member();
        }else if(mod == "card_transmittal"){
            $('#account_buttons').html(
                '<a class="btn btn-primary btn-md pull-right" onClick=contentMod("member_list","'+id+'")>Member List</a>'
            );
            $('#breadcrumb_member').html(
                '<ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0">'+
                    '<li><a href="javascript:void(0)" style="margin-left:5px;font-size:16px;opacity:0.6">&emsp; Home</a></li>'+
                    '<li><a href="javascript:void(0)" style="margin-left:-5px;font-size:16px;opacity:0.6">Member</a></li>'+
                    '<li><a href="javascript:void(0)" style="margin-left:-5px;font-size:16px">View Member List</a></li>'+
                    '<li><a href="javascript:void(0)" style="margin-left:-5px;font-size:16px">Card Transmittal</a></li>'+
                '</ol>'
            )
            tableAJAX(null,'active','',mod);
        }    
     } // function contentMod(mod){

     function getMemberModInfo(id){
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/getMemberModInfo'); ?>',
            data:{id:id},
            dataType:'json',
            beforeSend:function(){
                 $('#memberModGetCompany').val('Loading...');
            },
            success:function (data){
                $('#memberModGetCompany').html(' - <b style="color:#3c8dbc">' + data.comp_set_code+ ' - ' + data.comp_set_shortname+'</b>');
            }
        });
     }    

     function getMemberModInfoMember(id){
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/getMemberModInfo'); ?>',
            data:{id:id},
            dataType:'json',
            beforeSend:function(){
                 $('#memberModGetCompany2').val('Loading...');
            },
            success:function (data){
                $('#memberModGetCompany2').html(' - <b style="color:#3c8dbc">' + data.comp_set_code+ ' - ' + data.comp_set_shortname+'</b>');
            }
            
        });
     }  
     
    function getCompanyAPI(){
         $('#myModal').modal('show');
         $('#mod-title').html("Get API");
         $('#mod-content').html('<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+
          '<div id="ApiDiv"></div>');
          
         $.ajax({
            type:'POST',
            url:'<?php echo base_url('account/getCompanyAPI'); ?>',
            dataType:'json',
            beforeSend:function(){
                $('.loadingFORM').removeClass('hidden');
                $(".loadingFORM").fadeIn();
                $('#ApiDiv').css('opacity','0.1');
                $('#ApiDiv').attr('disabled',true);
                $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                    'width':'190px',
                    'height':'50px',
                    'border-radius':'5px',
                    'background-color':'white',
                    'padding':'15px',
                    'color':'black',
                }).addClass('alpha60');
            },
            success:function (data){
               $('.loadingFORM').html('Successfully uploaded').addClass('hidden');
                $('#ApiDiv').css('opacity','10');
                if(data.status ==  "added"){
                    $('#ApiDiv').html('<center><h3>Successfully Added new company api records</h3></center>').css('color','green');
                }else{
                    $('#ApiDiv').html('<center><h3>Successfully update company api records</h3></center>').css('color','green');
                }
                reload_table_company();
            }
            
        });
    }
    function reload_table_company(){
        table_company.ajax.reload(null,false); //reload datatable ajax 
    }

    function reload_table_member(){
        table_member.ajax.reload(null,false); //reload datatable ajax 
    }

    function tableAJAX(action = null, status='active', make_mul_action='',mod=''){
    if(mod == "company_list"){ 
        $("#tablediv").html(
            '<table id="ajaxTbl" class="display" width="100%" >'+
            '<thead>'+
            	'<tr>'+
                    '<th><center><input type="checkbox" value="" class="selectall"></center></th>' +
                    '<th></th>'+ 
            		'<th>Company Code</th>'+  
            		'<th>Company Name</th>'+
            		'<th><center>Policy Period</center></th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</thead>'+
            '<tbody></tbody>'+
            '<tfoot hidden>'+
            	'<tr>'+
                    '<th><input type="checkbox" value="" class="selectall"></th>' +
                    '<th></th>'+ 
            		'<th>Company Code</th>'+  
            		'<th>Company Name</th>'+
            		'<th><center>Policy Period</center></th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</tfoot>'+
            '</table>'
        );
	    table_company = $('#ajaxTbl').DataTable({
       	    "processing": true, 
            "serverSide": true, 
            "scrollY":   '60vh',
            "fixedHeader": {
                header: true,
                footer: true
            },
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('Account/ajax_list')?>",
                "type": "POST",
                "data":{
                    mod:mod,
                    user_status:status, 
                    umm_upd_info_pass_avtr: "<?php if(array_search('umm_upd_info_pass_avtr', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>",
                    umm_assgn_res: "<?php if(array_search('umm_assgn_res', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>"
                },
                 "error": function(xhr, ajaxOptions, thrownError) {
                     if (xhr.status == 403) {
                        table_company.settings()[0].jqXHR;
                     }
                 }
            },
            "language": {
                processing:
                "<img src='" +
                http_assets_path +
                "custom/img/ajax-loader.gif' width='2%'>&emsp; Please Wait... (Reload the page if doesn't work.) ",
                "infoFiltered": " "
            },
            "columnDefs": [
                {targets:0, orderable: false, width: "3%", searchable: false, className: 'select-checkbox'},
                {targets:1, orderable: false, visible: false },
                {targets:2, width: "20%" },
                {targets:3, width: "20%" },
                {targets:4, width: "20%" },
                {targets:5, orderable: false, width: "2%", searchable: false}
            ],
            select: {
                style:    'os',
                selector: 'td:first-child'
            },
        });
        $.fn.dataTable.ext.errMode = "none";
        table_company.on('xhr', function() { });
        table_company.on("draw", function() { });
        $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	});
    	$(".selectall").on("ifChanged", selectallfunc);
       
    }else if(mod == 'member_list'){
            $("#tablediv2").html(
                '<table id="ajaxTbl2" class="display" width="100%" >'+
                '<thead>'+
                    '<tr>'+
                        '<th><center><input type="checkbox" value="" class="selectall2"></center></th>' +
                        '<th></th>'+ 
                        '<th>Member ID</th>'+  
                        '<th>Enrollment Date</th>'+
                        '<th>Member Name</th>'+
                        '<th>Member Type</th>'+
                        '<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody></tbody>'+
                '<tfoot hidden>'+
                    '<tr>'+
                        '<th><input type="checkbox" value="" class="selectall2"></th>' +
                        '<th></th>'+ 
                        '<th>Member ID</th>'+  
                        '<th>Enrollment Date</th>'+
                        '<th>Member Name</th>'+
                        '<th>Member Type</th>'+
                        '<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
                    '</tr>'+
                '</tfoot>'+
                '</table>'
            );

            table_member = $('#ajaxTbl2').DataTable({
                "processing": true, 
                "serverSide": true, 
                "scrollY":   '60vh',
                "fixedHeader": {
                    header: true,
                    footer: true
                },
                "order": [], 
                "ajax": {
                    "url": "<?php echo site_url('Account/ajax_list2')?>",
                    "type": "POST",
                    "data":{
                        mod:mod,
                        user_status:status, 
                        umm_upd_info_pass_avtr: "<?php if(array_search('umm_upd_info_pass_avtr', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>",
                        umm_assgn_res: "<?php if(array_search('umm_assgn_res', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>"
                    },
                    "error": function(xhr, ajaxOptions, thrownError) {
                        if (xhr.status == 403) {
                            table_company.settings()[0].jqXHR;
                        }
                    }
                },
                "language": {
                    processing:
                    "<img src='" +
                    http_assets_path +
                    "custom/img/ajax-loader.gif' width='2%'>&emsp; Please Wait... (Reload the page if doesn't work.) ",
                    "infoFiltered": " "
                },
                "columnDefs": [
                    {targets:0, orderable: false, width: "3%", searchable: false, className: 'select-checkbox'},
                    {targets:1, orderable: false, visible: false },
                    {targets:2, width: "10%" },
                    {targets:3, width: "20%" },
                    {targets:4, width: "20%" },
                    {targets:5, width: "20%" },
                    {targets:6, orderable: false, width: "2%", searchable: false}
                ],
                select: {
                    style:    'os',
                    selector: 'td:first-child'
                },
            });

            $.fn.dataTable.ext.errMode = "none";
            table_member.on('xhr', function() { });
            
            table_member.on("draw", function() { });
        }else if(mod == "card_transmittal"){
            $("#tablediv2").html(
                '<table id="ajaxTbl2" class="display" width="100%" >'+
                '<thead>'+
                    '<tr>'+
                        '<th><center><input type="checkbox" value="" class="selectall2"></center></th>' +
                        '<th></th>'+ 
                        '<th>Member ID</th>'+  
                        '<th>Enrollment Date</th>'+
                        '<th>Member Name</th>'+
                        '<th>Member Type</th>'+
                        '<th style="background-color:#b39ddb">Batch ID</th>'+
                        '<th style="background-color:#b39ddb">ID Status</th>'+
                        '<th style="background-color:#b39ddb">Date</th>'+
                        '<th style="background-color:#b39ddb">Action</th>'+
                        '<th style="background-color:#b39ddb">Reference</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody></tbody>'+
                '<tfoot hidden>'+
                    '<tr>'+
                        '<th><input type="checkbox" value="" class="selectall2"></th>' +
                        '<th></th>'+ 
                        '<th>Member ID</th>'+  
                        '<th>Enrollment Date</th>'+
                        '<th>Member Name</th>'+
                        '<th>Member Type</th>'+
                        '<th style="background-color:#b39ddb">Batch ID</th>'+
                        '<th style="background-color:#b39ddb">ID Status</th>'+
                        '<th style="background-color:#b39ddb">Date</th>'+
                        '<th style="background-color:#b39ddb">Action</th>'+
                        '<th style="background-color:#b39ddb">Reference</th>'+
                    '</tr>'+
                '</tfoot>'+
                '</table>'
            );

            table_member = $('#ajaxTbl2').DataTable({
            "processing": true, 
                "serverSide": true, 
                "scrollY":   '40vh',
                "fixedHeader": {
                    header: true,
                    footer: true
                },
                "order": [], 
                "ajax": {
                    "url": "<?php echo site_url('Account/ajax_list3')?>",
                    "type": "POST",
                    "data":{
                        mod:mod,
                        user_status:status, 
                        umm_upd_info_pass_avtr: "<?php if(array_search('umm_upd_info_pass_avtr', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>",
                        umm_assgn_res: "<?php if(array_search('umm_assgn_res', $db->main_sub_header['responsibility']) !== false){ echo "TRUE"; }else{ echo "FALSE"; } ?>"
                    },
                    "error": function(xhr, ajaxOptions, thrownError) {
                        if (xhr.status == 403) {
                            table_company.settings()[0].jqXHR;
                        }
                    }
                },
                "language": {
                    processing:
                    "<img src='" +
                    http_assets_path +
                    "custom/img/ajax-loader.gif' width='2%'>&emsp; Please Wait... (Reload the page if doesn't work.) ",
                    "infoFiltered": " "
                },
                "columnDefs": [
                    {targets:0, orderable: false, width: "3%", searchable: false, className: 'select-checkbox'},
                    {targets:1, orderable: false, visible: false },
                    {targets:2, width: "10%" },
                    {targets:3, width: "15%" }, // 
                    {targets:4, width: "15%" },
                    {targets:5, width: "15%" }, // Member Type
                    {targets:6, width: "10%",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            $(td).css('background-color', '#ede7f6');
                        }
                    }, // Batch ID
                    {targets:7, width: "10%",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            $(td).css('background-color', '#ede7f6');
                        }
                    },
                    {targets:8, width: "10%",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            $(td).css('background-color', '#ede7f6');
                        }
                    },
                    {targets:9, width: "10%",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            $(td).css('background-color', '#ede7f6');
                        }
                    },
                    {targets:10, width: "10%",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            $(td).css('background-color', '#ede7f6');
                        }
                    },
                ],
                "createdRow": function( row, data, dataIndex){
                    // data[6] value
                    // console.log(data.name);
                    // if(data[6] == "Batch ID"){
                    //     $(row).css('background-color', '#b39ddb');
                    // }
                },
                select: {
                    style:    'os',
                    selector: 'td:first-child'
                },
            });

            $.fn.dataTable.ext.errMode = "none";
            table_member.on('xhr', function() { });
            
            table_member.on("draw", function() { });
        }
 
        $('.selectall2').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	});
    	
    	$(".selectall2").on("ifChanged", selectallfunc2);
    } // end of tableajax
    
    function selectallfunc(e){
        if(e.target.checked){
            $('.selectall').iCheck('check'); 
            table_company.rows(  ).select();   
        }else{
            $('.selectall').iCheck('uncheck'); 
            table_company.rows(  ).deselect(); 
        }
    }

    function selectallfunc2(e){
        if(e.target.checked){
            $('.selectall2').iCheck('check'); 
            table_company.rows(  ).select();   
        }else{
            $('.selectall2').iCheck('uncheck'); 
            table_company.rows(  ).deselect(); 
        }
    }
    
    function searchDefault(){
        table_company.search($("#searchEvntMtr").val()).draw();
    } 

    function searchDefault2(){
        table_company.search($("#searchEvntMtr2").val()).draw();
    } 
    
    function searchDefaultByEnter(){
        var keyPressed = event.keyCode || event.which;
        if(keyPressed==13){
            table_company.search($("#searchEvntMtr").val()).draw();
             keyPressed=null;
        }else{
            return false;
        }
    }

    function searchDefaultByEnter2(){
        var keyPressed = event.keyCode || event.which;
        if(keyPressed==13){
            table_company.search($("#searchEvntMtr2").val()).draw();
             keyPressed=null;
        }else{
            return false;
        }
    }

    var member_id_pass, pass_method_enrl_mem;
    function enrollMemberAction(action,method,id){
    contentMod("member_list",id);
        if(action == 'view'){
            $('#btn-header').html(''); 
    		$('#myModal').modal({backdrop: 'static', keyboard: false});
    		$('#mod-title').html("<b>Member > Enroll Member/s</b>");
    		$('#myModal .modal-dialog').addClass('modal-lg');

            $('#mod-content').html(
            '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+

            '<form id="Form1">'+
                '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
                '<input type="hidden" name="comp_setup_user_id" value="<?=$u_id_enc;?>" />'+
                '<input type="hidden" name="edit_id" value="" />'+
                '<input type="hidden" name="member_limit_arr" id="member_limit_arr" value="0">'+
                '<div style="margin-top:0;margin-left:15px;margin-bottom:5px">'+
                        '<div style="padding:0;font-size:16px;font-weight:400;float:left" class="hidden"></div>'+
                        '<div id="div_info"></div>'+
                '</div>'+
                '<div style="clear:both"></div>'+

                '<div id="Form0Div" class="hidden">'+  
                    '<div class="col-xs-2"></div>'+
                    '<div class="col-xs-4" style="padding-top:20px;padding-bottom:20px;border-right:2px solid black">'+
                        '<div class="form-group">'+
                            '<label style="margin-left:20px">'+
                                '<input type="radio" name="mode_setup" class="minimal selectall pull-left" value="bnt_mtx" checked>'+
                                '<div style="font-size:16px;float:right;margin-left:10px">Enroll a member</div>'+
                            '</label>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-xs-4" style="padding-top:20px;padding-bottom:20px">'+
                        '<div class="form-group">'+
                            '<center><label>'+
                                '<input type="radio" name="mode_setup" class="minimal selectall pull-left" value="stp_blk">'+
                                '<div style="font-size:16px;float:right;margin-left:10px">Enroll by bulk</div>'+
                            '</label></center>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-xs-2"></div>'+
                '</div>'+

                '<div id="Form1Div-2" class="hidden">'+  
                    '<div class="col-xs-12">'+
                            '<div class="form-group">'+
                                '<label for="exampleInputFile">Create by bulk (xlsx, xls)</label>'+
                                '<input type="file" id="exampleInputFile">'+
                                '<p class="help-block">Download Template here:</p>'+
                            '</div>'+
                    '</div>'+
                '</div>'+ 
                
                '<div id="Form1Div" class="hidden" style="margin-top:20px">'+
                    '<div class="col-xs-6">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Member Existing ID</label>'+
                            '<input type="text" class="form-control" name="member_exst_id_enroll_mem"  placeholder="Enter Existing ID">'+
                            '<span class="help-block"></span>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label class="control-label">Member </label>'+
                            '<select class="form-control" name="member_type_enroll_mem">'+
                                '<option value="">Select Type</option>'+
                                '<option value="1">Sample Value</option>'+
                            '</select>'+
                            '<span class="help-block"></span>'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label class="control-label">Type / Subtype <b style="color:red">*</b></label>'+
                            '<button class="btn btn-xs btn-primary pull-right" style="font-size:12px;margin:4px" onclick=add_dependent_enroll_mem("view") id="addModalities">Add Dependent</button>'+
                            '<select class="form-control" name="sub_type_enroll_mem">'+
                                '<option value="">Select Sub-type</option>'+
                                '<option value="1">value 1</option>'+
                            '</select>'+
                            '<span class="help-block"></span>'+
                        '</div>'+
                        '<div class="form-group">'+
        						  '<label class="control-label">First Name</label>'+
        						  '<input type="text" name="first_name_enroll_mem" class="form-control form-val" maxlength="50" placeholder="Enter First Name">'+
        						  '<span class="help-block"></span>'+
        				'</div>'+
                        '<div class="form-group">'+
        						  '<label class="control-label">Last Name</label>'+
        						  '<input type="text" name="last_name_enroll_mem" class="form-control form-val" maxlength="50" placeholder="Enter Last Name">'+
        						  '<span class="help-block"></span>'+
        				'</div>'+
                        '<div class="form-group">'+
        						  '<label class="control-label">Middle Name (Optional)</label>'+
        						  '<input type="text" name="middle_name_enroll_mem" class="form-control form-val" maxlength="50" placeholder="Enter Middle Name">'+
        						  '<span class="help-block"></span>'+
        				'</div>'+
                    '</div>'+
                    '<div class="col-xs-6">'+
                            '<div class="form-group">'+
                                    '<label class="control-label">Hired Date <b style="color:red">*</b></label>'+
                                    '<input type="text" class="form-control" data-provide="datepicker" id="hired_date_enroll_mem" name="hired_date_enroll_mem"  Placeholder="Enter Hired Date" >'+
                                    '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">Martial Status <b style="color:red">*</b></label>'+
                                '<select class="form-control" name="martial_status_enroll_mem">'+
                                    '<option value="">Select Martial Status</option>'+
                                    '<option value="0">Single</option>'+
                                    '<option value="1">Married</option>'+
                                    '<option value="2">Widowed</option>'+
                                    '<option value="3">Divorced</option>'+
                                    '<option value="4">Seperated</option>'+
                                '</select>'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">Gender <b style="color:red">*</b></label>'+
                                '<select class="form-control" name="gender_enroll_mem">'+
                                    '<option value="">Select Gender</option>'+
                                    '<option value="0">Male</option>'+
                                    '<option value="1">Female</option>'+
                                '</select>'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                    '<label class="control-label">Birth Date <b style="color:red">*</b></label>'+
                                    '<input type="text" class="form-control" data-provide="datepicker" id="birth_date_enroll_mem" name="birth_date_enroll_mem"  Placeholder="Enter Birth Date" >'+
                                    '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
        						  '<label class="control-label">Age <b style="color:red">*</b></label>'+
        						  '<input type="text" id="age_enroll_mem" name="age_enroll_mem"  class="form-control form-val" maxlength="50" placeholder="Enter Age">'+
        						  '<span class="help-block"></span>'+
        				'</div>'+
                    '</div>'+
                '</div>'+
                '</form>'+
            '<div style="clear:both"><div>'+
            '<div id="require_mess" class="hidden" style="margin-left:15px;padding-top:15px;font-weight:600">Note: Fields with <b style="color:red">*</b> are mandatory.<div>'
            );

            $('#mod-footer').html('<button id="Form0Btn"  class="btn btn-success" onclick=member_setup_breadcrumb("phase0",'+null+',"'+method+'") >Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');	
            	 
            $('#Form0Div').removeClass('hidden');
            formPlugins3("phase0"); 
            member_id_pass = id;
            pass_method_enrl_mem = method;
            formPlugins3("phase1"); 
        }
    }

    function member_setup_breadcrumb(page,returnpage=null,method){
        if(page == "phase0"){
            $('#Form0Div').removeClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');

            $('#mod-title').html("<b>Select Mode of Setup</b>");
            $('#wizard-setup').addClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#div_info').html('');
            $('#notif-mess').html(' ');

            $('#mod-footer').html('<button id="Form0Btn"  class="btn btn-success" onclick=benefit_setup_breadcrumb("phase0",'+null+',"'+method+'") >Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            if(returnpage == null){
                if($('[name="mode_setup"]:checked').val() == "bnt_mtx"){
                    member_setup_breadcrumb("phase1","back",pass_method_enrl_mem);
                }else{
                    member_setup_breadcrumb("phase1-2","back",pass_method_enrl_mem);
                }
            }
        }else if(page == "phase1-2"){
                $('#Form0Div').addClass('hidden');
                $('#Form1Div').addClass('hidden'); $('#Form1Div-2').removeClass('hidden');

                $('#mod-title').html("<b>Member > Enroll Member/s - Upload Bulk</b>");
                $('#wizard-setup').addClass('hidden');
                $('#require_mess').addClass('hidden');
                $('#div_info').html('');
                $('#notif-mess').html(' ');
                formPlugins3("phase1-2"); 

                $('#mod-footer').html(
                '<button id="Form1Btn-2-modsetup" class="btn btn-default" onclick=member_setup_breadcrumb("phase0","back","'+method+'") >Mode of Setup</button>'+
                '<button id="Form1Btn-2" class="btn btn-success" onclick=member_setup_breadcrumb("phase1-2") >Upload</button>');
        }else if(page == "phase1"){
            
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').removeClass('hidden'); $('#Form1Div-2').addClass('hidden');

            $('#mod-title').html("<b>Enroll Basic Information</b>");
            $('#require_mess').addClass('hidden');
            $('#div_info').html('<h3 class="box-title" style="margin-top:4px;margin-left:0;margin-bottom:2px;font-size:16px"><b>Company</b> <span id="memberModGetCompany2"></span></h3>');
            $('#notif-mess').html(' ');
            getMemberModInfoMember(member_id_pass);
            
           

            $('#mod-footer').html(
            '<button id="Form1Btn-2-modsetup" class="btn btn-default" onclick=member_setup_breadcrumb("phase0","back","'+method+'") >Mode of Setup</button>'+
            '<button id="Form1Btn" class="btn btn-success" onclick=member_setup_breadcrumb("phase1") >Save &emsp;<i class="fa fa-chevron-right"></i></button>');    

            if(returnpage == null){
                console.log(page +' ------ ' + pass_method_enrl_mem);
                var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_enrl_mem;
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('Account/memberSetupFormValidate'); ?>',
                    data:form,
                    dataType:'json',
                    beforeSend:function(){
                        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#Form1').css('opacity','0.1');
                        $('#Form1 :input').attr('disabled', true);
                        $('#Form1Btn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
                    },
                    success:function(data){
                        $('.loadingFORM').html('').addClass('hidden');
                        $('#Form1').css('opacity','10');
                        $('#Form1 :input').attr('disabled', false);
                        $('#Form1 select').attr('disabled', false);
                        $('#Form1 textarea').attr('disabled', false);
                        $('#Form1Btn').attr('disabled',false);
                    
                        if(data.status){
                            if(returnpage == null){
                                $('.form-group').removeClass('has-error'); // clear error class
                                $('.select2not').removeClass('input-validation-error');
                                $('.help-block').empty();
                                member_setup_breadcrumb('confirmation',"back");
                            }
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                            }
                        }
                    }
                })
            
            }
        }else if(page == "confirmation"){
                $('#mod-footer').html(
                    '<button id="FormDecideCancelBtn" class="btn btn-default pull-right" style="margin-left:5px" onclick=member_setup_breadcrumb("phase1","back")>Cancel</button>'+
                    '<button id="FormDecideNoBtn"  class="btn btn-success pull-right" style="margin-left:5px" onclick=member_setup_breadcrumb("save")>Save</button>'
                );
                
                $('#notif-mess').html('<p style="text-align:left;font-weight:600"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Are you sure do you want save this form?</p>').css('color','orange');
        }else if(page == "save"){
                $('#notif-mess').html('');

                var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_enrl_mem;
                $('.form-group').removeClass('has-error'); 
                $('.help-block').empty();
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('Account/memberSetupFormValidate'); ?>',
                    data:form,
                    dataType:'json',
                    beforeSend:function(){
                        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#Form1').css('opacity','0.1');
                        $('#Form1 :input').attr('disabled', true);
                        $('#Form1Btn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
                    },
                    success:function(data){
                        $('.loadingFORM').html('').addClass('hidden');
                        $('#Form1').css('opacity','10');
                        $('#Form1 :input').attr('disabled', false);
                        $('#Form1 select').attr('disabled', false);
                        $('#Form1 textarea').attr('disabled', false);
                        $('#Form1Btn').attr('disabled',false);
                        reload_table_member();
                        
                        if(data.status){
                                if(returnpage == null){
                                    $('.form-group').removeClass('has-error'); 
                                    $('.help-block').empty();
                                }
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                            }
                        }
                    }
                });
                

                if(pass_method_enrl_mem == "update"){
                    var suc_mes = '<p style="text-align:center;font-weight:600;font-size:26px;color:green"><i class="fa fa-check-circle" aria-hidden="true"></i> A Member Basic Info has been Updated!</p>';
                }else{
                    var suc_mes = '<p style="text-align:center;font-weight:600;font-size:26px;color:green"><i class="fa fa-check-circle" aria-hidden="true"></i> A New Member has been enrolled!</p>';
                }
                $('#mod-footer').html('<button type="button"    class="btn btn-default pull-right" data-dismiss="modal">Close</button>');
                $('#require_mess').html(' ');
                $('#Form1').html(suc_mes+
                '<p style="text-align:center;font-weight:600;font-size:16px;color:black">Go to the table and check the update.</p>');
       
        }
    }

    function comp_setup(action,method,id){
      	 if(action == 'view'){
    		$('#btn-header').html(''); 
    		$('#myModal').modal({backdrop: 'static', keyboard: false});
    		$('#mod-title').html("<b>Company Setup</b>");
    		$('#myModal .modal-dialog').addClass('modal-lg');
    		
    		$('#mod-content').html(
    		     '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+
    
    		     '<div class="row bs-wizard" style="border-bottom:0;padding-top:0;margin-top:0">'+
                    '<div class="col-xs-4 bs-wizard-step active" id="comp_set_prog_basicInfo">'+
                      '<div class="text-center bs-wizard-stepnum" id="comp_set_prog_basicInfo_txt">Company Basic Information</div>'+
                      '<div class="progress"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot"></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                    '<div class="col-xs-4 bs-wizard-step disabled" id="comp_set_prog_billing">'+
                      '<div class="text-center bs-wizard-stepnum" id="comp_set_prog_billing_txt">Company Billing Information</div>'+
                      '<div class="progress"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot"></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                    '<div class="col-xs-4 bs-wizard-step disabled" id="comp_set_prog_contract">'+
                      '<div class="text-center bs-wizard-stepnum" id="comp_set_prog_contract_txt">Company Contract Information</div>'+
                      '<div class="progress"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot"></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                '</div>'+
                
    			 '<form id="CompSetupForm">'+
    			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
    			 '<input type="hidden" name="comp_setup_user_id" value="<?=$u_id_enc;?>" />'+
    			 '<input type="hidden" name="edit_id" value="" />'+
    			            '<div style="margin-top:0;margin-left:15px;margin-bottom:5px">'+
    			            // id="title_comp_setup"
        						  '<div style="padding:0;font-size:16px;font-weight:400;float:left" class="hidden"></div><div style="float:left;font-weight:600;font-size:18px;margin-top:0" id="company_name"></div>'+
        					'</div>'+
        					'<div style="clear:both"></div>'+
    			    '<div id="CompBasicInfoFromDiv">'+
            			'<div class="col-xs-6">'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Code <b style="color:red">*</b></label><button class="btn btn-xs btn-default pull-right" onclick="compSetGenerateCode()">Generate</button>'+
        						  '<input type="text" name="comp_set_code" class="form-control form-val" maxlength="100" placeholder="Enter Code" value="<?=$gen_code;?>">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Shortname</label>'+
        						  '<input type="text" name="comp_set_shortname" class="form-control form-val" maxlength="100" placeholder="Enter Shortname">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label>Type <b style="color:red">*</b></label>'+
        						  '<select class="form-control" name="comp_set_type">'+
        							'<option value="">Select Type</option>'+
        							'<option value="1">Actual Cost</option>'+
                                    '<option value="1">Cost Plus</option>'+
        						  '</select>'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Company Name <b style="color:red">*</b></label>'+
        						  '<input type="text" name="comp_set_company_name" class="form-control form-val" maxlength="255" placeholder="Enter Company">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label>Address</label>'+
                                   '<textarea class="form-control" name="comp_set_address" rows="2" placeholder="Enter Address" style="resize:vertical" maxlength="500"></textarea><small>Maximum of 500 characters</small>'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Office Number</label>'+
        						  '<input type="text" name="comp_set_office_number" class="form-control form-val" maxlength="50" placeholder="Enter Office Number">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Fax Number</label>'+
        						  '<input type="text" name="comp_set_fax_number" class="form-control form-val" maxlength="50" placeholder="Fax Number">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Website</label>'+
        						  '<input type="text" name="comp_set_website" class="form-control form-val" maxlength="100" placeholder="Enter Website">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        				 '</div>'+
        				'<div class="col-xs-6">'+
        				    '<div class="form-group">'+
        						  '<label class="control-label">SEC Number <b style="color:red">*</b></label>'+
        						  '<input type="text" name="comp_set_sec_number" class="form-control form-val" maxlength="100" placeholder="Enter SEC Number">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">TAX ID / TIN No. <b style="color:red">*</b></label>'+
        						  '<input type="text" name="comp_set_tax_id_or_tin_no" class="form-control form-val" maxlength="100" placeholder="Enter TAX ID / TIN No.">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">DTI No. <b style="color:red">*</b></label>'+
        						  '<input type="text" name="comp_set_dti_no" class="form-control form-val" maxlength="100" placeholder="Enter DTI No.">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Payee Code. <b style="color:red">*</b></label>'+
        						  '<input type="text" name="comp_set_payee_code" class="form-control form-val" maxlength="100" placeholder="Enter Payee Code">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        				 '</div>'+
    				'</div>'+ 
    				
    				
    			    '<div id="CompBilInfoDiv" class="hidden">'+
    				    	'<div class="col-xs-6">'+
            					'<div class="form-group">'+
            						  '<label class="control-label">Billing Setup <b style="color:red">*</b></label>'+
            						  '<input type="text" name="comp_bil_billing_setup" class="form-control form-val" maxlength="100" placeholder="Enter Billing Setup">'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
            					'<div class="form-group">'+
            						   '<label>Send Billing Setup <b style="color:red">*</b></label>'+
            						   '<select class="form-control" name="comp_bil_send_billing_setup">'+
            						   '<option value="">Select Send Billing Setup</option>'+
            							'<option value="1">Send Billing to this company</option>'+
            						  '</select>'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
            				    '<div class="form-group">'+
            						  '<br>'+
            					'</div>'+
            					
            					'<div class="form-group">'+
            						  '<label class="control-label">VAT Type <b style="color:red">*</b></label>'+
            						  '<select class="form-control" name="comp_bil_vat_type">'+
            						    '<option value="">Select Vat Type</option>'+
            							'<option value="1">Vatable</option>'+
            						  '</select>'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
                				'<div class="form-group">' +
                                    '<label style="clear:both">WTax Code & Rate:</label>' +
                                    '<div class="input-group col-xs-12">' +
                                        '<div class="form-group">' +
                                            '<select class="form-control " style="width:1" name="comp_bil_vtax_code">'+
                							'<option value="">Select WTax Code</option>'+
                							'<option value="1">Sample Value</option>'+
                						  '</select>'+
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                        '<span class="input-group-btn" style="width:20px;"></span>' +
                                        
                                        '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_vtax_code_rate" class="form-control input-md " maxlength="50" placeholder="Rate">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                    '</div>'+
                                '</div>' +
            					'<div class="form-group">' +
                                    '<label style="clear:both">IP Admin Fee:</label>' +
                                    '<div class="input-group col-xs-12">' +
                                         '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_ip_admin_fee_rate" class="form-control input-md " maxlength="50" placeholder="Rate">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                        '<span class="input-group-btn" style="width:20px;"></span>' +
                                        '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_ip_admin_fee_amount" class="form-control input-md " maxlength="50" placeholder="Amount">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                    '</div>'+
                                '</div>' +
            					'<div class="form-group">' +
                                    '<label style="clear:both">OP Admin Fee:</label>' +
                                    '<div class="input-group col-xs-12">' +
                                         '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_op_admin_fee_rate" class="form-control input-md " maxlength="50" placeholder="Rate">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                        '<span class="input-group-btn" style="width:20px;"></span>' +
                                        '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_op_admin_fee_amount" class="form-control input-md " maxlength="50" placeholder="Amount">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                    '</div>'+
                                '</div>' +
        				'</div>'+
        				'<div class="col-xs-6">'+
            					'<div class="form-group">'+
            						  '<label class="control-label">Send Billing To <b style="color:red">*</b></label>'+
            						  '<input type="text" name="comp_bil_send_billing_to" class="form-control form-val" placeholder="Enter Send Billing To">'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
            					 '<div class="form-group">'+
            						  '<br>'+
            					'</div>'+
            				    '<div class="form-group">'+
            						  '<br>'+
            					'</div>'+
            					 '<div class="form-group">'+
            						  '<br>'+
            					'</div>'+
                				'<div class="form-group">' +
                                    '<label style="clear:both">RE Admin Fee:</label>' +
                                    '<div class="input-group col-xs-12">' +
                                         '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_re_admin_fee_rate" class="form-control input-md " maxlength="50" placeholder="Rate">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                        '<span class="input-group-btn" style="width:20px;"></span>' +
                                        '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_re_admin_fee_amount" class="form-control input-md " maxlength="50" placeholder="Amount">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                    '</div>'+
                                '</div>' +
            					'<div class="form-group">' +
                                    '<label style="clear:both">DC Admin Fee:</label>' +
                                    '<div class="input-group col-xs-12">' +
                                         '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_dc_admin_fee_rate" class="form-control input-md " maxlength="50" placeholder="Rate">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                        '<span class="input-group-btn" style="width:20px;"></span>' +
                                        '<div class="form-group ">' +
                                            '<input type="text"  name="comp_bil_dc_admin_fee_amount" class="form-control input-md " maxlength="50" placeholder="Amount">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                    '</div>'+
                                '</div>' +
            					'<div class="form-group">' +
                                    '<label style="clear:both">NAF Amount</label>' +
                                    '<div class="input-group col-xs-12">' +
                                         '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_naf_rate" class="form-control input-md " maxlength="50" placeholder="Rate">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                        '<span class="input-group-btn" style="width:20px;"></span>' +
                                        '<div class="form-group ">' +
                                            '<input type="text" name="comp_bil_naf_amount" class="form-control input-md " maxlength="50" placeholder="Amount">' +
                                            '<span class="help-block"></span>' +
                                        '</div>' +
                                    '</div>'+
                                '</div>' +
        				'</div>'+
    				'</div>'+ 
    				
    				
    				'<div id="CompContInfoDiv" class="hidden">'+
    				    	'<div class="col-xs-6">'+
            					'<div class="form-group">'+
            						  '<label class="control-label">Effectivity <b style="color:red">*</b></label>'+
            						  '<input type="text" class="form-control" data-provide="datepicker" id="comp_contr_effectivity" name="comp_contr_effectivity"  Placeholder="Date Requested" >'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
            				    '<div class="form-group">'+
            						  '<label class="control-label">Corporate Limit </label>'+
            						  '<input type="text" name="comp_contr_corporate_limit" class="form-control form-val" maxlength="100" placeholder="Enter Corporate Limit">'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
        				'</div>'+
        				 
        			    '<div class="col-xs-6">'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Expiration <b style="color:red">*</b></label>'+
        						  '<input type="text" class="form-control" data-provide="datepicker" id="comp_contr_expiration" name="comp_contr_expiration"  Placeholder="Date Requested" >'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        				'</div>'+
        				
        				'<div class="col-xs-12">'+
        					'<div class="form-group">'+
        						  '<label>Remarks</label>'+
                                   '<textarea class="form-control" name="comp_contr_remarks" rows="2" placeholder="Enter remarks" maxlength="500" style="resize:vertical"></textarea><small>Maximum of 500 characters</small>'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        				'</div>'+
    				'</div>'+ 
    			 '</form>'+
    			 '<div style="clear:both"><div>'+
    			 '<div id="require_mess" style="margin-left:15px;padding-top:15px;font-weight:600">Note: Fields with <b style="color:red">*</b> are mandatory.<div>');
    			 
    			 
            $('#title_comp_setup').html('<b>Company Basic Information</b>'); 
            $('#comp_set_prog_basicInfo_txt').css({'font-weight':'600'});
            $('#mod-footer').html('<button id="CompBasicInfoBtn"  class="btn btn-success" onclick=comp_setup_breadcrumb("comp_basic_info",'+null+',"'+method+'") >Company Billing Info &emsp;<i class="fa fa-chevron-right"></i></button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');	 
    		pass_method	= method;
    		if(method == "update"){
    		    $('#mod-title').html("<b>Update Company Setup</b>");
    		    comp_setup_get_data('comp_basic_info',id);
    		}	 
    			 
    		formPlugins();	 
    	 }
    }

    function comp_setup_breadcrumb(page,returnpage=null,method){
        if(page == "comp_basic_info"){
            $('#CompBasicInfoFromDiv').removeClass('hidden');
            $('#CompBilInfoDiv').addClass('hidden');
            $('#CompContInfoDiv').addClass('hidden');
            $('#notif-mess').html(' ');
            $('#company_name').html('');
            
            $('#title_comp_setup').html('<b>Company Basic Information</b>'); 
            if(returnpage == "back"){
            setTimeout(function(){ 
            $('#comp_set_prog_basicInfo').removeClass('complete').addClass('active');
            }, 490);
            }
            $('#comp_set_prog_billing').removeClass('active').removeClass('complete').addClass('disabled');
            
            $('#comp_set_prog_basicInfo_txt').css({'font-weight':'600'});
            $('#comp_set_prog_contract_txt').css({'font-weight':'400'});
         
             $('#mod-footer').html('<button id="CompBasicInfoBtn"  class="btn btn-success" onclick=comp_setup_breadcrumb("comp_basic_info") >Company Billing Info &emsp;<i class="fa fa-chevron-right"></i></button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
    		var form = $('#CompSetupForm').serialize()+"&page="+page+"&method="+pass_method;;
        	$.ajax({
        		 type:'POST',
        		 url:'<?php echo site_url('Account/CompSetupFormValidate'); ?>',
        		 data:form,
        		 dataType:'json',
        		 beforeSend:function(){
    		        $('.loadingFORM').removeClass('hidden');
                    $(".loadingFORM").fadeIn();
                    $('#CompSetupForm').css('opacity','0.1');
                    $('#CompSetupForm :input').attr('disabled', true);
                    $('#CompBasicInfoBtn').attr('disabled',true);
                    $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                        'width':'190px',
                        'height':'50px',
                        'border-radius':'5px',
                        'background-color':'white',
                        'padding':'15px',
                        'color':'black',
                    }).addClass('alpha60');
    		     },
        		 success:function(data){
                    $('.loadingFORM').html('').addClass('hidden');
                    $('#CompSetupForm').css('opacity','10');
                    $('#CompSetupForm :input').attr('disabled', false);
                    $('#CompSetupForm select').attr('disabled', false);
                    $('#CompSetupForm textarea').attr('disabled', false);
                    $('#CompBasicInfoBtn').attr('disabled',false);
                
        		    if(data.status){
    				        if(returnpage == null){
    				                $('.form-group').removeClass('has-error'); // clear error class
                					$('.help-block').empty(); // clear error string
                					//$('#notif-mess').html('<b><i class="fa fa-check-circle" aria-hidden="true"></i> The company basic information has been valified.</b>').css('color','green');
    				            //setTimeout(function(){ 
    				               company_name =  $('[name="comp_set_company_name"]').val();
        					       comp_setup_breadcrumb('comp_bil_info',"back");
    				            //}, 1000);
    					    }
    				}else{
    					for (var i = 0; i < data.inputerror.length; i++){
    						$('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
    						$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
    						
    					}
    					
    				}
        		 }
        	})
        }else if(page == "comp_bil_info"){
            $('#CompBasicInfoFromDiv').addClass('hidden');
            $('#CompBilInfoDiv').removeClass('hidden');
            $('#CompContInfoDiv').addClass('hidden');
            $('#notif-mess').html(' ');
            
            $('#title_comp_setup').html('<b>Company Billing Information</b>'); 
            
            $('#comp_set_prog_basicInfo').addClass('complete').removeClass('active');
            if(returnpage == "back"){
            setTimeout(function(){ 
                $('#comp_set_prog_billing').removeClass('complete').addClass('active');
                setTimeout(function(){ 
                    $('#comp_set_prog_billing').addClass('disabled');
                }, 490);
             }, 490);
            }
           
            $('#comp_set_prog_contract').addClass('disabled').removeClass('active').removeClass('complete');

            $('#comp_set_prog_billing_txt').css({'font-weight':'bold'}); $('#comp_set_prog_basicInfo_txt').css({'font-weight':'400'});  $('#comp_set_prog_contract_txt').css({'font-weight':'400'});
            
            $('#company_name').html('Company Name: '+ company_name).css({'margin-bottom':'10px','font-size':'16px'});
            $('#mod-footer').html(
                '<button id="CompBasicInfoBtn"  class="btn btn-success pull-right" style="margin-left:5px" onclick=comp_setup_breadcrumb("comp_bil_info")> Company Contract Information &emsp;<i class="fa fa-chevron-right"></i></button>'+
                '<button id="CompBilInfoBtn"  class="btn btn-info pull-right" onclick=comp_setup_breadcrumb("comp_basic_info","back")><i class="fa fa-chevron-left"></i>&emsp; Company Basic Information</button>'
            );
            if(returnpage == null){
                var form = $('#CompSetupForm').serialize()+"&page="+page+"&method="+pass_method;;
            	$.ajax({
            		 type:'POST',
            		 url:'<?php echo site_url('Account/CompSetupFormValidate'); ?>',
            		 data:form,
            		 dataType:'json',
            		 beforeSend:function(){
        		        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#CompSetupForm').css('opacity','0.1');
                        $('#CompSetupForm :input').attr('disabled', true);
                        $('#CompBasicInfoBtn').attr('disabled',true);
                        $('#CompBilInfoBtn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
    		         },
            		 success:function(data){
                        $('.loadingFORM').html('').addClass('hidden');
                        $('#CompSetupForm').css('opacity','10');
                        $('#CompSetupForm :input').attr('disabled', false);
                        $('#CompSetupForm select').attr('disabled', false);
                        $('#CompSetupForm textarea').attr('disabled', false);
                        $('#CompBasicInfoBtn').attr('disabled',false);
                        $('#CompBilInfoBtn').attr('disabled',false);
                    
            		    if(data.status){
        				        if(returnpage == null){
        				                $('.form-group').removeClass('has-error'); // clear error class
                    					$('.help-block').empty(); // clear error string
                    					//$('#notif-mess').html('<p style="text-align:left;font-weight:600"><i class="fa fa-check-circle" aria-hidden="true"></i> The company billing information has<br> been valified.</p>').css('color','green');
        				            //setTimeout(function(){ 
            					       comp_setup_breadcrumb('comp_cont_info',"back");
        				            //}, 1000);
        					    }
        				}else{
        					for (var i = 0; i < data.inputerror.length; i++){
        					    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
        						$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
        					
        					}
        					
        				}
            		 }
            	})
               
            }
        }else if(page == "comp_cont_info"){
            $('#CompBasicInfoFromDiv').addClass('hidden');
            $('#CompBilInfoDiv').addClass('hidden');
            $('#CompContInfoDiv').removeClass('hidden');
             $('#notif-mess').html(' ');
             
            $('#title_comp_setup').html('<b>Company Contract Information</b>');
            $('#comp_set_prog_billing').removeClass('disabled').addClass('complete').removeClass('active');
            setTimeout(function(){ 
            $('#comp_set_prog_contract').addClass('active').removeClass('complete'); 
             }, 490);
            $('#comp_set_prog_contract_txt').css({'font-weight':'bold'}); $('#comp_set_prog_billing_txt').css({'font-weight':'400'}); $('#comp_set_prog_basicInfo_txt').css({'font-weight':'400'});
            
                    $('#mod-footer').html(
                    '<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>'+
                    '<button id="CompSaveInfoBtn"  class="btn btn-success pull-right" style="margin-right:5px;margin-left:5px" onclick=comp_setup_breadcrumb("comp_cont_info")>Save</button>'+
                    '<button id="CompContInfoBtn"  class="btn btn-info pull-right" style="margin-left:5px" onclick=comp_setup_breadcrumb("comp_bil_info","back")><i class="fa fa-chevron-left"></i>&emsp; Company Billing Info</button>');
                    var form = $('#CompSetupForm').serialize()+"&page="+page+"&method="+pass_method;;
                    if(returnpage == null){
                	    $.ajax({
                    		 type:'POST',
                    		 url:'<?php echo site_url('Account/CompSetupFormValidate'); ?>',
                    		 data:form,
                    		 dataType:'json',
                    		 beforeSend:function(){
                		        $('.loadingFORM').removeClass('hidden');
                                $(".loadingFORM").fadeIn();
                                $('#CompSetupForm').css('opacity','0.1');
                                $('#CompSetupForm :input').attr('disabled', true);
                                $('#CompContInfoBtn').attr('disabled',true);
                                $('#CompSaveInfoBtn').attr('disabled',true);
                                $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                                    'width':'190px',
                                    'height':'50px',
                                    'border-radius':'5px',
                                    'background-color':'white',
                                    'padding':'15px',
                                    'color':'black',
                                }).addClass('alpha60');
            		         },
                    		 success:function(data){
                    		    $('.loadingFORM').html('').addClass('hidden');
                                $('#CompSetupForm').css('opacity','10');
                                $('#CompSetupForm :input').attr('disabled', false);
                                $('#CompSetupForm select').attr('disabled', false);
                                $('#CompSetupForm textarea').attr('disabled', false);
                                $('#CompContInfoBtn').attr('disabled',false);
                                $('#CompSaveInfoBtn').attr('disabled',false);  
                    		    	if(data.status){
                				        if(returnpage == null){
                				                $('.form-group').removeClass('has-error'); // clear error class
                            					$('.help-block').empty(); // clear error string
                            				//	$('#notif-mess').html('<p style="text-align:left;font-weight:600"><i class="fa fa-check-circle" aria-hidden="true"></i> The company billing information has<br> been valified.</p>').css('color','green');
                				            //setTimeout(function(){ 
                    					       comp_setup_breadcrumb('confirmation',"back");
                				            //}, 1000);
                					    }
                				}else{
                					for (var i = 0; i < data.inputerror.length; i++){
                					    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                						$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                					
                					}
                					
                				}
                    		 }
                	    })
                }
            }else if(page == "confirmation"){
                $('#CompBasicInfoFromDiv').addClass('hidden');
                $('#CompBilInfoDiv').addClass('hidden');
                $('#CompContInfoDiv').removeClass('hidden');
                $('#notif-mess').html(' ');
                 
                $('#title_comp_setup').html('<b>Company Contract Information</b>');
                $('#comp_set_prog_billing').addClass('complete').removeClass('active');
                $('#comp_set_prog_contract').addClass('active').removeClass('complete'); 
                $('#comp_set_prog_contract_txt').css({'font-weight':'bold'}); 
                $('#comp_set_prog_billing_txt').css({'font-weight':'400'}); 
                $('#comp_set_prog_basicInfo_txt').css({'font-weight':'400'});
                
                $('#mod-footer').html(
                '<button id="CompSetupDecideNoBtn"  class="btn btn-danger pull-right" style="margin-left:5px" onclick=comp_setup_breadcrumb("comp_cont_info","back")>No, cancel</button>'+
                '<button id="CompSaveInfoBtn"  class="btn btn-success pull-right" style="margin-right:5px;margin-left:5px" onclick=comp_setup_breadcrumb("save")>Yes, submit it!</button>'
                );
                var form = $('#CompSetupForm').serialize()+"&page="+page;
                
                $('#notif-mess').html('<p style="text-align:left;font-weight:600"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Are you sure do you want submit this form?</p>').css('color','orange');
               
            }else if(page == "save"){
                $('#CompBasicInfoFromDiv').addClass('hidden');
                $('#comp_set_prog_contract').removeClass('disabled').addClass('complete');
                $('#CompBilInfoDiv').addClass('hidden');
                $('#CompContInfoDiv').addClass('hidden');
                $('#comp_set_prog_billing').addClass('complete').removeClass('active');
                $('#comp_set_prog_contract').addClass('complete').removeClass('active'); 
                $('#notif-mess').html(' ');
                 
                $('#title_comp_setup').html('<b>Company Contract Information</b>');
                $('#comp_set_prog_contract').addClass('complete'); 
                $('#comp_set_prog_contract_txt').css({'font-weight':'bold'}); $('#comp_set_prog_billing_txt').css({'font-weight':'600'}); $('#comp_set_prog_basicInfo_txt').css({'font-weight':'600'});
                
                var form = $('#CompSetupForm').serialize()+"&page="+page+"&method="+pass_method;
                
                 $.ajax({
            		 type:'POST',
            		 url:'<?php echo site_url('Account/CompSetupFormValidate'); ?>',
            		 data:form,
            		 dataType:'json',
            		 beforeSend:function(){
        		        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#CompSetupForm').css('opacity','0.1');
                        $('#CompSetupForm :input').attr('disabled', true);
                        $('#CompSaveInfoBtn').attr('disabled',true);
                        $('#CompSetupDecideNoBtn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
        	         },
            		 success:function(data){
            		    $('.loadingFORM').html('').addClass('hidden');
                        $('#CompSetupForm').css('opacity','10');
                        $('#CompSetupForm :input').attr('disabled', false);
                        $('#CompSetupForm select').attr('disabled', false);
                        $('#CompSetupForm textarea').attr('disabled', false);
                        $('#CompSaveInfoBtn').attr('disabled',false);
                        $('#CompSetupDecideNoBtn').attr('disabled',false);  
            		    
            		    reload_table_company();
            		 }
        	    })
        	    
        	    if(pass_method == "update"){
        	        var suc_mes = '<p style="text-align:center;font-weight:600;font-size:26px;color:green"><i class="fa fa-check-circle" aria-hidden="true"></i> Company Setup has been Updated!</p>';
        	    }else{
        	        var suc_mes = '<p style="text-align:center;font-weight:600;font-size:26px;color:green"><i class="fa fa-check-circle" aria-hidden="true"></i> Company Setup has been Created!</p>';
        	    }
                
                $('#require_mess').html(' ');
                $('#CompSetupForm').html(suc_mes+
                '<p style="text-align:center;font-weight:600;font-size:16px;color:black">Go to the table and check the update.</p>');
                $('#mod-footer').html('<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>');
        }
    }
    
    function comp_setup_get_data(page,id){
         $.ajax({
            type:'POST',
            url:'<?php echo base_url('account/compSetGetData'); ?>',
            data:{page:page, id:id },
            dataType:'json',
            beforeSend:function(){
                $('.loadingFORM').removeClass('hidden');
                $(".loadingFORM").fadeIn();
                $('#CompSetupForm').css('opacity','0.1');
                $('#CompSetupForm :input').attr('disabled', true);
                $('#CompBasicInfoBtn').attr('disabled',true);
                $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                    'width':'190px',
                    'height':'50px',
                    'border-radius':'5px',
                    'background-color':'white',
                    'padding':'15px',
                    'color':'black',
                }).addClass('alpha60');
            },
            success:function (data){
                $('.loadingFORM').html('').addClass('hidden');
                $('#CompSetupForm').css('opacity','10');
                $('#CompSetupForm :input').attr('disabled', false);
                $('#CompSetupForm select').attr('disabled', false);
                $('#CompSetupForm textarea').attr('disabled', false);
                $('#CompBasicInfoBtn').attr('disabled',false);
                
                    $('[name="comp_set_code"').val(data.comp_set_code);
                    $('[name="comp_set_shortname"').val(data.comp_set_shortname);
                    $('[name="comp_set_type"').val(data.comp_set_type);
                    $('[name="comp_set_company_name"').val(data.comp_set_company_name);
                    $('[name="comp_set_address"').val(data.comp_set_address);
                    $('[name="comp_set_office_number"').val(data.comp_set_office_number);
                    $('[name="comp_set_fax_number"').val(data.comp_set_fax_number);
                    $('[name="comp_set_website"').val(data.comp_set_website);
                    $('[name="comp_set_sec_number"').val(data.comp_set_sec_number);
                    $('[name="comp_set_tax_id_or_tin_no"').val(data.comp_set_tax_id_or_tin_no);
                    $('[name="comp_set_dti_no"').val(data.comp_set_dti_no);
                    $('[name="comp_set_payee_code"').val(data.comp_set_payee_code);

                    $('[name="comp_bil_billing_setup"').val(data.comp_bil_billing_setup);
                    $('[name="comp_bil_send_billing_setup"').val(data.comp_bil_send_billing_setup);
                    $('[name="comp_bil_send_billing_to"').val(data.comp_bil_send_billing_to);
                    $('[name="comp_bil_vat_type"').val(data.comp_bil_vat_type);
                    $('[name="comp_bil_vtax_code"').val(data.comp_bil_vtax_code);
                    $('[name="comp_bil_vtax_code_rate"').val(data.comp_bil_vtax_code_rate);
                    $('[name="comp_bil_ip_admin_fee_rate"').val(data.comp_bil_ip_admin_fee_rate);
                    $('[name="comp_bil_ip_admin_fee_amount"').val(data.comp_bil_ip_admin_fee_amount);
                    $('[name="comp_bil_op_admin_fee_rate"').val(data.comp_bil_op_admin_fee_rate);
                    $('[name="comp_bil_op_admin_fee_amount"').val(data.comp_bil_op_admin_fee_amount);
                    $('[name="comp_bil_re_admin_fee_rate"').val(data.comp_bil_re_admin_fee_rate);
                    $('[name="comp_bil_re_admin_fee_amount"').val(data.comp_bil_re_admin_fee_amount);
                    $('[name="comp_bil_dc_admin_fee_rate"').val(data.comp_bil_dc_admin_fee_rate);
                    $('[name="comp_bil_dc_admin_fee_amount"').val(data.comp_bil_dc_admin_fee_amount);
                    $('[name="comp_bil_naf_rate"').val(data.comp_bil_naf_rate);
                    $('[name="comp_bil_naf_amount"').val(data.comp_bil_naf_amount);

                    $('[name="comp_contr_effectivity"').val(data.comp_contr_effectivity);
                    $('[name="comp_contr_expiration"').val(data.comp_contr_expiration);
                    $('[name="comp_contr_corporate_limit"').val(data.comp_contr_corporate_limit);
                    $('[name="comp_contr_remarks"').val(data.comp_contr_remarks);
                    $('[name="comp_setup_user_id"').val(data. comp_setup_user_id);
                    
                    $('[name="edit_id"').val(data.company_setup_id);
                    

            }
            
        });
    }
    
    function contractAction(action,id){
      	 if(action == 'view'){
    		$('#btn-header').html(''); 
    		$('#myModal').modal({backdrop: 'static', keyboard: false});
    		$('#mod-title').html("<b>Contract Details</b>");
    		$('#myModal .modal-dialog').addClass('modal-lg');
    		
    		$('#mod-content').html(
    		    '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+
    		    '<form id="CompSetupForm" class="row">'+
    			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
    			 '<input type="hidden" name="comp_setup_user_id" value="<?=$u_id_enc;?>" />'+
    			 '<input type="hidden" name="edit_id" value="" />'+
    		    '<div id="CompContInfoDiv">'+
    				    	'<div class="col-xs-6">'+
            					'<div class="form-group">'+
            						  '<label class="control-label">Effectivity <b style="color:red">*</b></label>'+
            						  '<input type="text" class="form-control" data-provide="datepicker" id="comp_contr_effectivity" name="comp_contr_effectivity"  Placeholder="Date Requested" readonly="readonly" >'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
            				    '<div class="form-group">'+
            						  '<label class="control-label">Corporate Limit </label>'+
            						  '<input type="text" name="comp_contr_corporate_limit" class="form-control form-val" maxlength="100" placeholder="Enter Corporate Limit" readonly="readonly">'+
            						  '<span class="help-block"></span>'+
            					'</div>'+
        				'</div>'+
        				 
        			    '<div class="col-xs-6">'+
        					'<div class="form-group">'+
        						  '<label class="control-label">Expiration <b style="color:red">*</b></label>'+
        						  '<input type="text" class="form-control" data-provide="datepicker" id="comp_contr_expiration" name="comp_contr_expiration"  Placeholder="Date Requested" readonly="readonly">'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        				'</div>'+
        				
        				'<div class="col-xs-12">'+
        					'<div class="form-group">'+
        						  '<label>Remarks</label>'+
                                   '<textarea class="form-control" name="comp_contr_remarks" rows="2" placeholder="Enter remarks" maxlength="500" style="resize:vertical" readonly="readonly"></textarea><small>Maximum of 500 characters</small>'+
        						  '<span class="help-block"></span>'+
        					'</div>'+
        				'</div>'+
    				'</div>'+ 
    		'</form>');
    		
    		$('#mod-footer').html('');	 
    		
    		comp_setup_get_data('comp_basic_info',id);
        	formPlugins();	 
      	 }
    }
    
    function compSetGenerateCode(){
        event.preventDefault();
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/compSetGenerateCode'); ?>',
            dataType:'json',
            beforeSend:function(){
                 $('[name="comp_set_code"]').attr('disabled',true);
                 $('[name="comp_set_code"]').val('Generating...');
            },
            success:function (code){
                $('[name="comp_set_code"]').attr('disabled',false);
                $('[name="comp_set_code"]').val(code);
            }
            
        });
    }
    
    function formPlugins(){
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
		 $("#comp_contr_effectivity").datepicker().datepicker("setDate", new Date());
		 $("#comp_contr_expiration").datepicker().datepicker("setDate", new Date(year + 1, month, day));
     
        $("input").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
    }

/*-----------------------------------------------------------------------------------*/
    
 var population_form; pass_method_bet = null; var page_pass; var review_bet = 0; 
 var comp_set_code_Arr = []; var member_type_Arr = [];

 function benefit_setup(action,method,id){
    if(action == 'view'){
        $('#btn-header').html(''); 
        $('#myModal').modal({backdrop: 'static', keyboard: false});
        $('#mod-title').html("<b>Select Mode of Setup</b>");
        $('#myModal .modal-dialog').addClass('modal-lg');

        $('#mod-content').html(
            '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+

            '<div id="wizard-setup" class="row bs-wizard hidden" style="border-bottom:0;padding-top:0;margin-top:0">'+
                '<div class="col-xs-3 bs-wizard-step active" id="step-phase1" idx="comp_set_prog_basicInfo">'+
                    '<div class="text-center bs-wizard-stepnum" id="step-phase1-txt" idx="comp_set_prog_basicInfo_txt">Type</div>'+
                    '<div class="progress disabled"><div class="progress-bar"></div></div>'+
                    '<a href="#" class="bs-wizard-dot" ></a>'+
                    '<div class="bs-wizard-info text-center"></div>'+
                '</div>'+
                '<div class="col-xs-3 bs-wizard-step disabled" id="step-phase2" idx="comp_set_prog_billing">'+
                    '<div class="text-center bs-wizard-stepnum" id="step-phase2-txt" idx="comp_set_prog_billing_txt" id="step-phase1">Limits</div>'+
                    '<div class="progress"><div class="progress-bar"></div></div>'+
                    '<a href="#" class="bs-wizard-dot"></a>'+
                    '<div class="bs-wizard-info text-center"></div>'+
                '</div>'+
                '<div class="col-xs-3 bs-wizard-step disabled" id="step-phase3" idx="comp_set_prog_contract">'+
                    '<div class="text-center bs-wizard-stepnum" id="step-phase3-txt" idx="comp_set_prog_contract_txt">Inclusions</div>'+
                    '<div class="progress"><div class="progress-bar"></div></div>'+
                    '<a href="#" class="bs-wizard-dot"></a>'+
                    '<div class="bs-wizard-info text-center"></div>'+
                '</div>'+
                '<div class="col-xs-3 bs-wizard-step disabled" id="step-phase4" idx="comp_set_prog_contract">'+
                    '<div class="text-center bs-wizard-stepnum" id="step-phase4-txt" idx="comp_set_prog_contract_txt">Modalities (if any)</div>'+
                    '<div class="progress"><div class="progress-bar"></div></div>'+
                    '<a href="#" class="bs-wizard-dot"></a>'+
                    '<div class="bs-wizard-info text-center"></div>'+
                '</div>'+
            '</div>'+
        
            '<form id="Form1">'+
                '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
                '<input type="hidden" name="comp_setup_user_id" value="<?=$u_id_enc;?>" />'+
                '<input type="hidden" name="edit_id" value="" />'+
                '<input type="hidden" name="member_limit_arr" id="member_limit_arr" value="0">'+
                '<div style="margin-top:0;margin-left:15px;margin-bottom:5px">'+
                        '<div style="padding:0;font-size:16px;font-weight:400;float:left" class="hidden"></div>'+
                        '<div id="div_info"></div>'+
                '</div>'+
                '<div style="clear:both"></div>'+

                '<div id="Form0Div" class="hidden">'+  
                    '<div class="col-xs-2"></div>'+
                    '<div class="col-xs-4" style="padding-top:20px;padding-bottom:20px;border-right:2px solid black">'+
                        '<div class="form-group">'+
                            '<label style="margin-left:20px">'+
                                '<input type="radio" name="mode_setup" class="minimal selectall pull-left" value="bnt_mtx" checked>'+
                                '<div style="font-size:16px;float:right;margin-left:10px">Set up a benefit matrix</div>'+
                            '</label>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-xs-4" style="padding-top:20px;padding-bottom:20px">'+
                        '<div class="form-group">'+
                            '<center><label>'+
                                '<input type="radio" name="mode_setup" class="minimal selectall pull-left" value="stp_blk">'+
                                '<div style="font-size:16px;float:right;margin-left:10px">Set up by bulk</div>'+
                            '</label></center>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-xs-2"></div>'+
                '</div>'+

                '<div id="Form1Div-2" class="hidden">'+  
                    '<div class="col-xs-12">'+
                            '<div class="form-group">'+
                                '<label for="exampleInputFile">Create by bulk (xlsx, xls)</label>'+
                                '<input type="file" id="exampleInputFile">'+
                                '<p class="help-block">Download Template here:</p>'+
                            '</div>'+
                    '</div>'+
                '</div>'+ 

                '<div id="Form1Div" class="hidden">'+
                    '<div class="col-xs-12">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Search and select by Account Name | Code  | Effectivity Date <b style="color:red">*</b></label>'+ 
                            '<select name="comp_set_code"  id="comp_set_code" class="form-control form-val select2not" ></select>'+
                            '<span class="help-block"></span>'+
                        '</div>'+
                    '</div>'+    
                    '<div class="col-xs-12">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Member Type <b style="color:red">*</b></label><button class="btn btn-xs btn-primary pull-right hidden" style="font-size:12px;margin:4px" onclick=member_list("view") id="viewMemberBtn">View Members</button>'+
                            '<select name="member_type" id="member_type" class="form-control form-val select2not" ><option></option></select>'+
                            '<span class="help-block"></span>'+
                        '</div>'+
                    '</div>'+    
                    // '<div class="col-xs-6">'+
                    //     '<div class="form-group">'+
                    //         '<label class="control-label">Age Limit <b style="color:red">*</b></label>'+
                    //         '<input type="text" name="age_limit" id="age_limit" class="form-control form-val" maxlength="5" placeholder="Age Limit">'+
                    //         '<span class="help-block"></span>'+
                    //     '</div>'+
                    // '</div>'+
                '</div>'+ 
            
                '<div id="Form2Div" class="hidden"></div>'+ 
            
                '<div id="Form3Div" class="hidden"></div>'+ 

                '<div id="Form4Div" class="hidden"></div>'+
            
                '<div id="Form5Div" class="hidden"></div>'+
            '</form>'+
            '<div style="clear:both"><div>'+
            '<div id="require_mess" class="hidden" style="margin-left:15px;padding-top:15px;font-weight:600">Note: Fields with <b style="color:red">*</b> are mandatory.<div>');

            $('#mod-footer').html('<button id="Form0Btn"  class="btn btn-success" onclick=benefit_setup_breadcrumb("phase0",'+null+',"'+method+'") >Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');	 
    		pass_method_bet	= method;
    		if(method == "update"){
    		    benefit_setup_get_data('phase1',id);
    		}	 
            $('#Form0Div').removeClass('hidden');
            formPlugins2("phase0"); 
            formPlugins2("phase1"); 

        }
 }

 function benefit_setup_breadcrumb(page,returnpage=null,method){
    if(page == "phase0"){
        page_pass = page;
        $('#Form0Div').removeClass('hidden');
        $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
        $('#Form2Div').addClass('hidden');
        $('#Form3Div').addClass('hidden');
        $('#Form4Div').addClass('hidden');

        $('#mod-title').html("<b>Select Mode of Setup</b>");
        $('#wizard-setup').addClass('hidden');
        $('#require_mess').addClass('hidden');
        $('#div_info').html('');
        $('#Form2Div').html('');
        $('#notif-mess').html(' ');
        formPlugins2("phase0");

        $('#mod-footer').html('<button id="Form0Btn"  class="btn btn-success" onclick=benefit_setup_breadcrumb("phase0",'+null+',"'+method+'") >Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        if(returnpage == null){
            if($('[name="mode_setup"]:checked').val() == "bnt_mtx"){
                benefit_setup_breadcrumb("phase1","back",pass_method_bet);
            }else{
                benefit_setup_breadcrumb("phase1-2","back",pass_method_bet);
            }
        }
        
    }else if(page == "phase1-2"){
            page_pass = page;
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').removeClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').addClass('hidden');

            $('#mod-title').html("<b>Benefit Setup - Upload Bulk</b>");
            $('#wizard-setup').addClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#div_info').html('');
            $('#Form2Div').html('');
            $('#notif-mess').html(' ');
            formPlugins2("phase1-2"); 

            $('#mod-footer').html('<button id="Form1Btn-2" class="btn btn-success" onclick=benefit_setup_breadcrumb("phase1-2") >Upload</button>'+
    		'<button id="Form1Btn-2-modsetup" class="btn btn-default" onclick=benefit_setup_breadcrumb("phase0","back","'+method+'") >Mode of Setup</button>');
            
    }else if(page == "phase1"){
            page_pass = page;
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').removeClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').addClass('hidden');

            $('#mod-title').html("<b>Benefit Setup</b>");
            $('#wizard-setup').removeClass('hidden');
            $('#require_mess').removeClass('hidden');
            $('#notif-mess').html(' ');
        
            
            if(returnpage == "back"){
                population_form = $('#Form1').serialize();

            $('#mod-footer').html(
            '<button id="Form1Btn-2-modsetup" class="btn btn-default" onclick=benefit_setup_breadcrumb("phase0","back","'+method+'") >Mode of Setup</button>'+
            '<button id="Form1Btn" class="btn btn-success" onclick=benefit_setup_breadcrumb("phase1") >Limits &emsp;<i class="fa fa-chevron-right"></i></button>');    
            $('#Form1Btn').attr('disabled',true);  $('Form1Btn-2-modsetup').attr('disabled',true);

            setTimeout(function(){ 
            $('#step-phase1').removeClass('complete').addClass('active');
            $('#Form1Btn').attr('disabled',false);  $('Form1Btn-2-modsetup').attr('disabled',false);
            }, 490);
            }
            $('#step-phase2').removeClass('active').removeClass('complete').addClass('disabled');
            $('#step-phase3').removeClass('active').removeClass('complete').addClass('disabled');
            $('#step-phase4').removeClass('active').removeClass('complete').addClass('disabled');

                // Animation txt bold
            $('#step-phase1-txt').css({'font-weight':'bold'}); 
            $('#step-phase2-txt').css({'font-weight':'normal'}); 
            $('#step-phase3-txt').css({'font-weight':'normal'});
            $('#step-phase4-txt').css({'font-weight':'normal'});
            
                
            if(returnpage == null){
                
                comp_set_code_Arr = [];
                member_type_Arr = [];
                var valueArrMem = [];
            
                var data_val_acc_NAMEID = $("#comp_set_code").val() == null ? "" : $("#comp_set_code").val().toString();
                var data_acc_NAMEID = {
                    comp_set_code: data_val_acc_NAMEID
                };

                var data_val_acc_MEM = $("#member_type").val() == null ? "" : $("#member_type").val().toString();
                var data_acc_MEM = {
                    member_type: data_val_acc_MEM
                };

                var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_bet+ "&" + $.param(data_acc_NAMEID)+ "&" + $.param(data_acc_MEM);
                
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                    data:form,
                    dataType:'json',
                    beforeSend:function(){
                        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#Form1').css('opacity','0.1');
                        $('#Form1 :input').attr('disabled', true);
                        $('#Form1Btn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
                    },
                    success:function(data){
                        $('.loadingFORM').html('').addClass('hidden');
                        $('#Form1').css('opacity','10');
                        $('#Form1 :input').attr('disabled', false);
                        $('#Form1 select').attr('disabled', false);
                        $('#Form1 textarea').attr('disabled', false);
                        $('#Form1Btn').attr('disabled',false);
                    
                        if(data.status){
                                if(returnpage == null){
                                    $('.form-group').removeClass('has-error'); // clear error class
                                    $('.select2not').removeClass('input-validation-error');
                                    $('.help-block').empty();
                                   

                                    $("#comp_set_code option:selected").each(function(){
                                        comp_set_code_Arr.push("<span style='background-color:#42a5f5;margin:1px 2px;padding:1px 4px;color:black;border-radius:5px;display:inline-block;'>"+$(this).val()+"</span>");
                                    })   
                                    $("#member_type option:selected").each(function(i_elem, obj){
                                        i_elem = i_elem + 1;
                                        member_type_Arr.push("<span class='mbrClass btn btn-xs' style='cursor:pointer;background-color:"+(i_elem==1 ? '#a5d6a7' : "#bdbdbd")+";margin:1px 2px;padding:1px 4px;color:black;border-radius:5px;display:inline-block;box-shadow:2px 2px 2px #888888' data-elem_id='"+i_elem+"' data-id='"+$(this).data('value')+"' data-txt='"+$(this).val()+"' data-num='"+i_elem+"'>"+i_elem+'.) '+$(this).val()+"</span>");
                                        valueArrMem.push($(this).data('value'));
                                    })  
                                    $('#member_limit_arr').val(valueArrMem);
                                    benefit_setup_breadcrumb('phase2',"back");
                               
                                }
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                                    $('.select2not[name="'+data.inputerror[i]+'"]').addClass('input-validation-error');
                                    
                                if(data.inputerror[i] == "comp_set_code" || data.inputerror[i] == "member_type"){
                                $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]); 
                                }else{
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                                }
                            }
                            
                        }
                    }
                })
            }
           
    }else if(page == "phase2"){
        page_pass = page;
        // Show Form per phase
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').removeClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').addClass('hidden');
            $('#div_info').removeClass('hidden');
            // Clear success and error message
            $('#mod-title').html("<b>Benefit Setup</b>");
            $('#wizard-setup').removeClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#notif-mess').html(' ');
            // Animation txt bold
            $('#step-phase1-txt').css({'font-weight':'normal'});  
            $('#step-phase2-txt').css({'font-weight':'bold'}); 
            $('#step-phase3-txt').css({'font-weight':'normal'});
            $('#step-phase4-txt').css({'font-weight':'normal'});
            var nameLlb =(comp_set_code_Arr.length == 1) ? 'Company' : 'Companies';
            var memLlb =(member_type_Arr.length == 1) ? 'Member Type' : 'Member Types';
         
            $('#div_info').html(
                '<div style="float:left;font-size:16px"><b>'+nameLlb+':</b></div>'+
                '<div style="float:left;font-size:12px">'+comp_set_code_Arr.join(" ")+'</div><div style="clear:both"></div>'+
                '<div style="font-size:14px"><b>'+memLlb+':</div><div style="font-size:12px"> </b> '+member_type_Arr.join(" ")+'</div>'
                ).css({
                'float':'left',
                'font-weight':'400',
                'margin-top':'0',
                'margin-bottom':'10px',
                'display': 'inline-block',
                'word-wrap': 'break-word',
            });

            // Change button per phase
            $('#mod-footer').html(
                '<button id="Form3Btn" class="btn btn-success pull-right"  onclick=benefit_setup_breadcrumb("phase2") style="margin-left:5px">Inclusions &emsp;<i class="fa fa-chevron-right"></i></button>'+
                '<button id="Form1Btn" class="btn btn-info pull-right" onclick=benefit_setup_breadcrumb("phase1","back")><i class="fa fa-chevron-left"></i>&emsp; Type</button>'
            );
            $('#Form3Btn').attr('disabled',true);  $('#Form1Btn').attr('disabled',true);
            if(returnpage == "back"){
                $('#step-phase3').removeClass('active').removeClass('complete').addClass('disabled');
                setTimeout(function(){ 
                $('#step-phase2').removeClass('complete').addClass('active');
                    setTimeout(function(){ 
                        $('#step-phase2').addClass('disabled');
                        $('#Form3Btn').attr('disabled',false);  $('#Form1Btn').attr('disabled',false);
                    }, 490);
                }, 490);
                population_form = $('#Form1').serialize();
                limitsForm();
            }
            // Animation
            $('#step-phase1').removeClass('disabled').addClass('complete').removeClass('active');  

           
            if(returnpage == null){
                    var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_bet;
                    population_form = form;
                    $('.form-group').removeClass('has-error'); 
                    $('.help-block').empty();
                    $('.errorLimit').remove();
                    $.ajax({
                        type:'POST',
                        url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                        data:form,
                        dataType:'json',
                        beforeSend:function(){
                            $('.loadingFORM').removeClass('hidden');
                            $(".loadingFORM").fadeIn();
                            $('#Form1').css('opacity','0.1');
                            $('#Form1 :input').attr('disabled', true);
                            $('#Form1Btn').attr('disabled',true);
                            $('#Form3Btn').attr('disabled',true);
                            $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                                'width':'190px',
                                'height':'50px',
                                'border-radius':'5px',
                                'background-color':'white',
                                'padding':'15px',
                                'color':'black',
                            }).addClass('alpha60');
                        },
                        success:function(data){
                            $('.loadingFORM').html('').addClass('hidden');
                            $('#Form1').css('opacity','10');
                            $('#Form1 :input').attr('disabled', false);
                            $('#Form1 select').attr('disabled', false);
                            $('#Form1 textarea').attr('disabled', false);
                            $('#Form1Btn').attr('disabled',false);
                            $('#Form3Btn').attr('disabled',false);

                           
                            if(data.status){
                                    if(returnpage == null){
                                        $('.form-group').removeClass('has-error'); 
                                        $('.help-block').empty();
                                    
                                        benefit_setup_breadcrumb('phase3',"back");
                                    }
                            }else{
                                for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                    //var lastChar = data.inputerror[i][data.inputerror[i] -1]; 
                                    var lastChar = data.inputerror[i].substr(data.inputerror[i].length - 1);
                                    $('.mbrClass[data-id="'+lastChar+'"]').after('<span class="errorLimit" data-id="'+lastChar+'">&nbsp;<i class="fa fa-exclamation-triangle" style="color:red"></i></span>');
                                }
                            }
                        }
                    })
            }
            
    }else if(page == "phase3"){
            page_pass = page;
            // Show Form per phase
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').removeClass('hidden');
            $('#Form4Div').addClass('hidden');
            $('#div_info').removeClass('hidden');
            // Clear success and error message
            $('#mod-title').html("<b>Inclusions</b>");
            $('#wizard-setup').removeClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#notif-mess').html(' ');

            // Change button per phase
            $('#mod-footer').html(
            '<button id="Form4Btn" class="btn btn-success pull-right"  onclick=benefit_setup_breadcrumb("phase3") style="margin-left:5px">Modalities &emsp;<i class="fa fa-chevron-right"></i></button>'+
            '<button id="Form2Btn" class="btn btn-info pull-right" onclick=benefit_setup_breadcrumb("phase2","back")><i class="fa fa-chevron-left"></i>&emsp; Limits</button>');
            $('#Form4Btn').attr('disabled',true);  $('#Form2Btn').attr('disabled',true);
            // Animation
            $('#step-phase2').removeClass('disabled').removeClass('active').addClass('complete')
            if(returnpage == "back"){
                setTimeout(function(){ 
                    $('#step-phase4').removeClass('active').removeClass('complete');
                    setTimeout(function(){ 
                        $('#step-phase3').addClass('active').removeClass('complete');
                        setTimeout(function(){ 
                            $('#step-phase3').addClass('disabled');
                            $('#Form4Btn').attr('disabled',false); $('#Form2Btn').attr('disabled',false);
                        }, 490);   
                    }, 490);
                }, 490);
                population_form = $('#Form1').serialize();
                limitsForm();
            }
             // Animation txt bold
            $('#step-phase2-txt').css({'font-weight':'normal'}); 
            $('#step-phase1-txt').css({'font-weight':'normal'});  
            $('#step-phase3-txt').css({'font-weight':'bold'});
            $('#step-phase4-txt').css({'font-weight':'normal'});

            $.each(population_form.split('&'), function (index, elem) {
                var vals = elem.split('=');
                var datarmove = $("#Form2Div [name='" + vals[0]+"']").attr('name');
                if(datarmove != undefined){
                    var datarmove_first = datarmove.slice(0, -2);
                    var datarmove_last = datarmove.slice(-2);
                    console.log(datarmove_last);    
                    if(datarmove_first != "member_opd"){
                        console.log(datarmove_first);
                       // $('[name'+datarmove_first+datarmove_last).
                    }
                    
              
                }
                // if($("#Form2Div [name='" + vals[0]+"']").val() == ""){
                //     $("#Form2Div [name='" + vals[0]+"']").addClass('hidden');
                // }
            });

            if(returnpage == null){
                    var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_bet;
                    population_form = form;
                    $('.form-group').removeClass('has-error'); 
                    $('.help-block').empty();
                    $('.errorLimit').remove();
                    $.ajax({
                        type:'POST',
                        url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                        data:form,
                        dataType:'json',
                        beforeSend:function(){
                            $('.loadingFORM').removeClass('hidden');
                            $(".loadingFORM").fadeIn();
                            $('#Form1').css('opacity','0.1');
                            $('#Form1 :input').attr('disabled', true);
                            $('#Form4Btn').attr('disabled',true);
                            $('#Form2Btn').attr('disabled',true);
                            $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                                'width':'190px',
                                'height':'50px',
                                'border-radius':'5px',
                                'background-color':'white',
                                'padding':'15px',
                                'color':'black',
                            }).addClass('alpha60');
                        },
                        success:function(data){
                            $('.loadingFORM').html('').addClass('hidden');
                            $('#Form1').css('opacity','10');
                            $('#Form1 :input').attr('disabled', false);
                            $('#Form1 select').attr('disabled', false);
                            $('#Form1 textarea').attr('disabled', false);
                            $('#Form4Btn').attr('disabled',false);
                            $('#Form2Btn').attr('disabled',true);
                        
                            if(data.status){
                                    if(returnpage == null){
                                        $('.form-group').removeClass('has-error'); 
                                        $('.help-block').empty();
                                    
                                        benefit_setup_breadcrumb('phase4',"back");
                                    }
                            }else{
                                for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                 }
                            }
                        }
                    })
            }
            
    }else if(page == "phase4"){
        page_pass = page;
        $('#Form0Div').addClass('hidden');
        $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
        $('#Form2Div').addClass('hidden');
        $('#Form3Div').addClass('hidden');
        $('#Form4Div').removeClass('hidden');
        $('#div_info').removeClass('hidden');
        // Clear success and error message
        $('#mod-title').html("<b>Modalities (If Any)</b>");
        $('#wizard-setup').removeClass('hidden');
        $('#require_mess').addClass('hidden');
        $('#notif-mess').html(' ');

        $('#mod-footer').html(
        '<button id="Form5Btn" class="btn btn-success pull-right"  onclick=benefit_setup_breadcrumb("phase5") style="margin-left:5px">Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
        '<button id="Form3Btn" class="btn btn-info pull-right" onclick=benefit_setup_breadcrumb("phase3","back")><i class="fa fa-chevron-left"></i>&emsp; Inclusions</button>');
        $('#Form5Btn').attr('disabled',true);  $('#Form3Btn').attr('disabled',true);

        // Animation
        $('#step-phase3').removeClass('disabled').removeClass('active').addClass('complete');
        if(returnpage == "back"){
        setTimeout(function(){ 
            $('#step-phase3').removeClass('active').addClass('complete');
            setTimeout(function(){ 
                $('#step-phase4').addClass('active');
                $('#Form5Btn').attr('disabled',false);  $('#Form3Btn').attr('disabled',false);
            }, 450);
        }, 490);
       
            $('#step-phase4').addClass('disabled');
            population_form = $('#Form1').serialize();
           
        }
        limitsForm();
            // Animation txt bold
        $('#step-phase2-txt').css({'font-weight':'normal'}); 
        $('#step-phase1-txt').css({'font-weight':'normal'});  
        $('#step-phase3-txt').css({'font-weight':'normal'});
        $('#step-phase4-txt').css({'font-weight':'bold'});
        
        if(returnpage == null){
                var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_bet;
                population_form = form;
                $('.form-group').removeClass('has-error'); 
                $('.help-block').empty();
                $('.errorLimit').remove();
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                    data:form,
                    dataType:'json',
                    beforeSend:function(){
                        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#Form1').css('opacity','0.1');
                        $('#Form1 :input').attr('disabled', true);
                        $('#Form5Btn').attr('disabled',true);
                        $('#Form3Btn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
                    },
                    success:function(data){
                        $('.loadingFORM').html('').addClass('hidden');
                        $('#Form1').css('opacity','10');
                        $('#Form1 :input').attr('disabled', false);
                        $('#Form1 select').attr('disabled', false);
                        $('#Form1 textarea').attr('disabled', false);
                        $('#Form5Btn').attr('disabled',false);
                        $('#Form3Btn').attr('disabled',false);
                    
                        if(data.status){
                                if(returnpage == null){
                                    $('.form-group').removeClass('has-error'); 
                                    $('.help-block').empty();
                                
                                    benefit_setup_breadcrumb('phase5',"back");
                                }
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                }
                        }
                    }
                })
        }
        
    }else if(page == "phase5"){
            //$('#step-phase4').removeClass('disabled').removeClass('active').addClass('complete');
            var review_bet_status = (review_bet == 0) ? 'hidden' : '';
            var review_bet_status2 = (review_bet == 1) ? 'hidden' : '';
            page_pass = page;
            $('#mod-footer').html(
            '<button id="FormDecideCancelBtn" class="btn btn-default pull-right" style="margin-left:5px" onclick=benefit_setup_breadcrumb("phase4","back")>Cancel</button>'+
            '<button id="FormDecideNoBtn"  class="btn btn-success pull-right '+review_bet_status+'" style="margin-left:5px" onclick=benefit_setup_breadcrumb("save")>Save</button>'+
            '<button id="FormDecideYesBtn"  class="btn btn-warning pull-right '+review_bet_status2+'" style="margin-right:5px;margin-left:5px" onclick=benefit_setup_breadcrumb("phase1","back")>Take a review</button>'
            );
            var form = $('#CompSetupForm').serialize()+"&page="+page;
            
            $('#notif-mess').addClass('hidden').html('<p style="text-align:left;font-weight:600"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Are you sure do you want take a review this benefit form?</p>').css('color','orange');
    }else if(page == "save"){
        page_pass = page;
        //$('#step-phase4').removeClass('disabled').removeClass('complete').addClass('active');
        $('#notif-mess').html('');
        setTimeout(function(){ 
            $('#step-phase4').removeClass('disabled');
           
        }, 450);

        $('#step-phase2-txt').css({'font-weight':'normal'}); 
        $('#step-phase1-txt').css({'font-weight':'normal'});  
        $('#step-phase3-txt').css({'font-weight':'normal'});
        $('#step-phase4-txt').css({'font-weight':'normal'});

        // Member Select 2
        var member_arr = [];
        $("#member_type option:selected").each(function(){
            member_arr.push($(this).data('value'));
        });

        var member_data = {
            member_id_arr: member_arr
        };

        // Company Select 2
        var company_arr = [];
        $("#comp_set_code option:selected").each(function(){
            company_arr.push($(this).data('value'));
        })  

        var company_data = {
            company_id_arr: company_arr
        };

        var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method_bet+ "&" + $.param(member_data) + "&" + $.param(company_data);
        population_form = form;
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty();
        $('.errorLimit').remove();
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
            data:form,
            dataType:'json',
            beforeSend:function(){
                $('.loadingFORM').removeClass('hidden');
                $(".loadingFORM").fadeIn();
                $('#Form1').css('opacity','0.1');
                $('#Form1 :input').attr('disabled', true);
                $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                    'width':'190px',
                    'height':'50px',
                    'border-radius':'5px',
                    'background-color':'white',
                    'padding':'15px',
                    'color':'black',
                }).addClass('alpha60');
            },
            success:function(data){
                console.log(data);
                $('.loadingFORM').html('').addClass('hidden');
                $('#Form1').css('opacity','10');
                $('#Form1 :input').attr('disabled', false);
                $('#Form1 select').attr('disabled', false);
                $('#Form1 textarea').attr('disabled', false);
                reload_table_company();
                
                if(data.status){
                        if(returnpage == null){
                            $('.form-group').removeClass('has-error'); 
                            $('.help-block').empty();
                        }
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
            }
        });
        

        if(pass_method_bet == "update"){
            var suc_mes = '<p style="text-align:center;font-weight:600;font-size:26px;color:green"><i class="fa fa-check-circle" aria-hidden="true"></i> Benefit Setup has been Updated!</p>';
        }else{
            var suc_mes = '<p style="text-align:center;font-weight:600;font-size:26px;color:green"><i class="fa fa-check-circle" aria-hidden="true"></i> Benefit Setup has been Created!</p>';
        }
        $('#mod-footer').html('<button type="button" id="FormDecideCloseBtn" class="btn btn-default pull-right" data-dismiss="modal">Close</button>');
        $('#require_mess').html(' ');
        $('#Form1').html(suc_mes+
        '<p style="text-align:center;font-weight:600;font-size:16px;color:black">Go to the table and check the update.</p>');
       
    }
}

$(document).on('click','#FormDecideYesBtn',function(){
    review_bet = 1;
})

function term(str, char) {
  var xStr = str.substring(0, str.length - 1);
  return xStr + char;
}

$(document).on('click','.mbrClass',function(){
    var elem_id = $(this).data('elem_id');
    var id = $(this).data('id');
    $('.limitFormDivCls2').addClass('hidden');
    $('.limitFormDivCls3').addClass('hidden');
    $('.limitFormDivCls4').addClass('hidden');
    $('.mbrClass').css({'background-color':'#bdbdbd'});

    console.log(page_pass);
    if(page_pass == "phase2"){
        $('#Form2Div').removeClass('hidden');
        $('.limitFormDivCls2[data-elem_id="'+id+'"]').removeClass('hidden');
        $(this).css({'background-color':'#a5d6a7'});
     
    }else if(page_pass == "phase3"){
        $('#Form3Div').removeClass('hidden');
        $('.limitFormDivCls3[data-elem_id="'+id+'"]').removeClass('hidden');
        $(this).css({'background-color':'#a5d6a7'});
    }else if(page_pass == "phase4"){
        $('#Form4Div').removeClass('hidden');
        $('.limitFormDivCls4[data-elem_id="'+id+'"]').removeClass('hidden');
        $(this).css({'background-color':'#a5d6a7'});
    }else{
    }
    $(this).css({'background-color':'#a5d6a7'});
});

function limitsForm(){
    $('#Form2Div').empty();
    $('#Form3Div').empty();
    $('#Form4Div').empty();
    var mbrClassLength =  $('.mbrClass').length;

    $('.mbrClass').css({'background-color':'#bdbdbd'});
    $('.mbrClass').first().css({'background-color':'#a5d6a7'});
    $('.mbrClass').first().css({'background-color':'#a5d6a7'});
    $('.mbrClass').first().css({'background-color':'#a5d6a7'});
    
    for(var i_elem = 1;i_elem <= mbrClassLength;i_elem++){
        var id = $('.mbrClass[data-elem_id="'+i_elem+'"]').data('id');
        $('#Form2Div').append(
            appendForm(i_elem,id)
        );

        $.each(population_form.split('&'), function (index, elem) {
        var vals = elem.split('=');
            $("#Form2Div [name='" + vals[0]+"']").val(vals[1]);
            $("#Form3Div [name='" + vals[0]+"']").val(vals[1]);
            $("#Form4Div [name='" + vals[0]+"']").val(vals[1]);
        });
    }

        $('.limitFormDivCls2').first().removeClass('hidden');
        $('.limitFormDivCls3').first().removeClass('hidden');
        $('.limitFormDivCls4').first().removeClass('hidden');
        $("input").keyup(function(){
            var lastChar = $(this).attr('name').substr($(this).attr('name').length - 1);
            $('.errorLimit[data-id="'+lastChar+'"]').remove();
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        
}

function appendForm(i_elem,id){
    $('#Form2Div').append(
        '<div style="clear:both"></div>'+
        '<div class="limitFormDivCls2 hidden" data-elem_id="'+id+'" data-id="'+id+'">'+
        '<div class="col-xs-6">'+
                '<div class="form-group">'+
                    '<label class="control-label">Annual Benefit Limit </label>'+
                    '<input type="text" name="member_abl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter Annual Benefit Limit">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">MXBL </label>'+
                    '<input type="text" name="member_mxbl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter MXBL">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">LBL </label>'+
                    '<input type="text" name="member_lbl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter LBL">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">PEL </label>'+
                    '<input type="text" name="member_pel_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter PEL">'+
                    '<span class="help-block"></span>'+
                '</div>'+
        '</div>'+
        '<div class="col-xs-6">'+
                '<div class="form-group">'+
                    '<label class="control-label">CIL </label>'+
                    '<input type="text" name="member_cil_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter CIL">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">MBL </label>'+
                    '<input type="text" name="member_mbl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter MBL">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">OPD </label>'+
                    '<input type="text" name="member_opd_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter OPD">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">Age Limit </label>'+
                    '<input type="text" name="member_age_limit_'+id+'"" class="form-control form-val" maxlength="5" placeholder="Age Limit">'+
                    '<span class="help-block"></span>'+
                '</div>'+
        '</div>'+
        '<div>'
    );
    $('#Form3Div').append(
        '<div class="limitFormDivCls3 hidden" data-elem_id="'+id+'" data-id="'+id+'">'+
        '<div class="col-xs-6">'+
            '<div class="form-group">'+
                '<label class="control-label">ABL </label>'+
                '<input type="text" name="abl_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter ABL">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">MXBL </label>'+
                '<input type="text" name="mxbl_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter MXBL">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">LBL </label>'+
                '<input type="text" name="lbl_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter LBL">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">PEL </label>'+
                '<input type="text" name="pel_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter PEL">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">CIL </label>'+
                '<input type="text" name="cil_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter CIL">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">MBL </label>'+
                '<input type="text" name="mbl_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter MBL">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">OPD </label>'+
                '<input type="text" name="opd_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter OPD">'+
                '<span class="help-block"></span>'+
            '</div>'+
        '</div>'+
        '<div class="col-xs-6">'+
            '<div class="form-group">'+
                '<label class="control-label">Room and board </label>'+
                '<input type="text" name="room_and_board_inclu_'+id+'"  class="form-control form-val" maxlength="5" placeholder="Enter Room and board">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">Dental </label>'+
                '<input type="text" name="dental_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter Dental">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">APE </label>'+
                '<input type="text" name="ape_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter APE">'+
                '<span class="help-block"></span>'+
            '</div>'+
            '<div class="form-group">'+
                '<label class="control-label">ECU </label>'+
                '<input type="text" name="ecu_inclu_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter ECU">'+
                '<span class="help-block"></span>'+
            '</div>'+
        '</div>'+
        '</div>'
    );
    $('#Form4Div').append(
        '<div class="limitFormDivCls4 hidden" data-elem_id="'+id+'" data-id="'+id+'">'+
            '<div class="col-xs-6">'+
                '<div class="form-group">'+
                    '<label class="control-label">Modality </label>'+
                    '<input type="text" name="modality_mod_'+id+'"  class="form-control form-val" maxlength="5" placeholder="Enter Modality">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">Amount </label><button class="btn btn-xs btn-primary pull-right" style="font-size:12px;margin:4px" onclick=add_modalities("view") id="addModalities">Add</button>'+
                    '<input type="text" name="amount_mod_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter Amount">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label class="control-label">Remarks </label>'+
                    '<input type="text" name="remark_mod_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter Remarks">'+
                    '<span class="help-block"></span>'+
                '</div>'+
            '</div>'+
            '<div class="col-xs-6">'+
                '<div class="form-group">'+
                        '<br>'+
                '</div>'+
            '</div>'+
        '</div>'
    );
}

 function formPlugins2(page){
            $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	    });
            $('#comp_set_code').select2({
                    width:"100%",
                    placeholder:'You can search by Account Name or Code',
                    allowClear:true,
                    multiple:true,
                    
            });
            comp_set_code_list();

            $("#comp_contr_effectivity").datepicker().datepicker("setDate", new Date());

            $('[name="member_type"]').select2({
                    width:"100%",
                    placeholder:'Member Type',
                    allowClear:true,
                    multiple:true,
                    /*sorter: function(data) {
                     
                        return data.sort(function (a, b) {
                            a = a.text.toLowerCase();
                            b = b.text.toLowerCase();
                            if (a > b) {
                                return 1;
                            } else if (a < b) {
                                return -1;
                            }
                            return 0;
                        });
            }*/
            });
            member_type_list();
        //}

        $("input").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").not('.select2not').change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $(".select2not").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().next().empty();
        });
    }

    function formPlugins3(page){
        $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	});
        var bday = moment().subtract(9, 'years').toDate();
        $("#hired_date_enroll_mem").datepicker().datepicker("setDate", new Date());
        $("#birth_date_enroll_mem").datepicker().datepicker("setDate", bday);
        
        $("input").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").not('.select2not').change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        // $(".select2not").change(function(){
        //     $(this).parent().removeClass('has-error');
        //     $(this).next().next().empty();
        // });

        $("#birth_date_enroll_mem").on('change',function(){
          $('#age_enroll_mem').val(getAge(moment($(this).val(),["MM-DD-YYYY"]).format("YYYY/MM/DD")));
        })

        $("#age_enroll_mem").on('change',function(){
            $("#birth_date_enroll_mem").datepicker().datepicker("setDate", getBirthdate($(this).val()));
        })
    }

    function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    function getBirthdate(age) {
       var bday = moment().subtract(age, 'years').toDate();
        return bday;
    }

    function comp_set_code_list(){
        $('#comp_set_code').empty();
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/comp_set_code_list'); ?>',
            dataType:'json',
            beforeSend:function(){
                $('#comp_set_code').attr("placeholder", "Loading Data...");
            },
            success:function (data){
                $('#comp_set_code').append(data);
            }
        });
    }

    function member_type_list(){
        $('[name="member_type"]').empty();
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/member_type_list'); ?>',
            dataType:'json',
            beforeSend:function(){
                $('[name="member_type"]').attr("placeholder", "Loading Data...");
            },
            success:function (data){
                $('[name="member_type"]').append(data);
            }
            
        });
    }

</script>

</body>
</html>
