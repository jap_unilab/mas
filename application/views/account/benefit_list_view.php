<!DOCTYPE html>
<html>
    
<?php 
$arr['db'] = isset($db->page_header) ? $db->page_header : array();
$this->load->view('include/header',$arr); 
?>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <?php 
  $arr['db'] = isset($db->main_header) ? $db->main_header : array();
  $this->load->view('include/main_header',$arr); 
  ?> 


  <!-- Full Width Column -->
  <div class="content-wrapper">
  <?php 
  $arr['db'] = isset($db->main_sub_header) ? $db->main_sub_header : array();
  $this->load->view('include/main_sub_header',$arr); 
  ?>
 <?php 
      $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
      );
  ?>
  <?php 
    $u_id_enc = $db->main_header['user_info']->id;
    $gen_code = $db->page_body['generate_code'];
  ?>
  <div class="">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="" >
        <h3 style="margin:0;padding:0;font-size:20px;font-weight:600;margin-left:-25px" class="pull-left">
        <ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0;">
          <li><a href="#">&emsp; Home</a></li>
          <li><a href="#">Benefit Setup</a></li>
        </ol>  
        </h3>

        <div class="pull-right" style="margin-top:-5px;margin-bottom:10px">
		    <a class="btn btn-md btn-primary pull-left" style="margin-right:10px;cursor: pointer;" onClick="benefit_setup('view','add')"> Setup Benefit</a>
        </div>
      </section>

      <!-- Main content -->
      <section class="content" >
       

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
                <div class="box-header">
                    <div class="col-xs-6" style="margin:0;padding:0">
                      <h3 class="box-title" style="margin-top:4px;margin-left:4px;margin-bottom:2px">Benefit Plan</h3>
                    </div>
                    <div class="col-xs-5 pull-right" style="margin:0;padding:0">
                        <div class="input-group" >
                            <input type="text" id="searchEvntMtr" class="form-control" placeholder="Enter any keyword to search." onkeyup="searchDefaultByEnter()">
                              <span class="input-group-btn">
                              <button type="submit" id="searchEvntMtrBtn" onclick="searchDefault()" class="btn btn-flat" ><i class="fa fa-search"></i>
                              </button>
                              </span>
                        </div>
                        <!--onClick="showAdvanceSearch()"-->
                        <div class="pull-right" id="searchDiv"><a href="javascript:void(0)" >Show Advance Search</a></div>
                    </div>
                </div>
                <!-- /.box-header -->
           
                <div id="tablediv" class="box-body table-responsive" style="margin-top:0;padding-top:0">
                <!--AJAX TABLE LOADING-->
                </div><br><br><br>
                <!-- /.box-body -->
                
                
          </div>
          <!-- /.box -->
          </div>
          </div>


      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
    
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="">
      <div class="pull-right hidden-xs">
        <b>Beta Version:</b> 1.0.0 | 12.17.2018
      </div>
      <strong>Omniquest.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>&nbsp;
			<div class="pull-right" id="btn-header"></div>&nbsp; 
			
			<h4 class="modal-title pull-left" id='mod-title'></h4>
		  </div>
		  <div class="modal-body">
			<div id='mod-content'>
			</div>
		  </div>
		  <div class="modal-footer">
			<span class="pull-left" id="notif-mess"></span>
			<div id="mod-footer"></div>
		  </div>
		</div>
	</div>
</div>
<?php $this->load->view('include/modals'); ?>
<!-- page script -->
<script>
 var save_method, table, company_name, member_type,population_form; pass_method = null;
 var comp_set_code_Arr = []; var member_type_Arr = [];
 var arr1 = [];  var arr2 = [];
 var cct = '<?php echo $this->security->get_csrf_hash() ?>';
 var get_csrf_token_name = '<?php echo $this->security->get_csrf_token_name() ?>';
 var http_assets_path = '<?php echo HTTP_ASSETS_PATH; ?>';

    $.ajaxSetup({
        type:'post',
        data:{ <?php echo $this->security->get_csrf_token_name() ?> :cct}
    });
    
    $(function () {
    	tableAJAX();
     });
     
    function reload_table(){
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function tableAJAX(action = null, status='active', make_mul_action=''){
        $("#tablediv").html(
            '<table id="ajaxTbl" class="display" width="100%" >'+
            '<thead>'+
            	'<tr>'+
                    '<th><center><input type="checkbox" value="" class="selectall"></center></th>' +
                    '<th></th>'+ 
            		'<th></th>'+  
            		'<th></th>'+
            		'<th></th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</thead>'+
            '<tbody></tbody>'+
            '<tfoot hidden>'+
            	'<tr>'+
                    '<th><input type="checkbox" value="" class="selectall"></th>' +
                    '<th></th>'+ 
            		'<th></th>'+  
            		'<th></th>'+
            		'<th></th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</tfoot>'+
            '</table>'
        );
	 
    
       	table = $('#ajaxTbl').DataTable({
       	   
            "scrollY":   '50vh',
            "fixedHeader": {
                header: true,
                footer: true
            },
            "order": [], 
           
            "language": {
                processing:
                "<img src='" +
                http_assets_path +
                "custom/img/ajax-loader.gif' width='2%'>&emsp; Please Wait... (Reload the page if doesn't work.) ",
                "infoFiltered": " "
            },
            "columnDefs": [
                {targets:0, orderable: false, width: "3%", searchable: false, className: 'select-checkbox'},
                {targets:1, orderable: false, visible: false },
                {targets:2, width: "20%" },
                {targets:3, width: "20%" },
                {targets:4, width: "20%" },
                {targets:5, orderable: false, width: "2%", searchable: false}
            ],
            select: {
                style:    'os',
                selector: 'td:first-child'
            },
        });
    
        $.fn.dataTable.ext.errMode = "none";
        
        table.on('xhr', function() {
    	
    	});
        
        table.on("draw", function() {
       
        });
    
        $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	});
    	
    	$(".selectall").on("ifChanged", selectallfunc);
     
    } // end of tableajax
    
     function selectallfunc(e){
        if(e.target.checked){
            $('.selectall').iCheck('check'); 
            table.rows(  ).select();   
        }else{
            $('.selectall').iCheck('uncheck'); 
            table.rows(  ).deselect(); 
        }
    }
    
    function searchDefault(){
        table.search($("#searchEvntMtr").val()).draw();
    } 
    
    function searchDefaultByEnter(){
        var keyPressed = event.keyCode || event.which;
        if(keyPressed==13){
             table.search($("#searchEvntMtr").val()).draw();
             keyPressed=null;
        }else{
            return false;
        }
    }

    function benefit_setup(action,method,id){
      	 if(action == 'view'){
            $('#btn-header').html(''); 
    		$('#myModal').modal({backdrop: 'static', keyboard: false});
    		$('#mod-title').html("<b>Select Mode of Setup</b>");
    		$('#myModal .modal-dialog').addClass('modal-lg');
    		
    		$('#mod-content').html(
    		     '<div class="loadingFORM hidden" style="max-height: calc(100% - 100px);position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">Please wait</div>'+
    
    		     '<div id="wizard-setup" class="row bs-wizard hidden" style="border-bottom:0;padding-top:0;margin-top:0">'+
                    '<div class="col-xs-3 bs-wizard-step active" id="step-phase1" idx="comp_set_prog_basicInfo">'+
                      '<div class="text-center bs-wizard-stepnum" id="step-phase1-txt" idx="comp_set_prog_basicInfo_txt">Type</div>'+
                      '<div class="progress disabled"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot" ></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                    '<div class="col-xs-3 bs-wizard-step disabled" id="step-phase2" idx="comp_set_prog_billing">'+
                      '<div class="text-center bs-wizard-stepnum" id="step-phase2-txt" idx="comp_set_prog_billing_txt" id="step-phase1">Limits</div>'+
                      '<div class="progress"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot"></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                    '<div class="col-xs-3 bs-wizard-step disabled" id="step-phase3" idx="comp_set_prog_contract">'+
                      '<div class="text-center bs-wizard-stepnum" id="step-phase3-txt" idx="comp_set_prog_contract_txt">Inclusions</div>'+
                      '<div class="progress"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot"></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                    '<div class="col-xs-3 bs-wizard-step disabled" id="step-phase4" idx="comp_set_prog_contract">'+
                      '<div class="text-center bs-wizard-stepnum" id="step-phase4-txt" idx="comp_set_prog_contract_txt">Modalities (if any)</div>'+
                      '<div class="progress"><div class="progress-bar"></div></div>'+
                      '<a href="#" class="bs-wizard-dot"></a>'+
                      '<div class="bs-wizard-info text-center"></div>'+
                    '</div>'+
                '</div>'+
                
    			 '<form id="Form1">'+
    			 '<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />'+
    			 '<input type="hidden" name="comp_setup_user_id" value="<?=$u_id_enc;?>" />'+
    			 '<input type="hidden" name="edit_id" value="" />'+
                 '<input type="hidden" name="member_limit_arr" id="member_limit_arr" value="0">'+
    			            '<div style="margin-top:0;margin-left:15px;margin-bottom:5px">'+
        						  '<div style="padding:0;font-size:16px;font-weight:400;float:left" class="hidden"></div>'+
                                  '<div id="div_info"></div>'+
        					'</div>'+
        					'<div style="clear:both"></div>'+
                    '<div id="Form0Div" class="hidden">'+  
                        '<div class="col-xs-2"></div>'+
                        '<div class="col-xs-4" style="padding-top:20px;padding-bottom:20px;border-right:2px solid black">'+
                            '<div class="form-group">'+
                                '<label style="margin-left:20px">'+
                                    '<input type="radio" name="mode_setup" class="minimal selectall pull-left" value="bnt_mtx" checked>'+
                                    '<div style="font-size:16px;float:right;margin-left:10px">Set up a benefit matrix</div>'+
                                '</label>'+
            				'</div>'+
                        '</div>'+
                        '<div class="col-xs-4" style="padding-top:20px;padding-bottom:20px">'+
                            '<div class="form-group">'+
                                '<center><label>'+
                                    '<input type="radio" name="mode_setup" class="minimal selectall pull-left" value="stp_blk">'+
                                    '<div style="font-size:16px;float:right;margin-left:10px">Set up by bulk</div>'+
                                '</label></center>'+
            				'</div>'+
                        '</div>'+
                        '<div class="col-xs-2"></div>'+
                    '</div>'+

                    '<div id="Form1Div-2" class="hidden">'+  
                        '<div class="col-xs-12">'+
            					'<div class="form-group">'+
            						'<label for="exampleInputFile">Create by bulk (xlsx, xls)</label>'+
                                    '<input type="file" id="exampleInputFile">'+
                                    '<p class="help-block">Download Template here:</p>'+
            					'</div>'+
                        '</div>'+
                    '</div>'+    
/* -------------------------------------------------------------------------------------------------------------- */
    			    '<div id="Form1Div" class="hidden">'+
                        '<div class="col-xs-12">'+
        					'<div class="form-group">'+
                                '<label class="control-label">Search and select by Account Name | Code  | Effectivity Date <b style="color:red">*</b></label>'+ 
                                '<select name="comp_set_code"  id="comp_set_code" class="form-control form-val select2not" ></select>'+
                                '<span class="help-block"></span>'+
        					'</div>'+
                        '</div>'+    
                        '<div class="col-xs-12">'+
        					'<div class="form-group">'+
                                '<label class="control-label">Member Type <b style="color:red">*</b></label><button class="btn btn-xs btn-primary pull-right hidden" style="font-size:12px;margin:4px" onclick=member_list("view") id="viewMemberBtn">View Members</button>'+
                                '<select name="member_type" id="member_type" class="form-control form-val select2not" ><option></option></select>'+
                                '<span class="help-block"></span>'+
        					'</div>'+
                        '</div>'+    
        				/*'<div class="col-xs-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Effectivity Date <b style="color:red">*</b></label>'+
                                '<input type="text" class="form-control" data-provide="datepicker" id="comp_contr_effectivity" name="comp_contr_effectivity"  Placeholder="Date Requested" >'+
                                '<span class="help-block"></span>'+
        					'</div>'+
        				'</div>'+*/
                        '<div class="col-xs-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Age Limit <b style="color:red">*</b></label>'+
                                '<input type="text" name="age_limit" id="age_limit" class="form-control form-val" maxlength="5" placeholder="Age Limit">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                        '</div>'+
    				'</div>'+ 
    				
    				
    			    '<div id="Form2Div" class="hidden">'+
    				  
    				'</div>'+ 
    				
    				'<div id="Form3Div" class="hidden">'+
                        '<div class="col-xs-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">ABL Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="abl_inclu" id="abl_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the ABL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">MXBL Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="mxbl_inclu" id="mxbl_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the MXBL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">LBL Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="lbl_inclu" id="lbl_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the LBL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">PEL Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="pel_inclu" id="pel_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the PEL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">CIL Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="cil_inclu" id="cil_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the CIL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">MBL Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="mbl_inclu" id="mbl_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the MBL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">OPD Inclusion <b style="color:red">*</b></label>'+
                                '<input type="text" name="opd_inclu" id="opd_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the OPD Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-xs-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Room and board <b style="color:red">*</b></label>'+
                                '<input type="text" name="room_and_board_inclu" id="room_and_board_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the Room and board">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">Dental <b style="color:red">*</b></label>'+
                                '<input type="text" name="dental_inclu" id="dental_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the Dental">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">APE <b style="color:red">*</b></label>'+
                                '<input type="text" name="ape_inclu" id="ape_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the APE">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">ECU <b style="color:red">*</b></label>'+
                                '<input type="text" name="ecu_inclu" id="ecu_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the ECU">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+ 

                    '<div id="Form4Div" class="hidden">'+
                         '<div class="col-xs-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Modality <b style="color:red">*</b></label>'+
                                '<input type="text" name="modality_mod" id="modality_mod" class="form-control form-val" maxlength="5" placeholder="Enter the ABL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">Amount <b style="color:red">*</b></label><button class="btn btn-xs btn-primary pull-right" style="font-size:12px;margin:4px" onclick=add_modalities("view") id="addModalities">Add</button>'+
                                '<input type="text" name="amount_mod" id="amount_mod" class="form-control form-val" maxlength="5" placeholder="Enter the ABL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="control-label">Remarks <b style="color:red">*</b></label>'+
                                '<input type="text" name="remark_mod" id="abl_inclu" class="form-control form-val" maxlength="5" placeholder="Enter the ABL Inclusion">'+
                                '<span class="help-block"></span>'+
                            '</div>'+
            			'</div>'+
                        '<div class="col-xs-6">'+
            					'<div class="form-group">'+
            						  '<br>'+
            					'</div>'+
            			'</div>'+
                    '</div>'+
                    
                    '<div id="Form5Div" class="hidden">'+
    				  
    				'</div>'+
    			 '</form>'+
    			 '<div style="clear:both"><div>'+
    			 '<div id="require_mess" class="hidden" style="margin-left:15px;padding-top:15px;font-weight:600">Note: Fields with <b style="color:red">*</b> are mandatory.<div>');
    			 
           
            $('#mod-footer').html('<button id="Form0Btn"  class="btn btn-success" onclick=benefit_setup_breadcrumb("phase0",'+null+',"'+method+'") >Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');	 
    		pass_method	= method;
    		if(method == "update"){
    		    benefit_setup_get_data('phase1',id);
    		}	 
            $('#Form0Div').removeClass('hidden');
            formPlugins("phase0"); 
            formPlugins("phase1"); 
        }
    }
    
    function benefit_setup_breadcrumb(page,returnpage=null,method){
       
        if(page == "phase0"){
            $('#Form0Div').removeClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').addClass('hidden');

            $('#mod-title').html("<b>Select Mode of Setup</b>");
            $('#wizard-setup').addClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#div_info').html('');
            $('#Form2Div').html('');
            $('#notif-mess').html(' ');
            formPlugins("phase0");

            $('#mod-footer').html('<button id="Form0Btn"  class="btn btn-success" onclick=benefit_setup_breadcrumb("phase0",'+null+',"'+method+'") >Proceed &emsp;<i class="fa fa-chevron-right"></i></button>'+
    		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            if(returnpage == null){
                if($('[name="mode_setup"]:checked').val() == "bnt_mtx"){
                    benefit_setup_breadcrumb("phase1","back",pass_method);
                }else{
                    benefit_setup_breadcrumb("phase1-2","back",pass_method);
                }
            }
        }else if(page == "phase1-2"){
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').removeClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').addClass('hidden');

            $('#mod-title').html("<b>Benefit Setup - Upload Bulk</b>");
            $('#wizard-setup').addClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#div_info').html('');
            $('#Form2Div').html('');
            $('#notif-mess').html(' ');
            formPlugins("phase1-2"); 

            $('#mod-footer').html('<button id="Form1Btn-2" class="btn btn-success" onclick=benefit_setup_breadcrumb("phase1-2") >Upload</button>'+
    		'<button id="Form1Btn-2-modsetup" class="btn btn-default" onclick=benefit_setup_breadcrumb("phase0","back","'+method+'") >Mode of Setup</button>');
        }else if(page == "phase1"){
                $('#Form0Div').addClass('hidden');
                $('#Form1Div').removeClass('hidden'); $('#Form1Div-2').addClass('hidden');
                $('#Form2Div').addClass('hidden');
                $('#Form3Div').addClass('hidden');
                $('#Form4Div').addClass('hidden');

                $('#mod-title').html("<b>Benefit Setup</b>");
                $('#wizard-setup').removeClass('hidden');
                $('#require_mess').removeClass('hidden');
                $('#notif-mess').html(' ');
            
                
                if(returnpage == "back"){
                    population_form = $('#Form1').serialize();

                $('#mod-footer').html('<button id="Form1Btn" class="btn btn-success" onclick=benefit_setup_breadcrumb("phase1") >Limits &emsp;<i class="fa fa-chevron-right"></i></button>'+
                '<button id="Form1Btn-2-modsetup" class="btn btn-default" onclick=benefit_setup_breadcrumb("phase0","back","'+method+'") >Mode of Setup</button>');    
                $('#Form1Btn').attr('disabled',true);  $('Form1Btn-2-modsetup').attr('disabled',true);

                setTimeout(function(){ 
                $('#step-phase1').removeClass('complete').addClass('active');
                $('#Form1Btn').attr('disabled',false);  $('Form1Btn-2-modsetup').attr('disabled',false);
                }, 490);
                }
                $('#step-phase2').removeClass('active').removeClass('complete').addClass('disabled');
                $('#step-phase3').removeClass('active').removeClass('complete').addClass('disabled');
                $('#step-phase4').removeClass('active').removeClass('complete').addClass('disabled');

                 // Animation txt bold
                $('#step-phase1-txt').css({'font-weight':'bold'}); 
                $('#step-phase2-txt').css({'font-weight':'normal'}); 
                $('#step-phase3-txt').css({'font-weight':'normal'});
                $('#step-phase4-txt').css({'font-weight':'normal'});
            
                
                if(returnpage == null){
                
                comp_set_code_Arr = [];
                member_type_Arr = [];
                var valueArrMem = [];
            
                var data_val_acc_NAMEID = $("#comp_set_code").val() == null ? "" : $("#comp_set_code").val().toString();
                var data_acc_NAMEID = {
                    comp_set_code: data_val_acc_NAMEID
                };

                var data_val_acc_MEM = $("#member_type").val() == null ? "" : $("#member_type").val().toString();
                var data_acc_MEM = {
                    member_type: data_val_acc_MEM
                };

                var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method+ "&" + $.param(data_acc_NAMEID)+ "&" + $.param(data_acc_MEM);
                
                $.ajax({
                    type:'POST',
                    url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                    data:form,
                    dataType:'json',
                    beforeSend:function(){
                        $('.loadingFORM').removeClass('hidden');
                        $(".loadingFORM").fadeIn();
                        $('#Form1').css('opacity','0.1');
                        $('#Form1 :input').attr('disabled', true);
                        $('#Form1Btn').attr('disabled',true);
                        $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                            'width':'190px',
                            'height':'50px',
                            'border-radius':'5px',
                            'background-color':'white',
                            'padding':'15px',
                            'color':'black',
                        }).addClass('alpha60');
                    },
                    success:function(data){
                        $('.loadingFORM').html('').addClass('hidden');
                        $('#Form1').css('opacity','10');
                        $('#Form1 :input').attr('disabled', false);
                        $('#Form1 select').attr('disabled', false);
                        $('#Form1 textarea').attr('disabled', false);
                        $('#Form1Btn').attr('disabled',false);
                    
                        if(data.status){
                                if(returnpage == null){
                                    $('.form-group').removeClass('has-error'); // clear error class
                                    $('.select2not').removeClass('input-validation-error');
                                    $('.help-block').empty();
                                   

                                    $("#comp_set_code option:selected").each(function(){
                                        comp_set_code_Arr.push("<span style='background-color:#42a5f5;margin:1px 2px;padding:1px 4px;color:black;border-radius:5px;display:inline-block;'>"+$(this).val()+"</span>");
                                    })   
                                    $("#member_type option:selected").each(function(i_elem, obj){
                                        i_elem = i_elem + 1;
                                        member_type_Arr.push("<span class='mbrClass btn btn-xs' style='cursor:pointer;background-color:"+(i_elem==1 ? '#a5d6a7' : "#bdbdbd")+";margin:1px 2px;padding:1px 4px;color:black;border-radius:5px;display:inline-block;box-shadow:2px 2px 2px #888888' data-elem_id='"+i_elem+"' data-id='"+$(this).data('value')+"' data-txt='"+$(this).val()+"' data-num='"+i_elem+"'>"+i_elem+'.) '+$(this).val()+"</span>");
                                        valueArrMem.push($(this).data('value'));
                                    })  
                                    $('#member_limit_arr').val(valueArrMem);
                                    benefit_setup_breadcrumb('phase2',"back");
                               
                                }
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error');
                                    $('.select2not[name="'+data.inputerror[i]+'"]').addClass('input-validation-error');
                                    
                                if(data.inputerror[i] == "comp_set_code" || data.inputerror[i] == "member_type"){
                                $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]); 
                                }else{
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                                }
                            }
                            
                        }
                    }
                })
                }
        }else if(page == "phase2"){
            // Show Form per phase
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').removeClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').addClass('hidden');
            $('#div_info').removeClass('hidden');
            // Clear success and error message
            $('#mod-title').html("<b>Benefit Setup</b>");
            $('#wizard-setup').removeClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#notif-mess').html(' ');
            // Animation txt bold
            $('#step-phase1-txt').css({'font-weight':'normal'});  
            $('#step-phase2-txt').css({'font-weight':'bold'}); 
            $('#step-phase3-txt').css({'font-weight':'normal'});
            $('#step-phase4-txt').css({'font-weight':'normal'});
            var nameLlb =(comp_set_code_Arr.length == 1) ? 'Company' : 'Companies';
            var memLlb =(member_type_Arr.length == 1) ? 'Member Type' : 'Member Types';
         
            $('#div_info').html(
                '<div style="float:left;font-size:16px"><b>'+nameLlb+':</b></div>'+
                '<div style="float:left;font-size:12px">'+comp_set_code_Arr.join(" ")+'</div><div style="clear:both"></div>'+
                '<div style="font-size:14px"><b>'+memLlb+':</div><div style="font-size:12px"> </b> '+member_type_Arr.join(" ")+'</div>'
                ).css({
                'float':'left',
                'font-weight':'400',
                'margin-top':'0',
                'margin-bottom':'10px',
                'display': 'inline-block',
                'word-wrap': 'break-word',
            });

            // Change button per phase
            $('#mod-footer').html(
                '<button id="Form3Btn" class="btn btn-success pull-right"  onclick=benefit_setup_breadcrumb("phase2") style="margin-left:5px">Inclusions &emsp;<i class="fa fa-chevron-right"></i></button>'+
                '<button id="Form1Btn" class="btn btn-info pull-right" onclick=benefit_setup_breadcrumb("phase1","back")><i class="fa fa-chevron-left"></i>&emsp; Type</button>'
            );
            $('#Form3Btn').attr('disabled',true);  $('#Form1Btn').attr('disabled',true);
            if(returnpage == "back"){
                $('#step-phase3').removeClass('active').removeClass('complete').addClass('disabled');
                setTimeout(function(){ 
                $('#step-phase2').removeClass('complete').addClass('active');
                    setTimeout(function(){ 
                        $('#step-phase2').addClass('disabled');
                        $('#Form3Btn').attr('disabled',false);  $('#Form1Btn').attr('disabled',false);
                    }, 490);
                }, 490);
                population_form = $('#Form1').serialize();
                limitsForm();
            }
            // Animation
            $('#step-phase1').removeClass('disabled').addClass('complete').removeClass('active');  

           
            if(returnpage == null){
                    var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method;
                    $('.form-group').removeClass('has-error'); 
                    $('.help-block').empty();
                    $('.errorLimit').remove();
                    $.ajax({
                        type:'POST',
                        url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                        data:form,
                        dataType:'json',
                        beforeSend:function(){
                            $('.loadingFORM').removeClass('hidden');
                            $(".loadingFORM").fadeIn();
                            $('#Form1').css('opacity','0.1');
                            $('#Form1 :input').attr('disabled', true);
                            $('#Form1Btn').attr('disabled',true);
                            $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                                'width':'190px',
                                'height':'50px',
                                'border-radius':'5px',
                                'background-color':'white',
                                'padding':'15px',
                                'color':'black',
                            }).addClass('alpha60');
                        },
                        success:function(data){
                            $('.loadingFORM').html('').addClass('hidden');
                            $('#Form1').css('opacity','10');
                            $('#Form1 :input').attr('disabled', false);
                            $('#Form1 select').attr('disabled', false);
                            $('#Form1 textarea').attr('disabled', false);
                            $('#Form1Btn').attr('disabled',false);
                        
                            if(data.status){
                                    if(returnpage == null){
                                        $('.form-group').removeClass('has-error'); 
                                        $('.help-block').empty();
                                    
                                        benefit_setup_breadcrumb('phase3',"back");
                                    }
                            }else{
                                for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                    //var lastChar = data.inputerror[i][data.inputerror[i] -1]; 
                                    var lastChar = data.inputerror[i].substr(data.inputerror[i].length - 1);
                                    $('.mbrClass[data-id="'+lastChar+'"]').after('<span class="errorLimit" data-id="'+lastChar+'">&nbsp;<i class="fa fa-exclamation-triangle" style="color:red"></i></span>');
                                }
                            }
                        }
                    })
            }
        }else if(page == "phase3"){
            // Show Form per phase
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').removeClass('hidden');
            $('#Form4Div').addClass('hidden');
            $('#div_info').addClass('hidden');
            // Clear success and error message
            $('#mod-title').html("<b>Inclusions</b>");
            $('#wizard-setup').removeClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#notif-mess').html(' ');

            // Change button per phase
            $('#mod-footer').html(
            '<button id="Form4Btn" class="btn btn-success pull-right"  onclick=benefit_setup_breadcrumb("phase3") style="margin-left:5px">Modalities &emsp;<i class="fa fa-chevron-right"></i></button>'+
            '<button id="Form2Btn" class="btn btn-info pull-right" onclick=benefit_setup_breadcrumb("phase2","back")><i class="fa fa-chevron-left"></i>&emsp; Limits</button>');
            $('#Form4Btn').attr('disabled',true);  $('#Form2Btn').attr('disabled',true);
            // Animation
            $('#step-phase2').removeClass('disabled').removeClass('active').addClass('complete')
            if(returnpage == "back"){
                setTimeout(function(){ 
                    $('#step-phase4').removeClass('active').removeClass('complete');
                    setTimeout(function(){ 
                        $('#step-phase3').addClass('active').removeClass('complete');
                        setTimeout(function(){ 
                            $('#step-phase3').addClass('disabled');
                            $('#Form4Btn').attr('disabled',false); $('#Form2Btn').attr('disabled',false);
                        }, 490);   
                    }, 490);
                }, 490);
            }
            

             // Animation txt bold
            $('#step-phase2-txt').css({'font-weight':'normal'}); 
            $('#step-phase1-txt').css({'font-weight':'normal'});  
            $('#step-phase3-txt').css({'font-weight':'bold'});
            $('#step-phase4-txt').css({'font-weight':'normal'});

            

            if(returnpage == null){
                    var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method;
                    $('.form-group').removeClass('has-error'); 
                    $('.help-block').empty();
                    $('.errorLimit').remove();
                    $.ajax({
                        type:'POST',
                        url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                        data:form,
                        dataType:'json',
                        beforeSend:function(){
                            $('.loadingFORM').removeClass('hidden');
                            $(".loadingFORM").fadeIn();
                            $('#Form1').css('opacity','0.1');
                            $('#Form1 :input').attr('disabled', true);
                            $('#Form1Btn').attr('disabled',true);
                            $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                                'width':'190px',
                                'height':'50px',
                                'border-radius':'5px',
                                'background-color':'white',
                                'padding':'15px',
                                'color':'black',
                            }).addClass('alpha60');
                        },
                        success:function(data){
                            $('.loadingFORM').html('').addClass('hidden');
                            $('#Form1').css('opacity','10');
                            $('#Form1 :input').attr('disabled', false);
                            $('#Form1 select').attr('disabled', false);
                            $('#Form1 textarea').attr('disabled', false);
                            $('#Form1Btn').attr('disabled',false);
                        
                            if(data.status){
                                    if(returnpage == null){
                                        $('.form-group').removeClass('has-error'); 
                                        $('.help-block').empty();
                                    
                                        benefit_setup_breadcrumb('phase4',"back");
                                    }
                            }else{
                                for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                 }
                            }
                        }
                    })
            }
        }else if(page == "phase4"){
            $('#Form0Div').addClass('hidden');
            $('#Form1Div').addClass('hidden'); $('#Form1Div-2').addClass('hidden');
            $('#Form2Div').addClass('hidden');
            $('#Form3Div').addClass('hidden');
            $('#Form4Div').removeClass('hidden');
            $('#div_info').addClass('hidden');
            // Clear success and error message
            $('#mod-title').html("<b>Modalities (If Any)</b>");
            $('#wizard-setup').removeClass('hidden');
            $('#require_mess').addClass('hidden');
            $('#notif-mess').html(' ');

            $('#mod-footer').html(
            '<button id="Form5Btn" class="btn btn-success pull-right"  onclick=benefit_setup_breadcrumb("phase5") style="margin-left:5px">Save &emsp;<i class="fa fa-chevron-right"></i></button>'+
            '<button id="Form3Btn" class="btn btn-info pull-right" onclick=benefit_setup_breadcrumb("phase3","back")><i class="fa fa-chevron-left"></i>&emsp; Inclusions</button>');
            $('#Form5Btn').attr('disabled',true);  $('#Form3Btn').attr('disabled',true);

            // Animation
            $('#step-phase3').removeClass('disabled').removeClass('active').addClass('complete');
            setTimeout(function(){ 
                $('#step-phase3').removeClass('active').addClass('complete');
                setTimeout(function(){ 
                    $('#step-phase4').addClass('active');
                    $('#Form5Btn').attr('disabled',false);  $('#Form3Btn').attr('disabled',false);
                }, 450);
            }, 490);
            if(returnpage == "back"){
                $('#step-phase4').addClass('disabled');
            }

             // Animation txt bold
            $('#step-phase2-txt').css({'font-weight':'normal'}); 
            $('#step-phase1-txt').css({'font-weight':'normal'});  
            $('#step-phase3-txt').css({'font-weight':'normal'});
            $('#step-phase4-txt').css({'font-weight':'bold'});
            
            if(returnpage == null){
                    var form = $('#Form1').serialize()+"&page="+page+"&method="+pass_method;
                    $('.form-group').removeClass('has-error'); 
                    $('.help-block').empty();
                    $('.errorLimit').remove();
                    $.ajax({
                        type:'POST',
                        url:'<?php echo site_url('Account/BenefitSetupFormValidate'); ?>',
                        data:form,
                        dataType:'json',
                        beforeSend:function(){
                            $('.loadingFORM').removeClass('hidden');
                            $(".loadingFORM").fadeIn();
                            $('#Form1').css('opacity','0.1');
                            $('#Form1 :input').attr('disabled', true);
                            $('#Form1Btn').attr('disabled',true);
                            $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                                'width':'190px',
                                'height':'50px',
                                'border-radius':'5px',
                                'background-color':'white',
                                'padding':'15px',
                                'color':'black',
                            }).addClass('alpha60');
                        },
                        success:function(data){
                            $('.loadingFORM').html('').addClass('hidden');
                            $('#Form1').css('opacity','10');
                            $('#Form1 :input').attr('disabled', false);
                            $('#Form1 select').attr('disabled', false);
                            $('#Form1 textarea').attr('disabled', false);
                            $('#Form1Btn').attr('disabled',false);
                        
                            if(data.status){
                                    if(returnpage == null){
                                        $('.form-group').removeClass('has-error'); 
                                        $('.help-block').empty();
                                    
                                        benefit_setup_breadcrumb('phase5',"back");
                                    }
                            }else{
                                for (var i = 0; i < data.inputerror.length; i++){
                                    $('[name="'+data.inputerror[i]+'"]').parent().addClass('has-error'); 
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                 }
                            }
                        }
                    })
            }
        }else if(page == "phase5"){
            $('#step-phase4').removeClass('disabled').removeClass('active').addClass('complete');

            $('#mod-footer').html(
            '<button id="CompSetupDecideNoBtn"  class="btn btn-info pull-right" style="margin-left:5px" onclick=benefit_setup_breadcrumb("phase4","back")>Cancel</button>'+
            '<button id="CompSetupDecideNoBtn"  class="btn btn-success pull-right" style="margin-left:5px" onclick=benefit_setup_breadcrumb("save")>No, Save this form</button>'+
            '<button id="CompSaveInfoBtn"  class="btn btn-success pull-right" style="margin-right:5px;margin-left:5px" onclick=benefit_setup_breadcrumb("phase1","back")>Yes, review it!</button>'
            );
            var form = $('#CompSetupForm').serialize()+"&page="+page;
            
            $('#notif-mess').html('<p style="text-align:left;font-weight:600"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Are you sure do you want take a review this benefit form?</p>').css('color','orange');
        }else if(page == "save"){

        }
    }

    $(document).on('click','.mbrClass',function(){
        var elem_id = $(this).data('elem_id');
        var id = $(this).data('id');
        $('.limitFormDivCls').addClass('hidden');
        $('#Form2Div').removeClass('hidden');
        $('.limitFormDivCls[data-elem_id="'+id+'"]').removeClass('hidden');
        $('.mbrClass').css({'background-color':'#bdbdbd'});

        $(this).css({'background-color':'#a5d6a7'});
    });

    function limitsForm(){
        $('#Form2Div').empty();
        var mbrClassLength =  $('.mbrClass').length;
        
        for(var i_elem = 1;i_elem <= mbrClassLength;i_elem++){
            var id = $('.mbrClass[data-elem_id="'+i_elem+'"]').data('id');
            $('#Form2Div').append(
                appendForm(i_elem,id)
            );

            $.each(population_form.split('&'), function (index, elem) {
            var vals = elem.split('=');
             $("#Form2Div [name='" + vals[0]+"']").val(vals[1]);
            });
        }
   
            $('.limitFormDivCls').first().removeClass('hidden');
            $("input").keyup(function(){
                var lastChar = $(this).attr('name').substr($(this).attr('name').length - 1);
                $('.errorLimit[data-id="'+lastChar+'"]').remove();
                $(this).parent().removeClass('has-error');
                $(this).next().empty();
            });
    }

    function appendForm(i_elem,id){
        $('#Form2Div').append(
            '<div style="clear:both"></div>'+
            '<div class="limitFormDivCls hidden" data-elem_id="'+id+'" data-id="'+id+'">'+
            '<div class="col-xs-6">'+
                    '<div class="form-group">'+
                        '<label class="control-label">Annual Benefit Limit <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_abl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the Annual Benefit Limit">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="control-label">MXBL <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_mxbl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the MXBL">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="control-label">LBL <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_lbl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the LBL">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="control-label">PEL <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_pel_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the PEL">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
            '</div>'+
            '<div class="col-xs-6">'+
                    '<div class="form-group">'+
                        '<label class="control-label">CIL <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_cil_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the CIL">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="control-label">MBL <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_mbl_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the MBL">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="control-label">OPD <b style="color:red">*</b></label>'+
                        '<input type="text" name="member_opd_'+id+'" class="form-control form-val" maxlength="5" placeholder="Enter the OPD">'+
                        '<span class="help-block"></span>'+
                    '</div>'+
            '</div>'+
            '<div>'
        );
    }

    function getMemberInfo(data_val){
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/findMemberInfo'); ?>',
            data:{id:data_val},
            dataType:'json',
            beforeSend:function(){
                $('.loadingFORM').removeClass('hidden');
                $(".loadingFORM").fadeIn();
                $('#Form2').css('opacity','0.1');
                $('#Form2 :input').attr('disabled', true);
                $('#Form2Btn').attr('disabled',true);
                $('.loadingFORM').html("<img src='"+http_assets_path + "custom/img/ajax-loader.gif'>&nbsp; Please wait...").css({
                    'width':'190px',
                    'height':'50px',
                    'border-radius':'5px',
                    'background-color':'white',
                    'padding':'15px',
                    'color':'black',
                }).addClass('alpha60');
            },
            success:function (data){
                $('.loadingFORM').html('').addClass('hidden');
                $('#Form2').css('opacity','10');
                $('#Form2 :input').attr('disabled', false);
                $('#Form2 select').attr('disabled', false);
                $('#Form2 textarea').attr('disabled', false);
                $('#Form2Btn').attr('disabled',false);

                $('#member_abl').val(data.member_abl);
                $('#member_mxbl').val(data.member_mxbl);
                $('#member_lbl').val(data.member_lbl);
                $('#member_pel').val(data.member_pel);
                $('#member_cil').val(data.member_cil);
                $('#member_mbl').val(data.member_mbl);
                $('#member_opd').val(data.member_opd);
            }
        });
    }

    function benefit_setup_get_data(){
        
    }

    function formPlugins(page){
        //if(page == "phase0"){
            $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	    });
       // }else if(page == "phase1"){
            $('#comp_set_code').select2({
                    width:"100%",
                    placeholder:'You can search by Account Name or Code',
                    allowClear:true,
                    multiple:true,
                    
            });
            comp_set_code_list();

            $("#comp_contr_effectivity").datepicker().datepicker("setDate", new Date());

            $('[name="member_type"]').select2({
                    width:"100%",
                    placeholder:'Member Type',
                    allowClear:true,
                    multiple:true,
                    /*sorter: function(data) {
                     
                        return data.sort(function (a, b) {
                            a = a.text.toLowerCase();
                            b = b.text.toLowerCase();
                            if (a > b) {
                                return 1;
                            } else if (a < b) {
                                return -1;
                            }
                            return 0;
                        });
            }*/
            });
            member_type_list();
            
        //}

        $("input").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").not('.select2not').change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().empty();
        });
        $(".select2not").change(function(){
            $(this).parent().removeClass('has-error');
            $(this).next().next().empty();
        });
    }

    function comp_set_code_list(){
        $('#comp_set_code').empty();
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/comp_set_code_list'); ?>',
            dataType:'json',
            beforeSend:function(){
                $('#comp_set_code').attr("placeholder", "Loading Data...");
            },
            success:function (data){
                $('#comp_set_code').append(data);
            }
        });
    }

    function member_type_list(){
        $('[name="member_type"]').empty();
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/member_type_list'); ?>',
            dataType:'json',
            beforeSend:function(){
                $('[name="member_type"]').attr("placeholder", "Loading Data...");
            },
            success:function (data){
                $('[name="member_type"]').append(data);
            }
            
        });
    }

    $(document.body).on("change","#member_type",function(){
        /*var data_val = $('[name="member_type"] :selected').data('id');
        $('.limitFormDivCls').each(function(){
            if($(this).data('id') != data_val){
                $(this).remove();
            }
        })*/
    });

    // onchange
    // oc comp_set_code
   /* $(document.body).on("change","#comp_set_code",function(){
        var data_val = $('[name="comp_set_code"] :selected').data('value');
        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/findCmpySetpInfo'); ?>',
            dataType:'json',
            data:{id:data_val},
            beforeSend:function(){
                $('#div_info').html(
                    '<span style="float:left;font-size:16px">Loading Data...</span>'
                    ).css({
                    'float':'left',
                    'font-weight':'400',
                    'margin-top':'0',
                    'margin-bottom':'10px'
                });
            },
            success:function (data){
                $('#div_info').html(
                    '<span style="float:left;font-size:16px">Company: '+data.comp_set_company_name+'</span>'
                    ).css({
                    'float':'left',
                    'font-weight':'400',
                    'margin-top':'0',
                    'margin-bottom':'10px'
                });
                $('[name="comp_contr_effectivity"]').val(data.comp_contr_effectivity);
          
                //$('[name="member_type"]').append(data);
            }
            
        });
    });

    // oc member_type
    $(document.body).on("change","#member_type",function(){
        var data_val = $('[name="member_type"] :selected').data('value');
        var opt = $('[name="member_type"] :selected').text();
        var length = $('[name="member_type"] option').not( "[data-select2-tag='true']").filter(function() {
                    return $(this).text() === opt;
                    }).length;
        
        if(length != 0){
            $('#addMemberBtn').addClass('hidden');
        } else {
            $('#addMemberBtn').removeClass('hidden');
        }

        $.ajax({
            type:'POST',
            url:'<?php echo base_url('Account/findMemberInfo'); ?>',
            dataType:'json',
            data:{id:data_val},
            beforeSend:function(){
                $('#age_limit').attr('disabled',true).attr('placeholder','Loading...');
            },
            success:function (data){
                $('#age_limit').attr('disabled',false).attr('placeholder','Age Limit');
                $('#age_limit').val((data.member_age_limit == 0 ? '' :data.member_age_limit));
            }
            
        });
    });*/

    
</script>

</body>
</html>
