<!DOCTYPE html>
<html>
    
<?php 
$arr['db'] = isset($db->page_header) ? $db->page_header : array();
$this->load->view('include/header',$arr); 
?>

<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <?php 
  $arr['db'] = isset($db->main_header) ? $db->main_header : array();
  $this->load->view('include/main_header',$arr); 
  ?> 


  <!-- Full Width Column -->
  <div class="content-wrapper">
  <?php 
  $arr['db'] = isset($db->main_sub_header) ? $db->main_sub_header : array();
  $this->load->view('include/main_sub_header',$arr); 
  ?>
 <?php 
      $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
      );
  ?>
  <?php 
    $u_id_enc = $db->main_header['user_info']->id;
    $gen_code = $db->page_body['generate_code'];
  ?>
  <div class="">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="" >
        <h3 style="margin:0;padding:0;font-size:20px;font-weight:600;margin-left:-25px" class="pull-left">
        <ol class="breadcrumb" style="background-color:transparent;margin:0;padding:0;">
          <li><a href="#">&emsp; Home</a></li>
          <li><a href="#">Member</a></li>
          <li><a href="#">View Member List</a></li>
        </ol>  
        </h3>

        <div class="pull-right" style="margin-top:-5px;margin-bottom:10px">
		    <a class="btn btn-md btn-primary pull-left" style="margin-right:10px;cursor: pointer;"> Company Setup</a>
        </div>
      </section>

      <!-- Main content -->
      <section class="content" >
       

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
                <div class="box-header">
                    <div class="col-xs-6" style="margin:0;padding:0">
                      <h3 class="box-title" style="margin-top:4px;margin-left:4px;margin-bottom:2px">Members</h3>
                    </div>
                    <div class="col-xs-5 pull-right" style="margin:0;padding:0">
                        <div class="input-group" >
                            <input type="text" id="searchEvntMtr" class="form-control" placeholder="Enter any keyword to search." onkeyup="searchDefaultByEnter()">
                              <span class="input-group-btn">
                              <button type="submit" id="searchEvntMtrBtn" onclick="searchDefault()" class="btn btn-flat" ><i class="fa fa-search"></i>
                              </button>
                              </span>
                        </div>
                        <!--onClick="showAdvanceSearch()"-->
                        <div class="pull-right" id="searchDiv"><a href="javascript:void(0)" >Show Advance Search</a></div>
                    </div>
                </div>
                <!-- /.box-header -->
           
                <div id="tablediv" class="box-body table-responsive" style="margin-top:0;padding-top:0">
                <!--AJAX TABLE LOADING-->
                </div><br><br><br>
                <!-- /.box-body -->
                
                
          </div>
          <!-- /.box -->
          </div>
          </div>


      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
    
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="">
      <div class="pull-right hidden-xs">
        <b>Beta Version:</b> 1.0.0 | 12.17.2018
      </div>
      <strong>Omniquest.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>&nbsp;
			<div class="pull-right" id="btn-header"></div>&nbsp; 
			
			<h4 class="modal-title pull-left" id='mod-title'></h4>
		  </div>
		  <div class="modal-body">
			<div id='mod-content'>
			</div>
		  </div>
		  <div class="modal-footer">
			<span class="pull-left" id="notif-mess"></span>
			<div id="mod-footer"></div>
		  </div>
		</div>
	</div>
</div>
<?php $this->load->view('include/modals'); ?>
<!-- page script -->
<script>
 var save_method, table, company_name, member_type,population_form; pass_method = null;
 var comp_set_code_Arr = []; var member_type_Arr = [];
 var arr1 = [];  var arr2 = [];
 var cct = '<?php echo $this->security->get_csrf_hash() ?>';
 var get_csrf_token_name = '<?php echo $this->security->get_csrf_token_name() ?>';
 var http_assets_path = '<?php echo HTTP_ASSETS_PATH; ?>';

    $.ajaxSetup({
        type:'post',
        data:{ <?php echo $this->security->get_csrf_token_name() ?> :cct}
    });
    
    $(function () {
    	tableAJAX();
     });
     
    function reload_table(){
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function tableAJAX(action = null, status='active', make_mul_action=''){
        $("#tablediv").html(
            '<table id="ajaxTbl" class="display" width="100%" >'+
            '<thead>'+
            	'<tr>'+
                    '<th><center><input type="checkbox" value="" class="selectall"></center></th>' +
                    '<th></th>'+ 
            		'<th></th>'+  
            		'<th></th>'+
            		'<th></th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</thead>'+
            '<tbody></tbody>'+
            '<tfoot hidden>'+
            	'<tr>'+
                    '<th><input type="checkbox" value="" class="selectall"></th>' +
                    '<th></th>'+ 
            		'<th></th>'+  
            		'<th></th>'+
            		'<th></th>'+
            		'<th><center><i class="fa fa-cog" aria-hidden="true"></i></center></th>'+
            	'</tr>'+
            '</tfoot>'+
            '</table>'
        );
	 
    
       	table = $('#ajaxTbl').DataTable({
       	   
            "scrollY":   '50vh',
            "fixedHeader": {
                header: true,
                footer: true
            },
            "order": [], 
           
            "language": {
                processing:
                "<img src='" +
                http_assets_path +
                "custom/img/ajax-loader.gif' width='2%'>&emsp; Please Wait... (Reload the page if doesn't work.) ",
                "infoFiltered": " "
            },
            "columnDefs": [
                {targets:0, orderable: false, width: "3%", searchable: false, className: 'select-checkbox'},
                {targets:1, orderable: false, visible: false },
                {targets:2, width: "20%" },
                {targets:3, width: "20%" },
                {targets:4, width: "20%" },
                {targets:5, orderable: false, width: "2%", searchable: false}
            ],
            select: {
                style:    'os',
                selector: 'td:first-child'
            },
        });
    
        $.fn.dataTable.ext.errMode = "none";
        
        table.on('xhr', function() {
    	
    	});
        
        table.on("draw", function() {
       
        });
    
        $('.selectall').iCheck({
    		  checkboxClass: 'icheckbox_square-blue',
    		  radioClass: 'iradio_square-blue',
    		  increaseArea: '10%' /* optional */
    	});
    	
    	$(".selectall").on("ifChanged", selectallfunc);
     
    } // end of tableajax
    
     function selectallfunc(e){
        if(e.target.checked){
            $('.selectall').iCheck('check'); 
            table.rows(  ).select();   
        }else{
            $('.selectall').iCheck('uncheck'); 
            table.rows(  ).deselect(); 
        }
    }
    
    function searchDefault(){
        table.search($("#searchEvntMtr").val()).draw();
    } 
    
    function searchDefaultByEnter(){
        var keyPressed = event.keyCode || event.which;
        if(keyPressed==13){
             table.search($("#searchEvntMtr").val()).draw();
             keyPressed=null;
        }else{
            return false;
        }
    }
</script>

</body>
</html>
