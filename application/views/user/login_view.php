<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login | MAS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo" style="margin-bottom:0">
    <a href="javascript:void(0)"><?php echo isset($db->system_title) ? $db->system_title : "Title of software"; ?></a>
  </div>
  <p class="login-box-msg"><?php echo isset($db->system_sub_title) ? $db->system_sub_title : "Sub Title of software"; ?></p>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg" >Login</p>
    <?php echo form_open('login'); ?>
    <div class="form-group has-feedback <?php if(form_error('username')) { echo 'has-error'; } ?> ">
        <input type="text" class="form-control" name="username" placeholder="Username or Email Address" autofocus="autofocus"  value="<?php echo set_value('username'); ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		    <span class="help-block">
          <?php echo form_error('username'); ?>
        </span>
		  </div>
      <div class="form-group input-group input-group-md col-md-12 col-xs-12 <?php if(form_error('password')) { echo 'has-error'; } ?> <?php echo isset($error_p) ? 'has-error' : ''; ?>">
          <div class="form-group input-group input-group-md col-md-12 col-xs-12" style="padding-bottom:0;margin-bottom:0;">
            <input type="password" name="password" placeholder="Password" class="form-control showpassword">	
            <span class="input-group-btn">
            <button type="button" class="btn btn-default btn-flat showpasswordbtn"><i class="fa fa-eye" aria-hidden="true"></i></button>
            </span>
          </div>
          <span class="help-block helpforpw" style="padding-bottom:0;margin-bottom:0;">
          <?php echo form_error('password'); ?>
         </span>
		  </div>
      <div class="row">
        <div class="col-xs-12 ">
          <div class="checkbox icheck ">
            <div class="<?php if(form_error('accept_terms')) { echo 'has-error'; } ?>">
            <span class="help-block" style="color:#dd4b39"><?php echo form_error('accept_terms'); ?> </span>
            </div>
            <label>
              <input type="checkbox" name="accept_terms" required id="aggrement_chk">&nbsp; I agree to abide by the details of the <a href="https://www.privacy.gov.ph/data-privacy-act/">Data Privacy Policy.</a>
            </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat sign-in-btn">Login In</button>
        </div>
        <div class="col-xs-4"></div>
      </div>
    <?php echo form_close(); ?>
    <input type='checkbox'  id='showpasswordcheckbox' class="hidden" />
    <br>
    <center>
    <a href="<?php echo base_url('reset-password'); ?>">Forgot your password?
</a><br>
    <a href="<?php echo base_url('register'); ?>" class="text-center">Register a new membership</a>
    </center>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/icheck.min.js"></script>
<script>
 $(function () {
  $('.showpasswordbtn').click(function(e) {	
		e.preventDefault();
		if($(this).find('i').hasClass('fa-eye')){
			$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
			$('#showpasswordcheckbox').prop('checked',true).change();
		}else{
			$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
			$('#showpasswordcheckbox').prop('checked',false).change();
		}
	});

  $('#aggrement_chk').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
  });
	
	$('#showpasswordcheckbox').change(function(){
		var $input = $('.showpassword'); 	var $input2 = $('.showpassword2');
		  var change = $(this).is(":checked") ? "text" : "password";
                var rep = $(".showpassword")
					.attr("type", change)
                    .attr("id", $input.attr("id"))
                    .attr("name", $input.attr("name"))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
                $input = rep;
				var rep2 = $(".showpassword2")
					.attr("type", change)
                    .attr("id", $input2.attr("id"))
                    .attr("name", $input2.attr("name"))
                    .attr('class', $input2.attr('class'))
                    .val($input2.val())
                    .insertBefore($input2);
                $input2 = rep2;
	  });
	
	  $("input").change(function(){
        $(this).parent().removeClass('has-error');
        $(this).next().next().empty();
    });
    $("password").change(function(){
        $(this).parent().removeClass('has-error');
        $(this).next().next().empty();
    });
	
	  $("input").focus(function(){
		var thisname = this.name; 
			$(this).parent().removeClass('has-error');
			$(this).next().next().empty();
			$('.helpforpw').html('');
	  });
  }); 

</script>
</body>
</html>
