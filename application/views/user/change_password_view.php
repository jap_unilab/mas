<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Change Password | MAS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo" style="margin-bottom:0">
    <a href="javascript:void(0)"><?php echo isset($db->system_title) ? $db->system_title : "Title of software"; ?></a>
  </div>
  <p class="login-box-msg"><?php echo isset($db->system_sub_title) ? $db->system_sub_title : "Sub Title of software"; ?></p>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Change Password</p>

    <?php  
       $en2 = http_build_query(array('key' => $this->encryption->encrypt($db->email_pass)));; 
       echo form_open('change-password?'.$en2); 
    ?>
    <?php if(isset($db->success)){ ?>
    <div style="padding-bottom:15px;padding-top:0"><center><p style="color:green;font-weight:bold">
        <i class="fa fa-check-circle-o"></i>&nbsp; Your password is successfully changed.</p></center></div>
    <br>
    <a href="<?php echo base_url('login'); ?>">Login</a>
    
    <?php }else if(isset($db->error)){ ?>
    <div style="padding-bottom:15px;padding-top:0"><center><p style="color:#dd4b39;font-weight:bold">
        <i class="fa fa-exclamation-triangle"></i>&nbsp; <?php echo $db->error_mes;  ?></p></center></div>
    <br>
    <a href="<?php echo base_url('login'); ?>">Login</a>
    
    <?php }else{ ?>
       <?php echo isset($db->post_email) ? $db->post_email : ''; ?>
       <input type="hidden" value="<?php echo isset($db->email_pass) ? $db->email_pass : ''; ?>" name="email">
       <input type="hidden" value="<?php echo isset($db->status) ? $db->status : ''; ?>" name="status">
       <div class="form-group input-group input-group-md col-md-12 col-xs-12 <?php if(form_error('password')) { echo 'has-error'; } ?>">
    		<div class="form-group input-group input-group-md col-md-12 col-xs-12" style="padding-bottom:0;margin-bottom:0;">
    		<input type="password" name="password" autofocus="autofocus" placeholder="Password" class="form-control showpassword">	
    			<span class="input-group-btn">
    			 <button type="button" class="btn btn-default btn-flat showpasswordbtn"><i class="fa fa-eye" aria-hidden="true"></i></button>
    			</span>
    		</div>
			<span class="help-block helpforpw" style="padding-bottom:0;margin-bottom:0;"><?php echo form_error('email'); ?></span>
	  </div>
	  <div class="form-group has-feedback <?php if(form_error('passconf')) { echo 'has-error'; } ?>">
            <input type="password" name="passconf" class="form-control showpassword2" placeholder="Retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            <span class="help-block">
                <?php echo form_error('passconf'); ?>
                <?php echo !empty($this->session->flashdata('msg')) ? $this->session->flashdata('msg') : ''; ?>
            </span>
      </div>
      <div class="row">
        <div class="col-xs-8"><div class="checkbox icheck"><label id="formMes"></label></div></div>
        <div class="col-xs-4"><button type="submit" id="savesetupPasswordBtn" class="btn btn-primary btn-block btn-flat">Submit</button></div>
      </div>
    <?php echo form_close(); ?>
    <input type='checkbox'  id='showpasswordcheckbox' class="hidden" />
    <?php } ?>
<br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('#reset_btn').click(function(){
      $('#loading_gif').removeClass('hidden');
      setTimeout(function(){  $('#reset_btn').attr('disabled',true); }, 500);
      clearInput();
    })
    
     $('.showpasswordbtn').click(function(e) {	
		e.preventDefault();
		if($(this).find('i').hasClass('fa-eye')){
			$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
			$('#showpasswordcheckbox').prop('checked',true).change();
		}else{
			$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
			$('#showpasswordcheckbox').prop('checked',false).change();
		}
	});
	
	$('#showpasswordcheckbox').change(function(){
		var $input = $('.showpassword'); 	var $input2 = $('.showpassword2');
		  var change = $(this).is(":checked") ? "text" : "password";
                var rep = $(".showpassword")
					.attr("type", change)
                    .attr("id", $input.attr("id"))
                    .attr("name", $input.attr("name"))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
                $input = rep;
				var rep2 = $(".showpassword2")
					.attr("type", change)
                    .attr("id", $input2.attr("id"))
                    .attr("name", $input2.attr("name"))
                    .attr('class', $input2.attr('class'))
                    .val($input2.val())
                    .insertBefore($input2);
                $input2 = rep2;
	});
  });

  function clearInput() {
      $("form input").parent().removeClass("has-error");
      $(".form-group").removeClass("has-error"); // clear error class
      $(".help-block").empty();
  } 
</script>
</body>
</html>
