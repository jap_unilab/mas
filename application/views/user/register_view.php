<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Registration Page | MAS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="login-logo" style="margin-bottom:0">
    <a href="javascript:void(0)"><?php echo isset($db->system_title) ? $db->system_title : "Title of software"; ?></a>
  </div>
  <p class="login-box-msg"><?php echo isset($db->system_sub_title) ? $db->system_sub_title : "Sub Title of software"; ?></p>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>
    <?php if(isset($db->success)){ ?> 
      <div style="padding-bottom:15px;padding-top:0"><center><p style="color:green;font-weight:bold">
        <i class="fa fa-check-circle-o"></i>&nbsp; You are successfully registered.</p></center>
      </div>
      <br>
    <?php }else{ ?> 
    <?php echo form_open('register'); ?>
      <input type="hidden" name="agreement" value="">
      <div class="form-group <?php if(form_error('username')) { echo 'has-error'; } ?>">
        <input type="text" class="form-control" name="username" value="<?php echo set_value('username'); ?>" autofocus="autofocus" placeholder="Username | ex: juan01">
        <span class="help-block"><?php echo form_error('username'); ?></span>
      </div>
      <div class="form-group <?php if(form_error('email')) { echo 'has-error'; } ?>">
        <input type="email" class="form-control" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email Address">
        <span class="help-block"><?php echo form_error('email'); ?></span>
      </div>
      <div class="form-group input-group input-group-md col-md-12 col-xs-12  <?php if(form_error('password')) { echo 'has-error'; } ?>">
    		<div class="form-group input-group input-group-md col-md-12 col-xs-12" style="padding-bottom:0;margin-bottom:0;">
    		<input type="password" name="password" placeholder="Password" class="form-control showpassword">	
    			<span class="input-group-btn">
    			 <button type="button" class="btn btn-default btn-flat showpasswordbtn"><i class="fa fa-eye" aria-hidden="true"></i></button>
    			</span>
    		</div>
			<span class="help-block helpforpw" style="padding-bottom:0;margin-bottom:0;"><?php echo form_error('password'); ?></span>
      </div>
      <div class="form-group has-feedback <?php if(form_error('passconf')) { echo 'has-error'; } ?>">
              <input type="password" name="passconf" class="form-control showpassword2" placeholder="Retype password">
              <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
              <span class="help-block"><?php echo form_error('passconf'); ?></span>
      </div>
      <div class="row">
        <div class="col-xs-8 hidden">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" id="aggrement"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
      </div>  
      <div class="row">
      <div class="col-xs-4"></div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-4"></div>
      </div>
    <?php echo form_close(); ?>
    <?php } ?>
    <input type='checkbox' id='showpasswordcheckbox' class="hidden" />
    <br>
    <a href="<?php echo base_url('login'); ?>" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/icheck.min.js"></script>
<script>
  function clearInput() {
      $(".setupPasswordForm input") .parent().removeClass("has-error");
      $(".form-group").removeClass("has-error"); // clear error class
      $(".help-block").empty();
  }  
  $(function () {
    $('#aggrement').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });

   $('.showpasswordbtn').click(function(e) {	
		e.preventDefault();
		if($(this).find('i').hasClass('fa-eye')){
			$(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
			$('#showpasswordcheckbox').prop('checked',true).change();
		}else{
			$(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
			$('#showpasswordcheckbox').prop('checked',false).change();
		}
	});
	
	$('#showpasswordcheckbox').change(function(){
		var $input = $('.showpassword'); 	var $input2 = $('.showpassword2');
		  var change = $(this).is(":checked") ? "text" : "password";
                var rep = $(".showpassword")
					.attr("type", change)
                    .attr("id", $input.attr("id"))
                    .attr("name", $input.attr("name"))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
                $input = rep;
				var rep2 = $(".showpassword2")
					.attr("type", change)
                    .attr("id", $input2.attr("id"))
                    .attr("name", $input2.attr("name"))
                    .attr('class', $input2.attr('class'))
                    .val($input2.val())
                    .insertBefore($input2);
                $input2 = rep2;
	});
  });
</script>
</body>
</html>
