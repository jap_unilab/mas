<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Reset Password | MAS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo" style="margin-bottom:0">
    <a href="javascript:void(0)"><?php echo isset($db->system_title) ? $db->system_title : "Title of software"; ?></a>
  </div>
  <p class="login-box-msg"><?php echo isset($db->system_sub_title) ? $db->system_sub_title : "Sub Title of software"; ?></p>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php echo form_open('reset-password'); ?>
      <div class="form-group <?php if(form_error('email')) { echo 'has-error'; } ?> <?php echo isset($db->error) ? 'has-error' : ''; ?>">
        <input type="email" class="form-control" name="email" autofocus="autofocus" value="<?php echo isset($db->success) ? '' : set_value('email'); ?>" placeholder="Email Address">
        <span class="help-block">
          <?php echo form_error('email'); ?>
          <?php echo isset($db->error) ? $db->error : ''; ?>
          <?php echo isset($db->success) ? '<center><span style="color:#66bb6a"><i class="fa fa-check-circle-o"></i>&nbsp;'.$db->success.'</span></center>' : ''; ?>
        </span>
        <div id="loading_gif" class="hidden"><img src="<?php echo HTTP_ASSETS_PATH; ?>custom/img/ajax-loader.gif"></div>
      </div>
     
      <div class="row">
        <div class="col-xs-2">
        </div>
        <!-- /.col -->
        <div class="col-xs-8">
          <button type="submit" id="reset_btn" class="btn btn-primary btn-block btn-flat">Send recovery link</button>
        </div>
        <div class="col-xs-2">
         
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close(); ?>
<br>
    <a href="<?php echo base_url('login'); ?>" class="text-center">Login</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo HTTP_ASSETS_ADLTE2_PATH; ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
    $('#reset_btn').click(function(){
      $('#loading_gif').removeClass('hidden');
      setTimeout(function(){  $('#reset_btn').attr('disabled',true); }, 500);
     
      clearInput();
    })
  });

  function clearInput() {
      $("form input").parent().removeClass("has-error");
      $(".form-group").removeClass("has-error"); // clear error class
      $(".help-block").empty();
  } 
</script>
</body>
</html>
